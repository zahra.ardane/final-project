package GUI;

import Logic.Quiz;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class Poll {

    private Quiz quiz;
    private GridPane gridPane = new GridPane();
    //private AnchorPane container =  new AnchorPane();
    private VBox container=new VBox();
    private RadioButton veryHard = new RadioButton("بسیار دشوار");
    private RadioButton hard = new RadioButton("دشوار");
    private RadioButton average = new RadioButton("متوسط");
    private RadioButton easy = new RadioButton("ساده");
    private RadioButton veryEasy = new RadioButton("بسیار ساده");
    private ToggleGroup toggleGroup = new ToggleGroup();
    private HBox saveHolder=new HBox();
    int participants;
    Button save = new Button("ذخیره");

    public Poll(){
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        gridPane.setBackground(new Background(bgImg));

        Label label = new Label("نظرسنحی");
        label.setFont(new Font("Calibri Light", 20));
//        label.setLayoutX(40);
//        label.setLayoutY(40);

        save.setStyle("-fx-background-color: #FCF3CF");
        save.setFont(new Font("Calibri Light", 23));

        veryHard.setToggleGroup(toggleGroup);
        hard.setToggleGroup(toggleGroup);
        average.setToggleGroup(toggleGroup);
        easy.setToggleGroup(toggleGroup);
        veryEasy.setToggleGroup(toggleGroup);
//

        saveHolder.getChildren().add(save);
        saveHolder.setPadding(new Insets(10,10,10,130));
        //give save location
        container.setSpacing(30);
        container.setPadding(new Insets(20,20,20,40));
        container.setStyle("-fx-background-color: #F9E79F");
        container.setMinSize(400,400);

        container.getChildren().addAll(veryHard,hard,average,easy,veryEasy,saveHolder);

        gridPane.getChildren().add(container);
        gridPane.setAlignment(Pos.CENTER);
    }

    public void setQuiz(Quiz quiz) {
        if(quiz.getHardshipLevel()==1)veryEasy.setSelected(true);
        if(quiz.getHardshipLevel()==2)easy.setSelected(true);
        if(quiz.getHardshipLevel()==3)average.setSelected(true);
        if(quiz.getHardshipLevel()==4)hard.setSelected(true);
        if(quiz.getHardshipLevel()==5)veryHard.setSelected(true);
    }

    public void addParticipant(){
        participants++;
    }

    public int getParticipants(){
        return participants;
    }

    public Button getSave(){
        return save;
    }

    public ToggleGroup getToggleGroup(){
        return toggleGroup;
    }

    public RadioButton getVeryHardButton(){
        return veryHard;
    }

    public RadioButton getHardButton(){
        return hard;
    }

    public RadioButton getAverageButton(){
        return average;
    }

    public RadioButton getEasyButton(){
        return easy;
    }

    public RadioButton getVeryEasyButton(){
        return veryEasy;
    }

    public GridPane getGridPane(){
        return gridPane;
    }

}
