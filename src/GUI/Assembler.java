package GUI;

import Logic.Quiz;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Assembler {

    Stage stage;
    Scene scene;
    public Start start = new Start();
    public StudentLogin studentLogin = new StudentLogin();
    public ManagerLogin managerLogin = new ManagerLogin();
    public StudentSignUp studentSignUp = new StudentSignUp();
    public ManagerSignUp managerSignUp = new ManagerSignUp();
    public ManagerPanel managerPanel = new ManagerPanel();
    public StudentPanel studentPanel = new StudentPanel();
    public AlertBox alertBox = new AlertBox();
    public AddManually addManually=new AddManually();
    public AddFromExcel addFromExcel;
    public QuestionMaking questionMaking=new QuestionMaking();
    public Charts charts = new Charts();
    public Correction correction=new Correction();
    public CreateTest createTest;
    public RemoveStudents removeStudents=new RemoveStudents();
    public ManageStudentsParticipation manageStudentsParticipation = new ManageStudentsParticipation();
    public ManageInformation manageInformation = new ManageInformation();
    public QuizInfo quizInfo = new QuizInfo();
    public QuizHistory quizHistory = new QuizHistory();
    public Poll poll = new Poll();
    public PrepareForTest prepareForTest = new PrepareForTest();
    public TakeTest takeTest = new TakeTest();
    public TwoOptionAlert twoOptionAlert=new TwoOptionAlert();
    public ArrayList<Chat> chats = new ArrayList<>();
    public Review review=new Review();
    public Results results = new Results();
    public ExitAlert exitAlert;

    public Assembler(Stage stage){
        this.stage = stage;
        showStartPage();
    }

    public void showStartPage(){
        stage.setTitle("سیستم آزمون یار");
        stage.setX(300);
        stage.setY(100);
        stage.setWidth(1300);
        stage.setHeight(800);
        stage.setScene(start.getScene());
        stage.show();
    }

    public void showManagerLogin(){
        stage.setTitle("ورود");
        stage.setScene(managerLogin.getScene());
    }

    public void showStudentLogin(){
        stage.setTitle("ورود");
        stage.setScene(studentLogin.getScene());
    }

    public void showManagerSignUp(){
        stage.setTitle("ثبت نام");
        stage.setScene(managerSignUp.getScene());
    }

    public void showStudentSignUp(){
        stage.setTitle("ثبت نام");
        stage.setScene(studentSignUp.getScene());
    }

    public void showManagerPanel(){
        BorderPane borderPane = new BorderPane();
        stage.setTitle("پنل مدیریت");
        borderPane.setLeft(managerPanel.getTreeView());
        Image image = new Image("GUI\\pics\\background.jpg");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1300,800,false,false,false,false));
        borderPane.setBackground(new Background(background));
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void showStudentPanel(){
        stage.setTitle("پنل دانشجویی");
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        Image image = new Image("GUI\\pics\\background.jpg");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1300,800,false,false,false,false));
        borderPane.setBackground(new Background(background));
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeAddManually(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setCenter(addManually.getAnchorPane());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeAddFromExcel(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        addFromExcel = new AddFromExcel();
        borderPane.setCenter(addFromExcel.getAnchorPane());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        scene = new Scene(borderPane);
        stage.setScene(scene);

    }

    public void mergeAddQuestions(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(questionMaking.getAnchorPane());
        scene = new Scene(borderPane);
        //scene.getStylesheets().addAll(this.getClass().getResource("background.css").toExternalForm());
        stage.setScene(scene);
    }

    public void mergeAddInfo(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(quizInfo.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void showChart(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(charts.getBorderPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeCorrection(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(correction.getBorderPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeManageStudentsParticipation(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(manageStudentsParticipation.getAnchorPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeManageInformation(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(manageInformation.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeQuizHistory(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(quizHistory.getAnchorPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergePoll(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(poll.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergePrepareForTest(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(prepareForTest.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }
    public void mergeTakeTest(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(takeTest.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void newChat(String quizName, String sender){
        Chat chat = new Chat(quizName,sender);
        chats.add(chat);
    }

    public void mergeChatForManager(String name){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(managerPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        for(Chat c : chats){
            if(c.getMessage().getQuizName() != null && c.getMessage().getQuizName().equals(name)){
                borderPane.setCenter(c.getBorderPane());
            }
        }
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeChatForStudent(String name){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        for(Chat c : chats){
            if(c.getMessage().getQuizName() != null && c.getMessage().getQuizName().equals(name)){
                borderPane.setCenter(c.getBorderPane());
            }
        }
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeReview(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(review.getBorderPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void mergeResults(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(studentPanel.getTreeView());
        borderPane.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");
        borderPane.setCenter(results.getGridPane());
        scene = new Scene(borderPane);
        stage.setScene(scene);
    }

    public void showExitAlert(){
        exitAlert = new ExitAlert();
    }

    public void loginAlertBox(){
        alertBox.display("خطا","نام کاربری یا رمز عبور صحیح نمی باشد",720,450);
    }

    public void takenUserNameAlertBox(){
        alertBox.display("خطا","نام کاربری قبلا انتخاب شده",700,450);
    }

    public void takenIdAlertBox(){
        alertBox.display("خطا","شماره دانشحویی قبلا ثبت شده است",700,450);
    }

    public void alreadyVoted(){
        alertBox.display("خطا","رای شما قبلا ثبت شده است",700,450);
    }

    public void notRegistered(){
        alertBox.display("خطا","برای استفاده از برنامه ابتدا ثبت نام کنید",700,450);
    }

    public void questionSaved(){
        alertBox.display(null,"ذخیره سوال با موفقیت انجام شد",700,450);
    }

    public void saveChanges(){
        alertBox.display("خطا","ابتدا آزمون را ذخیره کنید",700,450);
    }

    public void showCreateTest(){
        createTest = new CreateTest();
    }

    public void showTwoOptionsAlert(){twoOptionAlert=new TwoOptionAlert();}

    public QuestionMaking getQuestionMaking() {
        return questionMaking;
    }

    public AddManually getAddManually() {
        return addManually;
    }

    public Stage getStage() {
        return stage;
    }
}
