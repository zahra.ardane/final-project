package GUI;

import Logic.Quiz;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.ArrayList;

public class QuestionMaking {

    private int multipleQuestionsNumber=2;
    private int yesNoQuestionsNumbers=2;
    private int minWidth=1035;
    private int minHeight=800;
    private AnchorPane anchorPane=new AnchorPane();
    private AnchorPane rightAnchor=new AnchorPane();
    private AnchorPane yesNoPane=new AnchorPane();
    private AnchorPane leftAnchor=new AnchorPane();
    private AnchorPane bottomPane=new AnchorPane();
    private VBox vBox;
    private VBox vBox2;
    private VBox yesNoHolder;
    private RadioButton fourAnswer;
    private RadioButton twoAnswer;
    private RadioButton descriptive;
    private ToggleGroup toggleGroup;
    private ScrollPane scrollPane;
    private TextArea textArea;
    private TextArea grade;
    private ArrayList<TextArea> multiples=new ArrayList<>();
    private ArrayList<TextArea> yesNoQuestions=new ArrayList<>();
    private Button save;
    private Button add;
    private Button done;
    private Button next;
    private Button previous;
    private Button remove;
    private TextField hour;
    private TextField minute;
    private int currentQuestion;
    private Quiz quiz;

    public QuestionMaking(){
        anchorPane.setMinSize(minWidth,minHeight);

        textArea=new TextArea();
        textArea.setMinSize(800,40);
        textArea.setMaxSize(800,170);
        textArea.setWrapText(true);
        textArea.setLayoutX(190);
        textArea.setLayoutY(30);
        textArea.setText("");

        bottomPane.setLayoutX(190);
        bottomPane.setLayoutY(300);
        bottomPane.setMinWidth(800);
        bottomPane.setMinHeight(300);
        bottomPane.setStyle("-fx-background-color: #F9E79F");

        rightAnchor.setMinSize(140,300);
        //rightAnchor.setMaxSize(140,300);
        rightAnchor.setLayoutY(10);
        rightAnchor.setLayoutX(10);

        twoAnswer=new RadioButton("صحیح غلط");
        fourAnswer=new RadioButton("چند گزینه ای");
        descriptive=new RadioButton("تشریحی");
        descriptive.setSelected(true);

        toggleGroup=new ToggleGroup();

        twoAnswer.setToggleGroup(toggleGroup);
        fourAnswer.setToggleGroup(toggleGroup);
        descriptive.setToggleGroup(toggleGroup);

        twoAnswer.setLayoutX(300);
        twoAnswer.setLayoutY(300);
        fourAnswer.setLayoutX(350);
        fourAnswer.setLayoutY(350);
        descriptive.setLayoutX(400);
        descriptive.setLayoutY(400);

        TextArea first=new TextArea();
        first.setMaxSize(600,50);
        first.setMinSize(600,50);
        first.setPromptText("گزینه "+1);
        multiples.add(first);
        //first.setMinSize(100,10);
        TextArea second=new TextArea();
        second.setMinSize(600,50);
        second.setMaxSize(600,50);
        second.setPromptText("گزینه 2");
        multiples.add(second);

        TextArea first2=new TextArea();
        first2.setMaxSize(600,50);
        first2.setMaxSize(600,50);
        first2.setPromptText("گزینه "+1);
        yesNoQuestions.add(first2);
        //first.setMinSize(100,10);
        TextArea second2=new TextArea();
        second2.setMinSize(600,50);
        second2.setMaxSize(600,50);
        second2.setPromptText("گزینه 2");
        yesNoQuestions.add(second2);

        vBox2=new VBox(first,second);
        vBox2.setSpacing(50);
        leftAnchor.setMinSize(100,500);
        leftAnchor.getChildren().add(vBox2);
        leftAnchor.setLayoutX(180);
        leftAnchor.setLayoutY(10);
        leftAnchor.setVisible(false);

        yesNoPane.setMinSize(100,500);
        yesNoPane.setLayoutX(180);
        yesNoPane.setLayoutY(10);
        yesNoPane.setVisible(false);
        yesNoHolder=new VBox();
        yesNoHolder.setSpacing(50);
        yesNoHolder.getChildren().addAll(first2,second2);
        yesNoPane.getChildren().add(yesNoHolder);

        grade=new TextArea();
        grade.setPromptText("نمره");
        grade.setMaxSize(60,40);
        grade.setMinSize(60,40);
        grade.setLayoutX(65);
        grade.setLayoutY(400);
        grade.setText("");

        add=new Button("افزودن گزینه");
        add.setFont(new Font("Calibri Light", 20));
        add.setStyle("-fx-background-color: #FCF3CF");
        add.setLayoutY(300);
        add.setLayoutX(40);

        next=new Button("سوال بعد");
        next.setFont(new Font("Calibri Light", 20));
        next.setStyle("-fx-background-color: #FCF3CF");
        next.setLayoutY(600);
        next.setLayoutX(40);

        previous=new Button("سوال قبل");
        previous.setFont(new Font("Calibri Light", 20));
        previous.setStyle("-fx-background-color: #FCF3CF");
        previous.setLayoutY(650);
        previous.setLayoutX(40);

        save=new Button("ذخیره سوال");
        save.setFont(new Font("Calibri Light", 20));
        save.setStyle("-fx-background-color: #FCF3CF");
        save.setLayoutX(40);
        save.setLayoutY(120);

        remove=new Button("حذف سوال");
        remove.setFont(new Font("Calibri Light", 20));
        remove.setStyle("-fx-background-color: #FCF3CF");
        remove.setLayoutX(40);
        remove.setLayoutY(50);

        done=new Button("ذخیره آزمون");
        done.setFont(new Font("Calibri Light", 20));
        done.setStyle("-fx-background-color: #FCF3CF");
        done.setLayoutX(40);
        done.setLayoutY(190);

        hour=new TextField();
        hour.setPromptText("ساعت آزمون");
        hour.setFont(new Font("Calibri Light", 20));
        hour.setLayoutX(20);
        hour.setLayoutY(450);
        hour.setMaxWidth(150);
        hour.setText("");

        minute=new TextField();
        minute.setPromptText("دقیقه آزمون");
        minute.setFont(new Font("Calibri Light", 20));
        minute.setLayoutX(20);
        minute.setLayoutY(500);
        minute.setMaxWidth(150);
        minute.setText("");


        vBox=new VBox(descriptive,fourAnswer,twoAnswer);
        vBox.setSpacing(50);
        vBox.setMaxWidth(140);
        scrollPane=new ScrollPane();
        scrollPane.setContent(bottomPane);
        scrollPane.setMinSize(800,450);
        scrollPane.setMaxSize(800,450);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setLayoutX(190);
        scrollPane.setLayoutY(250);
        rightAnchor.getChildren().add(vBox);
        bottomPane.getChildren().addAll(yesNoPane,rightAnchor,leftAnchor);
        anchorPane.getChildren().addAll(scrollPane,textArea,save,add,done,remove,next,previous,grade,hour,minute);

        //scrollPane=new ScrollPane();
        //scrollPane.setContent(bottomPane);
        //scrollPane.setMinSize(300,800);
        //scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        //scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);


        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(1035,800,false,false,false,true));
        anchorPane.setBackground(new Background(bgImg));

//        scrollPane.setId("pane");
    }

    public void addMultipleAnswer(){
        //if(multipleQuestionsNumber>=multiples.size()){
        TextArea newTextArea=new TextArea();
        multipleQuestionsNumber++;
        newTextArea.setPromptText("گزینه "+multipleQuestionsNumber);
        newTextArea.setMinSize(600,50);
        newTextArea.setMaxSize(600,50);
        multiples.add(newTextArea);
        vBox2.getChildren().add(newTextArea);//}
       /* else{
            for(int i=0;i<multipleQuestionsNumber;i++)multiples.get(i).setVisible(true);
            multiples.get(multipleQuestionsNumber-1).setText(null);
            for(int i=multipleQuestionsNumber;i<multiples.size();i++)multiples.get(i).setVisible(false);

        }*/
    }

    public void addYesNoAnswer(){
        //if(yesNoQuestionsNumbers>=yesNoQuestions.size()) {
        TextArea newTextArea = new TextArea();
        yesNoQuestionsNumbers++;
        newTextArea.setPromptText("گزینه " + yesNoQuestionsNumbers);
        newTextArea.setMinSize(600, 50);
        newTextArea.setMaxSize(600, 50);
        yesNoQuestions.add(newTextArea);
        yesNoHolder.getChildren().add(newTextArea);
        //}
        /*else{
            for(int i=0;i<yesNoQuestionsNumbers;i++) {
                yesNoQuestions.get(i).setVisible(true);
            }
            yesNoQuestions.get(yesNoQuestionsNumbers-1).setText(null);
            for(int i=yesNoQuestionsNumbers;i<yesNoQuestions.size();i++)yesNoQuestions.get(i).setVisible(false);
        }*/
    }
    public void fixQuestionMaking() {
        if (quiz != null&&quiz.getName()!=null) {
            if (currentQuestion == quiz.getQuestions().size()) {
                setTextsNull();
                getTextArea().setText("");
                setMultipleQuestionsNumber(2);
                setMultipleQuestionsNumber(2);
                getFourAnswer().setSelected(false);
                getTwoAnswer().setSelected(false);
                getDescriptive().setSelected(true);

            } else {
                setTextsNull();
                vBox2.getChildren().clear();
                yesNoHolder.getChildren().clear();
                if(quiz.getQuestions().get(currentQuestion).getContext()!=null)
                textArea.setText(quiz.getQuestions().get(currentQuestion).getContext());

                if (quiz.getQuestions().get(currentQuestion).getType() == 2) {
                    ArrayList<TextArea> multiples = new ArrayList<>();
                    ArrayList<TextArea> yesNo = new ArrayList<>();
                    setMultipleQuestionsNumber(quiz.getQuestions().get(currentQuestion).getOptions().size());
                    setYesNoQuestionsNumbers(2);
                    for (int i = 0; i < quiz.getQuestions().get(currentQuestion).getOptions().size(); i++) {
                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        if(quiz.getQuestions().get(currentQuestion).getOptions().get(i)!=null)
                        textArea.setText(quiz.getQuestions().get(currentQuestion).getOptions().get(i));
                        textArea.setPromptText("گزینه " + (i + 1));
                        multiples.add(textArea);
                        vBox2.getChildren().add(textArea);
                    }
                    for (int i = 0; i < 2; i++) {
                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        textArea.setPromptText("گزینه " + (i + 1));
                        textArea.setText("");
                        yesNo.add(textArea);
                        yesNoHolder.getChildren().add(textArea);
                    }
                    this.multiples = multiples;
                    yesNoQuestions = yesNo;
                    fourAnswer.setSelected(true);
                }
                if (quiz.getQuestions().get(currentQuestion).getType() == 3) {
                    ArrayList<TextArea> multiples = new ArrayList<>();
                    ArrayList<TextArea> yesNo = new ArrayList<>();
                    setMultipleQuestionsNumber(2);
                    setYesNoQuestionsNumbers(quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().size());
                    for (int i = 0; i < quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().size(); i++) {

                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        if(quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().get(i)!=null)
                        textArea.setText(quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().get(i));
                        textArea.setPromptText("گزینه " + (i + 1));
                        yesNo.add(textArea);
                        yesNoHolder.getChildren().add(textArea);
                    }
                    for (int i = 0; i < 2; i++) {
                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        textArea.setPromptText("گزینه " + (i + 1));
                        textArea.setText("");
                        multiples.add(textArea);
                        vBox2.getChildren().add(textArea);
                    }
                    this.multiples = multiples;
                    yesNoQuestions = yesNo;
                    twoAnswer.setSelected(true);
                }
                if (quiz.getQuestions().get(currentQuestion).getType() == 1) {
                    setTextsNull();
                    ArrayList<TextArea> yesNo = new ArrayList<>();
                    ArrayList<TextArea> multiples = new ArrayList<>();
                    vBox2.getChildren().clear();
                    yesNoHolder.getChildren().clear();
                    if(quiz.getQuestions().get(currentQuestion).getContext()!=null)
                    textArea.setText(quiz.getQuestions().get(currentQuestion).getContext());
                    for (int i = 0; i < 2; i++) {
                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        textArea.setText("");
                        textArea.setPromptText("گزینه " + (i + 1));
                        multiples.add(textArea);
                        vBox2.getChildren().add(textArea);
                    }
                    for (int i = 0; i < 2; i++) {
                        TextArea textArea = new TextArea();
                        textArea.setMaxSize(600,50);
                        textArea.setMinSize(600,50);
                        textArea.setText("");
                        textArea.setPromptText("گزینه " + (i + 1));
                        yesNo.add(textArea);
                        yesNoHolder.getChildren().add(textArea);
                    }
                    this.multiples = multiples;
                    yesNoQuestions = yesNo;
                    descriptive.setSelected(true);
                }
                if(quiz.getQuestions().get(currentQuestion).getGrade()!=0)
                    grade.setText(String.valueOf(quiz.getQuestions().get(currentQuestion).getGrade()));
                if(quiz.getQuestions().get(currentQuestion).getHour()!=0)
                    hour.setText(String.valueOf(quiz.getQuestions().get(currentQuestion).getHour()));
                if(quiz.getQuestions().get(currentQuestion).getMinuet()!=0)
                    minute.setText(String.valueOf(quiz.getQuestions().get(currentQuestion).getMinuet()));
            }
        }
    }

    public void setMultipleQuestionsNumber(int multipleQuestionsNumber) {
        this.multipleQuestionsNumber = multipleQuestionsNumber;
    }

    public void setYesNoQuestionsNumbers(int yesNoQuestionsNumbers) {
        this.yesNoQuestionsNumbers = yesNoQuestionsNumbers;
    }
    public void setTextsNull(){
        for(int i=0;i<multiples.size();i++)multiples.get(i).setText("");
        for(int i=0;i<yesNoQuestions.size();i++)yesNoQuestions.get(i).setText("");
        grade.setText("");
        hour.setText("");
        minute.setText("");
    }
    public void nextQuestion(){
        if(quiz!=null&&quiz.getName()!=null) {
            if (currentQuestion + 1 <= quiz.getQuestions().size()) {
                currentQuestion++;
                fixQuestionMaking();
            } else {
                AlertBox alertBox = new AlertBox();
                alertBox.display("خطا","ابتدا سوال آخر را ذخیره کنید", 700, 450);
            }
        }
        else{
            AlertBox alertBox=new AlertBox();
            alertBox.display("خطا","آزمون جدیدی وجود ندارد",720,400);
        }
    }
    public void previousQuestion() {
        if (quiz != null&&quiz.getName()!=null) {
            if(currentQuestion==0){
                AlertBox alertBox = new AlertBox();
                alertBox.display("خطا", "سوال قبل وجود ندارد", 720, 400);
            }
            else if(quiz.getQuestions().size()==currentQuestion){
                int sw=0;
                if(fourAnswer.isSelected()){
                    for(int i=0;i<multiples.size()&&sw==0;i++){
                        if(!multiples.get(i).getText().equals("")){
                            AlertBox alertBox = new AlertBox();
                            sw=1;
                            alertBox.display("خطا","سوال آخر را ذخیره کنید",720,400);
                        }
                    }

                }
                else if(twoAnswer.isSelected()){
                    for(int i=0;i<yesNoQuestions.size()&&sw==0;i++){
                        if(!yesNoQuestions.get(i).getText().equals("")){
                            AlertBox alertBox = new AlertBox();
                            sw=1;
                            alertBox.display("خطا","سوال آخر را ذخیره کنید",720,400);
                        }
                    }
                }

                if(sw==0&&(!textArea.getText().equals("")||!grade.getText().equals("")||!hour.getText().equals("")||!minute.getText().equals(""))){

                    AlertBox alertBox = new AlertBox();
                    sw=1;
                    alertBox.display("خطا","سوال آخر را ذخیره کنید",720,400);
                }
                if(sw==0){
                    if (currentQuestion - 1 >= 0) {
                        currentQuestion--;
                        fixQuestionMaking();
                    } else {
                        AlertBox alertBox = new AlertBox();
                        alertBox.display("خطا", "سوال قبل وجود ندارد", 720, 400);

                    }
                }
            }
            else {
                if (currentQuestion - 1 >= 0) {
                    currentQuestion--;
                    fixQuestionMaking();
                }
            }
        }
        else{
            AlertBox alertBox=new AlertBox();
            alertBox.display("خطا","آزمون جدیدی وجود ندارد",720,400);
        }
    }
    public void newPage(){
        setTextsNull();
        ArrayList<TextArea>yesNo=new ArrayList<>();
        ArrayList<TextArea>multiples=new ArrayList<>();
        vBox2.getChildren().clear();
        yesNoHolder.getChildren().clear();
        //textArea.setText(quiz.getQuestions().get(currentQuestion).getContext());
        textArea.setText("");
        for(int i=0;i<2;i++){
            TextArea textArea=new TextArea();
            textArea.setMaxSize(600,50);
            textArea.setMinSize(600,50);
            textArea.setPromptText("گزینه "+(i+1));
            textArea.setText("");
            multiples.add(textArea);
            vBox2.getChildren().add(textArea);
        }
        for(int i=0;i<2;i++){
            TextArea textArea=new TextArea();
            textArea.setMaxSize(600,50);
            textArea.setMinSize(600,50);
            textArea.setPromptText("گزینه "+(i+1));
            textArea.setText("");
            yesNo.add(textArea);
            yesNoHolder.getChildren().add(textArea);
        }
        this.multiples=multiples;
        yesNoQuestions=yesNo;
    }

    public TextArea getGrade() {
        return grade;
    }

    public TextField getHour() {
        return hour;
    }

    public TextField getMinute() {
        return minute;
    }

    public Button getNext() {
        return next;
    }

    public Button getPrevious() {
        return previous;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public Button getRemove() {
        return remove;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    public RadioButton getFourAnswer() {
        return fourAnswer;
    }

    public ArrayList<TextArea> getMultiples() {
        return multiples;
    }

    public Button getSave() {
        return save;
    }

    public ToggleGroup getToggleGroup() {
        return toggleGroup;
    }

    public AnchorPane getLeftAnchor() {
        return leftAnchor;
    }

    public int getMultipleQuestionsNumber() {
        return multipleQuestionsNumber;
    }

    public AnchorPane getYesNoPane() {
        return yesNoPane;
    }

    public RadioButton getTwoAnswer() {
        return twoAnswer;
    }

    public int getYesNoQuestionsNumbers() {
        return yesNoQuestionsNumbers;
    }

    public ArrayList<TextArea> getYesNoQuestions() {
        return yesNoQuestions;
    }

    public Button getAdd() {
        return add;
    }

    public Button getDone() {
        return done;
    }

    public RadioButton getDescriptive() {
        return descriptive;
    }

}
