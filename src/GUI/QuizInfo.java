package GUI;

import Logic.Question;
import Logic.Quiz;
import Logic.Student;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.ArrayList;

public class QuizInfo {

    private GridPane container = new GridPane();
    private AnchorPane subContainer = new AnchorPane();
    private CheckBox shuffle;
    private TextField startHour;
    private TextField startMinute;
    private TextField finishHour;
    private TextField finishMinute;
    private CheckBox oneByOneQuestions;
    private boolean isOneByOneQSelected=false;
    private ArrayList<TextField> textFields = new ArrayList<>();
    private ArrayList<Student>students=new ArrayList<>();
    private ArrayList<CheckBox>checkBoxes=new ArrayList<>();
    private Label label=new Label("محروم کردن دانشجویان");
    private VBox vBox=new VBox();
    private Quiz quiz;
    private TextField yearTextField;
    private TextField monthTextField;
    private TextField dayTextField;
    private CheckBox review;
    private Button save=new Button("ذخیره تغیرات");
    private Button done=new Button("ذخیره آزمون");


    public QuizInfo(){

        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        container.setBackground(new Background(bgImg));

        shuffle = new CheckBox("نمایش سوال ها به صورت به هم ریخته");
        shuffle.setFont(new Font("Calibri Light", 20));
        shuffle.setLayoutX(40);
        shuffle.setLayoutY(40);

        Label startTime = new Label("زمان شروع");
        startTime.setFont(new Font("Calibri Light", 20));
        startTime.setLayoutX(40);
        startTime.setLayoutY(95);

        Label finishTime = new Label("مدت آزمون");
        finishTime.setFont(new Font("Calibri Light", 20));
        finishTime.setLayoutX(40);
        finishTime.setLayoutY(135);

        startHour = new TextField();
        startHour.setFont(new Font("Calibri Light", 20));
        startHour.setLayoutX(150);
        startHour.setLayoutY(90);
        startHour.setMaxSize(50,20);
        startHour.setPromptText("00");

        Label label1 = new Label(":");
        label1.setFont(new Font("Calibri Light", 20));
        label1.setLayoutX(212);
        label1.setLayoutY(95);

        startMinute = new TextField();
        startMinute.setFont(new Font("Calibri Light", 20));
        startMinute.setLayoutX(230);
        startMinute.setLayoutY(90);
        startMinute.setMaxSize(50,20);
        startMinute.setPromptText("00");

        finishHour = new TextField();
        finishHour.setFont(new Font("Calibri Light", 20));
        finishHour.setLayoutX(150);
        finishHour.setLayoutY(130);
        finishHour.setMaxSize(50,20);
        finishHour.setPromptText("00");

        Label label2 = new Label(":");
        label2.setFont(new Font("Calibri Light", 20));
        label2.setLayoutX(212);
        label2.setLayoutY(135);

        finishMinute = new TextField();
        finishMinute.setFont(new Font("Calibri Light", 20));
        finishMinute.setLayoutX(230);
        finishMinute.setLayoutY(130);
        finishMinute.setMaxSize(50,20);
        finishMinute.setPromptText("00");
        Label date = new Label("تاریخ امتحان");
        date.setFont(new Font("Calibri Light", 20));
        date.setLayoutX(40);
        date.setLayoutY(280);

        yearTextField = new TextField();
        yearTextField.setLayoutX(150);
        yearTextField.setLayoutY(280);
        yearTextField.setMaxSize(50,20);
        yearTextField.setPromptText("1399");

        Label label3 = new Label("/");
        label3.setFont(new Font("Calibri Light", 20));
        label3.setLayoutX(212);
        label3.setLayoutY(280);

        monthTextField = new TextField();
        monthTextField.setLayoutX(230);
        monthTextField.setLayoutY(280);
        monthTextField.setMaxSize(50,20);
        monthTextField.setPromptText("01");


        Label label4 = new Label("/");
        label4.setFont(new Font("Calibri Light", 20));
        label4.setLayoutX(292);
        label4.setLayoutY(280);

        dayTextField = new TextField();
        dayTextField.setLayoutX(310);
        dayTextField.setLayoutY(280);
        dayTextField.setMaxSize(50,20);
        dayTextField.setPromptText("01");

        oneByOneQuestions = new CheckBox("زمان بندی هر سوال به صورت مجزا");
        oneByOneQuestions.setFont(new Font("Calibri Light", 20));
        oneByOneQuestions.setLayoutX(40);
        oneByOneQuestions.setLayoutY(180);

        review = new CheckBox("قابلیت مرور");
        review.setFont(new Font("Calibri Light", 20));
        review.setLayoutX(40);
        review.setLayoutY(230);

        save.setFont(new Font("Calibri Light", 20));
        save.setStyle("-fx-background-color: #FCF3CF");
        save.setLayoutY(500);
        save.setLayoutX(40);

        done.setFont(new Font("Calibri Light", 20));
        done.setStyle("-fx-background-color: #FCF3CF");
        done.setLayoutY(500);
        done.setLayoutX(260);

        subContainer.getChildren().addAll(done,save,shuffle,startTime,finishTime,startHour,label1,startMinute,review
                ,finishHour,label2,label4,label3,finishMinute,oneByOneQuestions,yearTextField,date,dayTextField,monthTextField);
        subContainer.setStyle("-fx-background-color: #F9E79F");
        subContainer.setMinSize(800,600);

        ScrollPane scrollPane=new ScrollPane();
        scrollPane.setLayoutX(450);
        scrollPane.setLayoutY(10);
        scrollPane.setMinSize(300,570);
        scrollPane.setMaxSize(300,570);
        vBox.setPadding(new Insets(10,10,10,70));
        vBox.setSpacing(30);
        scrollPane.setContent(vBox);

        subContainer.getChildren().add(scrollPane);

        container.getChildren().add(subContainer);
        container.setAlignment(Pos.CENTER);

    }

    public void addStudents(ArrayList<Student>students){
        vBox.getChildren().clear();
        vBox.getChildren().add(label);
        ArrayList<CheckBox>checkBoxes=new ArrayList<>();
        this.students=students;
        for(int i=0;i<students.size();i++){
            CheckBox checkBox=new CheckBox((students.get(i).getStudentId())+"  "+String.valueOf(students.get(i).getName())+
                    "  "+students.get(i).getSurName());
            for(int j=0;j<students.get(i).getQuizzes().size();j++){
                if(students.get(i).getQuizzes().get(j).getName().equals(quiz.getName())){
                    checkBox.setSelected(students.get(i).getQuizzes().get(j).getIsBanned());
                    break;
                }
            }
            checkBoxes.add(checkBox);
            vBox.getChildren().add(checkBox);
        }
        this.checkBoxes=checkBoxes;

    }

    public GridPane getGridPane(){
        return container;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        if(quiz.getYear()!=0)getYearTextField().setText(String.valueOf(quiz.getYear()));
        else getYearTextField().setText(null);
        if(quiz.getMonth()!=0)getMonthTextField().setText(String.valueOf(quiz.getMonth()));
        else getMonthTextField().setText(null);
        if(quiz.getDay()!=0)getDayTextField().setText(String.valueOf(quiz.getDay()));
        else getDayTextField().setText(null);
        if(quiz.getStartHour()!=0)getStartHour().setText(String.valueOf(quiz.getStartHour()));
        else getStartHour().setText(null);
        if(quiz.getStartMinute()!=0)getStartMinute().setText(String.valueOf(quiz.getStartMinute()));
        else getStartMinute().setText(null);
        if(quiz.getStopHour()!=0)getFinishHour().setText(String.valueOf(quiz.getStopHour()));
        else getFinishHour().setText(null);
        if(quiz.getStopMinute()!=0)getFinishMinute().setText(String.valueOf(quiz.getStopMinute()));
        else getFinishMinute().setText(null);
        addStudents(quiz.getStudents());
    }
    public void setNull(){
        getYearTextField().setText(null);
        getDayTextField().setText(null);
        getMonthTextField().setText(null);
        getFinishHour().setText(null);
        getStartMinute().setText(null);
        getFinishMinute().setText(null);
        getStartHour().setText(null);
        getOneByOneQuestions().setSelected(false);
        getReview().setSelected(false);
        getShuffle().setSelected(false);
        vBox.getChildren().clear();
    }

    public Button getSave() {
        return save;
    }

    public TextField getFinishHour() {
        return finishHour;
    }

    public TextField getFinishMinute() {
        return finishMinute;
    }

    public TextField getStartHour() {
        return startHour;
    }

    public TextField getStartMinute() {
        return startMinute;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }

    public TextField getDayTextField() {
        return dayTextField;
    }

    public TextField getMonthTextField() {
        return monthTextField;
    }

    public TextField getYearTextField() {
        return yearTextField;
    }

    public CheckBox getReview() {
        return review;
    }

    public CheckBox getOneByOneQuestions() {
        return oneByOneQuestions;
    }

    public CheckBox getShuffle() {
        return shuffle;
    }

    public void setShuffle(CheckBox shuffle) {
        this.shuffle = shuffle;
    }

    public Button getDone() {
        return done;
    }
}
