package GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {

    Stage window=new Stage();

    public AlertBox(){
        window.initModality(Modality.APPLICATION_MODAL);
    }
    public void display(String stageName,String context, int x, int y){
        window.setMinHeight(150);
        window.setMinWidth(500);
        window.setX(x);
        window.setY(y);

        window.setTitle(stageName);
        Label label=new Label();
        label.setText(context);
        label.setFont(new Font("Calibri Light", 23));

        VBox vBox = new VBox(10);
        vBox.getChildren().add(label);
        vBox.setAlignment(Pos.CENTER);

        Image image = new Image("GUI\\pics\\alert.png");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(500,150,false,false,false,false));
        vBox.setBackground(new Background(background));

        Scene scene=new Scene(vBox);
        window.setScene(scene);
        window.show();
    }
    public void setStageTitle(String title){
        window.setTitle(title);
    }
}
