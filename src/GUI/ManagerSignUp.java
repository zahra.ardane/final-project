package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Font;

public class ManagerSignUp extends SignUp {

    public ManagerSignUp() {

        super();
        signUpButton = new Button("ثبت نام");
        signUpButton.setFont(new Font("Calibri Light", 27));
        signUpButton.setStyle("-fx-background-color: #FCF3CF");
        signUpButton.setLayoutY(290);
        signUpButton.setLayoutX(270);

        back.setLayoutY(290);
        back.setLayoutX(90);

        anchor2.setMinSize(480,400);

        setLayoutCoordinates(300,150,userName);
        setLayoutCoordinates(60,145,userNameTextField);
        setLayoutCoordinates(300,200,password);
        setLayoutCoordinates(60,195,passwordTextField);
        setSignUpButton(signUpButton);
        anchor2.getChildren().add(getSignUpButton());

        //anchor1.getChildren().add(anchor2);
        //scene = new Scene(anchor1);

    }

}
