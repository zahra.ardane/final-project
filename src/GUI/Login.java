package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public abstract class Login {
    private AnchorPane anchorPane=new AnchorPane();
    private AnchorPane anchorPane1=new AnchorPane();
    private Scene scene;
    private Button enterButton;
    private Button registerButton;
    private PasswordField passwordField;
    private TextField accountNumberField;
    private Label title;

    public Login(){



        anchorPane.setMinSize(1300,800);
        anchorPane1.setStyle("-fx-background-color: #F9E79F");
        anchorPane1.setLayoutX(360);
        anchorPane1.setLayoutY(250);
        anchorPane1.setMinSize(600,300);

        accountNumberField =new TextField();
        accountNumberField.setPromptText("نام کاربری");
        accountNumberField.setFont(new Font("Calibri Light", 20));

        passwordField =new PasswordField();
        passwordField.setPromptText("رمز عبور");
        passwordField.setFont(new Font("Calibri Light", 20));


        passwordField.setFocusTraversable(false);
        accountNumberField.setFocusTraversable(false);
        accountNumberField.setLayoutX(180);
        accountNumberField.setLayoutY(80);
        passwordField.setLayoutX(180);
        passwordField.setLayoutY(130);

        title=new Label();
        title.setFont(new Font("Calibri Light", 30));;
        title.setLayoutY(10);

        anchorPane1.setOpacity(0.9);
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(1300,800,false,false,false,false));


        anchorPane.setBackground(new Background(bgImg));
        anchorPane1.getChildren().addAll(accountNumberField, passwordField,title);
        anchorPane.getChildren().addAll(anchorPane1);
        scene=new Scene(anchorPane);
    }


    public void setEnterButton(Button enterButton) {
        this.enterButton = enterButton;
    }

    public Scene getScene() {
        return scene;
    }

    public Button getEnterButton() {
        return enterButton;
    }

    public Button getRegisterButton() {
        return registerButton;
    }

    public Label getTitle() {
        return title;
    }

    public PasswordField getPasswordField() {
        return passwordField;
    }

    public TextField getAccountNumberField() {
        return accountNumberField;
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    public AnchorPane getAnchorPane1() {
        return anchorPane1;
    }

    public void setRegisterButton(Button registerButton) {
        this.registerButton = registerButton;
    }
}
