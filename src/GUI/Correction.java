package GUI;

import Logic.Answer;
import Logic.Quiz;
import Logic.Student;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;


public class Correction {
    BorderPane borderPane=new BorderPane();
    VBox vBox=new VBox();
    Student student;
    Quiz quiz;
    private VBox vBox3=new VBox();
    private int currentQuestion;
    private TextArea textArea=new TextArea();
    private TextArea answer=new TextArea();
    private Button next;
    private Button previous;
    public Button openFile;
    private Button isGraded=new Button("محاسبه نمره نهایی");
    private Button save=new Button("دخیره");
    private ArrayList<Button> studentsButtons;
    private ArrayList<Student>students;
    private ScrollPane scrollPane2=new ScrollPane();
    ScrollPane scrollPane;
    HBox hbox=new HBox();
    TextField grade=new TextField();

    public Correction(){
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(1035,800,false,false,false,true));
        borderPane.setBackground(new Background(bgImg));
        vBox3.setStyle("-fx-background-color: #F9E79F");
        vBox3.setPadding(new Insets(0,0,0,20));
        scrollPane2.setMaxSize(300,800);
        borderPane.setMinSize(1050,800);
        AnchorPane right=new AnchorPane();
        AnchorPane top=new AnchorPane();
        right.setMinSize(100,800);


        scrollPane=new ScrollPane();
        scrollPane.setContent(vBox);
        borderPane.setRight(right);
        borderPane.setCenter(scrollPane);
        openFile=new Button("فایل");
        next=new Button();

        previous=new Button();
        next.setText("سوال بعد");
        previous.setText("سوال قبل");
        previous.setStyle("-fx-background-color: #FCF3CF");
        next.setStyle("-fx-background-color: #FCF3CF");
        save.setStyle("-fx-background-color: #FCF3CF");
        isGraded.setStyle("-fx-background-color: #FCF3CF");
        hbox.setSpacing(630);
        hbox.getChildren().addAll(next,previous);


        SimpleIntegerProperty count = new SimpleIntegerProperty(textArea.getPrefRowCount());
        //int rowHeight = 10;

        textArea.prefHeightProperty().bindBidirectional(count);
        textArea.minHeightProperty().bindBidirectional(count);
        /*textArea.scrollTopProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldVal, Number newVal) {
                if(newVal.intValue() > rowHeight){
                    count.setValue(count.get() + newVal.intValue());
                }
            }
        });*/
    }
    public void fixCorrection(){
        vBox3.getChildren().clear();

        VBox newVbox=new VBox();
        AnchorPane anchorPane=new AnchorPane();
        AnchorPane anchorPane1=new AnchorPane();
        anchorPane1.setStyle("-fx-background-color: #FCF3CF");
        anchorPane.setStyle("-fx-background-color: #FCF3CF");
        anchorPane.setMaxWidth(800);
        anchorPane.setMinHeight(50);
        anchorPane.scaleYProperty();
        anchorPane1.setMaxWidth(800);
        anchorPane1.setMinHeight(50);
        newVbox.setSpacing(30);
        newVbox.setPadding(new Insets(20,10,10,5));
        if(quiz.getQuestions().get(currentQuestion).getType()==1){
            Text text=new Text(quiz.getQuestions().get(currentQuestion).getContext());
            text.setWrappingWidth(800);
            text.setLayoutY(20);
            anchorPane.getChildren().add(text);
            text.setTextAlignment(TextAlignment.RIGHT);
            Text text1=new Text();
            if(quiz.getAnswers().size()>currentQuestion) {
                text1.setText(quiz.getAnswers().get(currentQuestion).getAnswer());
            }
            else {
                Answer answer=new Answer();
                quiz.getAnswers().add(answer);
                text1.setText(null);
            }
            text1.setLayoutY(20);
            text1.setWrappingWidth(800);
            text1.setTextAlignment(TextAlignment.RIGHT);
            anchorPane1.getChildren().add(text1);
            newVbox.getChildren().add(anchorPane);
            newVbox.getChildren().add(anchorPane1);
            if(quiz.getAnswers().get(currentQuestion).getImageUrl()!=null){
                newVbox.getChildren().add(openFile);
            }
            grade.setFocusTraversable(false);
            if(quiz.getAnswers().get(currentQuestion).getGrade()==0)grade.setText(null);
            else {
                if(quiz.getAnswers().size()>currentQuestion){
                    if(quiz.getAnswers().get(currentQuestion)!=null){
                        grade.setText(String.valueOf(quiz.getAnswers().get(currentQuestion).getGrade()));
                    }
                }

            }
            grade.setMinSize(100,100);
            grade.setMaxSize(100,100);
            newVbox.getChildren().addAll(grade,isGraded,save,hbox);
        }
        if(quiz.getQuestions().get(currentQuestion).getType()==2){
            Text text=new Text(quiz.getQuestions().get(currentQuestion).getContext());
            text.setWrappingWidth(800);
            text.setLayoutY(20);
            anchorPane.getChildren().add(text);
            text.setTextAlignment(TextAlignment.RIGHT);

            newVbox.getChildren().add(anchorPane);
            VBox vBox1=new VBox();
            vBox1.setSpacing(20);
            for(int i=0;i<quiz.getQuestions().get(currentQuestion).getOptions().size();i++){
                RadioButton radioButton=new RadioButton();
                radioButton.setText(quiz.getQuestions().get(currentQuestion).getOptions().get(i));
                if(quiz.getAnswers().size()<=currentQuestion){
                    Answer answer=new Answer();
                    quiz.getAnswers().add(answer);
                }
                if(quiz.getAnswers().get(currentQuestion).getAnswer()!=null){
                if(quiz.getAnswers().get(currentQuestion).getAnswer().equals(radioButton.getText()))radioButton.setSelected(true);}
                radioButton.setDisable(true);
                anchorPane1.getChildren().addAll(radioButton);
                vBox1.getChildren().add(radioButton);
            }
            vBox1.setMaxWidth(800);

            vBox1.setStyle("-fx-background-color: #FCF3CF");
            newVbox.getChildren().add(vBox1);
            grade.setFocusTraversable(false);
            if(quiz.getAnswers().get(currentQuestion).getGrade()!=0)grade.setText(null);
            else {
                if(quiz.getAnswers().size()>currentQuestion){
                    if(quiz.getAnswers().get(currentQuestion)!=null){
                        grade.setText(String.valueOf(quiz.getAnswers().get(currentQuestion).getGrade()));
                    }
                }

            }
            grade.setMinSize(100,100);
            grade.setMaxSize(100,100);
            newVbox.getChildren().addAll(grade,isGraded,save,hbox);

        }
        if(quiz.getQuestions().get(currentQuestion).getType()==3){
            Text text1=new Text(quiz.getQuestions().get(currentQuestion).getContext());
            text1.setWrappingWidth(800);
            text1.setLayoutY(20);
            anchorPane.getChildren().add(text1);
            text1.setTextAlignment(TextAlignment.RIGHT);
            AnchorPane anchorPane2=new AnchorPane();
            anchorPane2.setMaxWidth(800);
            VBox vBox1=new VBox();
            vBox1.setSpacing(20);

            newVbox.getChildren().add(anchorPane);
            for(int i=0;i<quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().size();i++){
                HBox newHBox=new HBox();
                hbox.setMinWidth(800);
                newHBox.setSpacing(50);

                Text text=new Text();
                text.setText(quiz.getQuestions().get(currentQuestion).getTrueFalsePhrases().get(i));
                RadioButton radioButton=new RadioButton("صحیح");
                RadioButton radioButton1=new RadioButton("غلط");
                if(quiz.getAnswers().size()<=currentQuestion){
                    Answer answer=new Answer();
                    quiz.getAnswers().add(answer);
                }
                if(quiz.getAnswers().get(currentQuestion).getYesNoAnswers().size()<2){
                    for(int j=quiz.getAnswers().get(currentQuestion).getYesNoAnswers().size();j<2;j++){
                        quiz.getAnswers().get(currentQuestion).getYesNoAnswers().add(j,0);
                    }
                }
                if(quiz.getAnswers().get(currentQuestion).getYesNoAnswers().get(i)==2)radioButton.setSelected(true);
                if(quiz.getAnswers().get(currentQuestion).getYesNoAnswers().get(i)==1)radioButton1.setSelected(true);
                radioButton.setDisable(true);
                radioButton1.setDisable(true);
                newHBox.getChildren().addAll(text,radioButton,radioButton1);
                vBox1.getChildren().add(newHBox);


            }
            anchorPane2.getChildren().add(vBox1);
            anchorPane2.setStyle("-fx-background-color: #F9E79F");
            newVbox.getChildren().add(anchorPane2);
            grade.setFocusTraversable(false);
            grade.setPromptText("نمره");
            if(quiz.getAnswers().get(currentQuestion).getGrade()==0)grade.setText(null);
            else {
                if(quiz.getAnswers().size()>currentQuestion){
                    if(quiz.getAnswers().get(currentQuestion)!=null){
                        grade.setText(String.valueOf(quiz.getAnswers().get(currentQuestion).getGrade()));
                    }
                }

            }
            grade.setMinSize(100,100);
            grade.setMaxSize(100,100);

            newVbox.getChildren().addAll(grade,isGraded,save);
            newVbox.getChildren().add(hbox);
        }
        setvBox(newVbox);
        newVbox.setMaxWidth(500);
        vBox3.getChildren().add(newVbox);
        borderPane.setCenter(vBox3);

    }

    public void setvBox(VBox vBox) {
        this.vBox = vBox;
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

   /* public void setStudent(Student student) {
        this.student = student;
    }*/

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setStudents(ArrayList<Student> studentsArray) {
        VBox newVBox=new VBox();
        ArrayList<Button>buttons=new ArrayList<>();
        System.out.println("array"+studentsArray.size());

        for(int i = 0; i< studentsArray.size(); i++){
            Button button=new Button();
            button.setText(studentsArray.get(i).getName()+" "+studentsArray.get(i).getSurName()+":"+studentsArray.get(i).getAccountNumber());
            System.out.println(studentsArray.get(i).getName()+" "+studentsArray.get(i).getSurName()+":"+studentsArray.get(i).getAccountNumber());
            //button.setMaxSize(100,50);
            button.setMinSize(100,50);
            button.setStyle("-fx-background-color: #FCF3CF");
            buttons.add(button);
            newVBox.getChildren().add(button);

        }
        scrollPane2.setContent(newVBox);
        newVBox.setMinWidth(100);
        borderPane.setLeft(newVBox);
        this.students=studentsArray;
        this.studentsButtons=buttons;

    }

    public ArrayList<Button> getStudentsButtons() {
        return studentsButtons;
    }

    public Button getNext() {
        return next;
    }

    public Button getPrevious() {
        return previous;
    }

    public Button getOpenFile() {
        return openFile;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ArrayList<Student> getStudentsArray() {
        return students;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public TextField getGrade() {
        return grade;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public Button getSave() {
        return save;
    }

    public Button getIsGraded() {
        return isGraded;
    }
}
