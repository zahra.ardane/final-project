package GUI;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public abstract class SignUp {

    Scene scene;
    AnchorPane anchor1 = new AnchorPane();
    AnchorPane anchor2 = new AnchorPane();
    Label name;
    TextField nameTextField;
    Label lastName;
    TextField lastNameTextField;
    Label userName;
    TextField userNameTextField;
    Label password;
    TextField passwordTextField;
    Button signUpButton;
    Button back;

    public SignUp(){

        anchor1.setMinSize(1300,800);
        anchor2.setStyle("-fx-background-color: #F9E79F");
        anchor2.setLayoutX(400);
        anchor2.setLayoutY(175);

        name = new Label("نام : ");
        name.setFont(new Font("Calibri Light", 23));
        name.setTextFill(Color.BLACK);
        setLayoutCoordinates(300,50,name);

        nameTextField = new TextField();
        nameTextField.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(60,45,nameTextField);

        lastName = new Label("نام خانوادگی : ");
        lastName.setFont(new Font("Calibri Light", 23));
        lastName.setTextFill(Color.BLACK);
        setLayoutCoordinates(300,100,lastName);

        lastNameTextField = new TextField();
        lastNameTextField.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(60,95,lastNameTextField);

        userName = new Label("نام کابری : ");
        userName.setFont(new Font("Calibri Light", 23));
        userName.setTextFill(Color.BLACK);

        userNameTextField = new TextField();
        userNameTextField.setFont(new Font("Calibri Light", 20));

        password = new Label("رمز عبور : ");
        password.setFont(new Font("Calibri Light", 23));
        password.setTextFill(Color.BLACK);

        passwordTextField = new TextField();
        passwordTextField.setFont(new Font("Calibri Light", 20));

        back = new Button("بر گشت");
        back.setFont(new Font("Calibri Light", 27));
        back.setStyle("-fx-background-color: #FCF3CF");

        Image image = new Image("GUI\\pics\\background.jpg");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1300,800,false,false,false,false));
        anchor1.setBackground(new Background(background));

        anchor2.getChildren().addAll(name,nameTextField,lastName,lastNameTextField
                ,userName,userNameTextField,password,passwordTextField,back);
        anchor1.getChildren().add(anchor2);
        scene = new Scene(anchor1);

    }

    void setLayoutCoordinates(int x,int y,Label label){
        label.setLayoutX(x);
        label.setLayoutY(y);
    }

    void setLayoutCoordinates(int x,int y,TextField textField){
        textField.setLayoutX(x);
        textField.setLayoutY(y);
    }

    Scene getScene(){
        return scene;
    }

    public Button getSignUpButton(){
        return signUpButton;
    }

    public Button getBackButton(){
        return back;
    }

    public TextField getLastNameTextField() {
        return lastNameTextField;
    }

    public TextField getUserNameTextField() {
        return userNameTextField;
    }

    public TextField getPasswordTextField() {
        return passwordTextField;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public void setSignUpButton(Button signUpButton) {
        this.signUpButton = signUpButton;
    }
}
