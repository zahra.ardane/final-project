package GUI;

import Logic.Quiz;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class PrepareForTest {

    Quiz quiz;
    GridPane gridPane = new GridPane();
    AnchorPane container = new AnchorPane();
    Button participate = new Button("شرکت در آزمون");
    Label name=new Label();
    Label date=new Label();
    Label startTime=new Label();
    Label finishTime=new Label();

    public PrepareForTest(){
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(1035,800,false,false,false,true));
        gridPane.setBackground(new Background(bgImg));
        container.setMinSize(400,400);
        name.setFont(new Font("Calibri Light", 23));
        name.setLayoutX(350);
        name.setLayoutY(30);

        date.setFont(new Font("Calibri Light", 23));
        date.setLayoutX(250);
        date.setLayoutY(120);
        //name.setLayoutX(350);
        //name.setLayoutY(30);

        startTime.setFont(new Font("Calibri Light", 20));
        startTime.setLayoutX(360);
        startTime.setLayoutY(210);

        finishTime.setFont(new Font("Calibri Light", 20));
        finishTime.setLayoutX(360);
        finishTime.setLayoutY(300);

        participate.setStyle("-fx-background-color: #FCF3CF");
        participate.setLayoutX(180);
        participate.setLayoutY(360);

                //finishTime.setLayoutX(15);
                //finishTime.setLayoutY(230);
//
        container.getChildren().addAll(name,date,startTime,finishTime,participate);
//        container.setMinSize();
        container.setStyle("-fx-background-color: #F9E79F");

        gridPane.getChildren().add(container);
        gridPane.setAlignment(Pos.CENTER);
    }


    public void setQuiz (Quiz quiz){
        this.quiz = quiz;
        String[]quizSplit=quiz.getName().split("#");
        name .setText(" نام آزمون:   " +quizSplit[0] );
        date .setText("تاریخ برگذاری:   "+quiz.getYear()+" / "+ quiz.getMonth()+" / "+quiz.getDay());
        startTime .setText("زمان شروع:  "+quiz.getStartMinute()+" : "+quiz.getStartHour());
        if(quiz.getQuestionsShowedAtOnce()){
        finishTime .setText("مدت زمان:  "+quiz.getStopMinute()+" : "+quiz.getStopHour());}

    }

    public Quiz getQuiz(){
        return quiz;
    }

    public Button getParticipateButton(){
        return participate;
    }

    public GridPane getGridPane(){
        return gridPane;
    }

}
