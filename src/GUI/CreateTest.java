package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CreateTest {

    private Stage stage = new Stage();
    private Scene scene ;
    private TextField textField;
    private Button back;
    private Button ok;

    public CreateTest(){

        stage.setTitle("نام آزمون");
        stage.setMinHeight(300);
        stage.setMinWidth(500);
        stage.setX(700);
        stage.setY(400);

        Label testName = new Label("نام آزمون را وارد کنید");
        testName.setFont(new Font("Calibri Light", 23));
        testName.setLayoutX(150);
        testName.setLayoutY(30);
//        testName.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");

        textField = new TextField();
        textField.setFont(new Font("Calibri Light", 23));
        textField.setLayoutX(100);
        textField.setLayoutY(90);

        back = new Button("بر گشت");
        back.setFont(new Font("Calibri Light", 23));
        back.setStyle("-fx-background-color: #FCF3CF");
        back.setLayoutX(80);
        back.setLayoutY(170);

        ok = new Button("تصویب");
        ok.setFont(new Font("Calibri Light", 23));
        ok.setStyle("-fx-background-color: #FCF3CF");
        ok.setLayoutX(300);
        ok.setLayoutY(170);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(testName,textField,back,ok);
        Image image = new Image("GUI\\pics\\alert.png");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(500,250,false,false,false,false));
        anchorPane.setBackground(new Background(background));

        scene = new Scene(anchorPane);
        stage.setScene(scene);
        stage.show();
    }

    public TextField getTextField(){
        return textField;
    }

    public Button getOk(){
        return ok;
    }

    public Button getBack(){
        return back;
    }

    public Stage getStage(){
        return stage;
    }

}
