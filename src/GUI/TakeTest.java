package GUI;

import Logic.Quiz;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;

public class TakeTest {

    Button next = new Button("سوال بعد");
    Button previous = new Button("سوال ثبل");
    Button done=new Button("ذخیره");
    Button file=new Button("فایل");
    HBox doneHolder=new HBox(done);
    HBox buttonHolder=new HBox(previous,next);
    AnchorPane anchorPane = new AnchorPane();
    VBox container = new VBox();
    VBox optionHolder=new VBox();
    Label time=new Label();
    Label time2=new Label();
    TextArea descriptiveAnswer=new TextArea();
    ToggleGroup multipleOptionToggleGroup;
    ArrayList<RadioButton> multipleOptionRadioButtons;
    ArrayList<ToggleGroup> trueFalseToggleGroup;
    ArrayList<RadioButton> trueFalseRadioButtons;
    GridPane gridPane=new GridPane();
    ArrayList<String> phrases;
    Label question=new Label();
    Quiz quiz;

    int currentQuestion;
    public TakeTest(){
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        gridPane.setBackground(new Background(bgImg));
        buttonHolder.setSpacing(730);
        container.setStyle("-fx-background-color: #F9E79F");
        container.setPadding(new Insets(10,10,10,40));
        container.setMinSize(900,700);
        container.setSpacing(70);
        //anchorPane.getChildren().add(container);
        gridPane.getChildren().add(container);
        gridPane.setAlignment(Pos.CENTER);
        next.setStyle("-fx-background-color: #FCF3CF");
        previous.setStyle("-fx-background-color: #FCF3CF");
        doneHolder.setPadding(new Insets(10,10,10,200));
        time2.setLayoutX(200);

    }

    public void displayQuestion(){
        container.getChildren().clear();
        optionHolder.getChildren().clear();

        System.out.println(quiz.getOrder().size());
        Label question = new Label(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getContext());
        question.setPadding(new Insets(10,10,10,10));
        question.setStyle("-fx-background-color: #FCF3CF");
        question.setMinSize(900,100);
        question.setMaxWidth(900);
        question.setWrapText(true);

        if(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getType()==1){

            descriptiveAnswer.setFocusTraversable(false);
            descriptiveAnswer.setText(quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getAnswer());
            descriptiveAnswer.setMaxWidth(900);
            descriptiveAnswer.setFont(new Font("Calibri Light", 20));
            descriptiveAnswer.setPromptText("پاسخ");
            descriptiveAnswer.setWrapText(true);
            container.getChildren().addAll(time,time2,question,descriptiveAnswer,file,buttonHolder,doneHolder);

        } else if(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getType() == 2){ //multiple answer
            multipleOptionRadioButtons = new ArrayList<>();
            multipleOptionToggleGroup = new ToggleGroup();
            for(int j=0;j<quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getOptions().size();j++){
                RadioButton radioButton = new RadioButton(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getOptions().get(j));
                if(quiz.getAnswers().get(quiz.getOrder().get(currentQuestion))!=null) {
                    if (quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getAnswer() != null) {
                        if (quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getAnswer().equals(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getOptions().get(j)))
                            radioButton.setSelected(true);
                    }
                }
                radioButton.setToggleGroup(multipleOptionToggleGroup);
                multipleOptionRadioButtons.add(radioButton);
                optionHolder.getChildren().add(radioButton);
                //optionHolder.setStyle("-fx-background-color: #FCF3CF");
            }
            optionHolder.setSpacing(20);
            container.getChildren().addAll(time,time2,question,optionHolder,buttonHolder,doneHolder);
        } else if(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getType() == 3){ //true false
            trueFalseToggleGroup = new ArrayList<>();
            trueFalseRadioButtons = new ArrayList<>();
            phrases = new ArrayList<>();
            container.getChildren().addAll(time,time2,question);
            Label trueLabel = new Label("صحیح");
            Label falseLabel = new Label("غلط");

            System.out.println(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getTrueFalsePhrases().size());
            for(int j=0;j<quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getTrueFalsePhrases().size();j++){
                HBox hBox=new HBox();

                Label phrase = new Label(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getTrueFalsePhrases().get(j));
                //hBox.setStyle("-fx-background-color: #FCF3CF");
                hBox.getChildren().add(phrase);
                hBox.setSpacing(100);
                //give location
                HBox trueFalseHolder=new HBox();
                phrases.add(phrase.getText());
                ToggleGroup toggleGroup=new ToggleGroup();
                trueFalseToggleGroup.add(toggleGroup);
                //Label isTrue=new Label("صحیح");
                //Label isFalse=new Label("غلط");
                RadioButton radioButton1 = new RadioButton("صحیح"); //true
                trueFalseHolder.getChildren().addAll(radioButton1);
                trueFalseHolder.setSpacing(5);
                radioButton1.setToggleGroup(trueFalseToggleGroup.get(j));
                RadioButton radioButton2 = new RadioButton("غلط"); //false
                trueFalseHolder.getChildren().addAll(radioButton2);
                radioButton2.setToggleGroup(trueFalseToggleGroup.get(j));
                if(quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getYesNoAnswers().size()>j) {
                    if (quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getYesNoAnswers().get(j) != null) {

                        if (quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getYesNoAnswers().get(j) == 2)
                            radioButton1.setSelected(true);
                        else if (quiz.getAnswers().get(quiz.getOrder().get(currentQuestion)).getYesNoAnswers().get(j) == 1)
                            radioButton2.setSelected(true);

                    }
                }
                trueFalseRadioButtons.add(radioButton1);
                trueFalseRadioButtons.add(radioButton2);
                hBox.getChildren().addAll(trueFalseHolder);
                container.getChildren().add(hBox);
            }
            container.getChildren().addAll(buttonHolder,doneHolder);
        }

    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public void setQuiz(Quiz quiz){
        this.quiz = quiz;
        currentQuestion=0;
    }

    public Button getNext(){
        return next;
    }

    public Button getPrevious(){
        return previous;
    }

    public TextArea getDescriptiveAnswer(){
        return descriptiveAnswer;
    }

    public ToggleGroup getMultipleOptionToggleGroup(){
        return multipleOptionToggleGroup;
    }

    public ArrayList<RadioButton> getMultipleOptionRadioButtons(){
        return multipleOptionRadioButtons;
    }

    public ArrayList<ToggleGroup> getTrueFalseToggleGroup(){
        return trueFalseToggleGroup;
    }

    public ArrayList<RadioButton> getTrueFalseRadioButtons(){
        return trueFalseRadioButtons;
    }

    public AnchorPane getAnchorPane(){
        return anchorPane;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public Button getDone() {
        return done;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public Label getQuestion() {
        return question;
    }

    public void setTime(String time) {
        this.time.setText(time);
    }
    public void setTime2(String time) {
        this.time2.setText(time);
    }
    public Label getTime() {
        return time;
    }

    public Button getFile() {
        return file;
    }
}