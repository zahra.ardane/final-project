package GUI;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class StudentPanel {

    private TreeView treeView = new TreeView();
    private TreeItem tests;
    private TreeItem chats;
    private TreeItem heldTests;
    private TreeItem toBeHeldTests;
    private ArrayList<TreeItem> items = new ArrayList<>();

    public StudentPanel(){

        TreeItem sideBar = new TreeItem();

        heldTests = new TreeItem("بر گذار شده");

        toBeHeldTests = new TreeItem("بر گذار نشده");

        ImageView testsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\tests.png")));
        resizeImage(testsIcon);
        tests = new TreeItem("آزمون ها",testsIcon);

        tests.getChildren().addAll(heldTests,toBeHeldTests);

        ImageView chatsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\chats.png")));
        resizeImage(chatsIcon);
        chats = new TreeItem("گفت و گو",chatsIcon);

        sideBar.getChildren().addAll(tests,chats);

        treeView.setRoot(sideBar);
        treeView.setShowRoot(false);

        treeView.getStyleClass().add("Tree");
        treeView.getStylesheets().add("/GUI/Tree.css");

    }

    private void resizeImage(ImageView image){
        image.setFitWidth(25);
        image.setFitHeight(25);
    }

    public void addItemsToNotYetHeldTests(String name){

        TreeItem item = new TreeItem(name);

        toBeHeldTests.getChildren().add(item);
        items.add(item);
    }

    public void addItemsToHeldTests(String name){

        TreeItem review = new TreeItem("مرور");
        items.add(review);
        TreeItem information = new TreeItem("اطلاعات کلی");
        items.add(information);
        TreeItem poll = new TreeItem("نظرسنجی");
        items.add(poll);

        TreeItem item = new TreeItem(name);
        item.getChildren().addAll(review,information,poll);

        heldTests.getChildren().add(item);

    }

    public void addItemsToChat(String name){
        TreeItem item = new TreeItem(name);
        chats.getChildren().add(item);
        items.add(item);
    }

    public ArrayList<TreeItem> getItems(){
        return items;
    }

    public TreeItem getChats(){
        return chats;
    }

    public TreeItem getToBeHeldTests(){
        return toBeHeldTests;
    }

    public TreeItem getHeldTests(){
        return heldTests;
    }

    public TreeView getTreeView(){
        return treeView;
    }

}
