package GUI;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;
import java.util.TreeMap;

public class ManagerPanel {

    private boolean isTestCreated;
    private TreeItem addStudentsFromExcel;
    private TreeItem addStudentsManually;
    private TreeItem addQuestions;
    private TreeItem addInformation;
    private TreeItem testHistory;
    private TreeItem gradeTests;
    private TreeItem manageTests;
    private TreeItem chats;
    private TreeView treeView;
    private TreeItem createTest;
    private TreeItem addStudents;
    private TreeItem exit;

    private ArrayList<TreeItem> treeItems = new ArrayList<>();

    public ManagerPanel(){

        TreeItem sideBar = new TreeItem();

        ImageView createTestIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\createTest.png")));
        resizeImage(createTestIcon);
        createTest = new TreeItem("ایجاد آزمون",createTestIcon);
        ImageView addStudentsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\addStudents.jpg")));
        resizeImage(addStudentsIcon);
        addStudents = new TreeItem("اضافه کردن دانشجو ها",addStudentsIcon);
        addStudentsFromExcel = new TreeItem("اضافه کردن از فایل اکسل");
        addStudentsManually = new TreeItem("اضافه کردن دستی");
        addStudents.getChildren().add(addStudentsFromExcel);
        addStudents.getChildren().add(addStudentsManually);

        ImageView addQuestionsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\addQuestions.png")));
        resizeImage(addQuestionsIcon);
        addQuestions = new TreeItem("اضافه کردن سوال",addQuestionsIcon);

        ImageView addInformationIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\addInfo.png")));
        resizeImage(addInformationIcon);
        addInformation = new TreeItem("اضافه کردن اطلاعات",addInformationIcon);

        createTest.getChildren().addAll(addStudents,addQuestions,addInformation);

        ImageView testHistoryIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\testHistory.png")));
        resizeImage(testHistoryIcon);
        testHistory = new TreeItem("تاریخچه آزمون",testHistoryIcon);

        ImageView gradeTestsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\gradeTests.png")));
        resizeImage(gradeTestsIcon);
        gradeTests = new TreeItem("تصحیح آزمون",gradeTestsIcon);

        ImageView manageTestsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\manageTests.png")));
        resizeImage(manageTestsIcon);
        manageTests = new TreeItem("مدیریت آزمون",manageTestsIcon);

        ImageView chatsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\chats.png")));
        resizeImage(chatsIcon);
        chats = new TreeItem("گفت و گو",chatsIcon);

        ImageView exitIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\exitIcon.jpg")));
        resizeImage(exitIcon);
        exit = new TreeItem("خروج",exitIcon);

        sideBar.getChildren().addAll(createTest,testHistory,gradeTests,manageTests,chats,exit);

        treeView = new TreeView();
        treeView.setRoot(sideBar);
        treeView.setShowRoot(false);

        treeView.getStyleClass().add("Tree");
        treeView.getStylesheets().add("/GUI/Tree.css");

    }


    private void resizeImage(ImageView image) {
        image.setFitWidth(25);
        image.setFitHeight(25);
    }

    public void addItemsToHistoryAndCorrection(String name){

        TreeItem testByTest1 = new TreeItem("آزمون به آزمون");
        treeItems.add(testByTest1);
        TreeItem studentByStudent1 = new TreeItem("فرد به فرد");
        treeItems.add(studentByStudent1);

        ImageView chartsIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\charts.png")));
        resizeImage(chartsIcon);
        TreeItem charts = new TreeItem("نمودار آزمون ها",chartsIcon);

        charts.getChildren().addAll(testByTest1,studentByStudent1);

        TreeItem testByTest2 = new TreeItem("آزمون به آزمون");
        treeItems.add(testByTest2);
        TreeItem studentByStudent2 = new TreeItem("فرد به فرد");
        treeItems.add(studentByStudent2);

        TreeItem excelResults = new TreeItem("نمایش نتیجه در اکسل");
        excelResults.getChildren().addAll(testByTest2,studentByStudent2);

        TreeItem generalHistory  = new TreeItem("اطلاعات کلی");
        treeItems.add(generalHistory);

        TreeItem item1 = new TreeItem(name);
        item1.getChildren().addAll(generalHistory,charts,excelResults);
        testHistory.getChildren().add(item1);
//        treeItems.add(item1);

        TreeItem item2 = new TreeItem(name);
        gradeTests.getChildren().add(item2);
        treeItems.add(item2);
    }

    public void addItemToManageAndChat(String name){

        TreeItem changeStudentsParticipation = new TreeItem("تغییر شرکت کنندگان");
        TreeItem changeInfo = new TreeItem("تغییر اطلاعات");
        TreeItem changeQuestions = new TreeItem("تغییر سوالات");

        TreeItem item1 = new TreeItem(name);
        item1.getChildren().addAll(changeStudentsParticipation,changeInfo,changeQuestions);
        manageTests.getChildren().add(item1);
        treeItems.add(changeStudentsParticipation);
        treeItems.add(changeInfo);
        treeItems.add(changeQuestions);

        TreeItem item2 = new TreeItem(name);
        chats.getChildren().add(item2);
        treeItems.add(item2);
    }

    public void removeTestsFromTreeView(){

    }

    public ArrayList<TreeItem> getTreeItems(){
        return treeItems;
    }

    public void setTestCreated(boolean value){
        isTestCreated = value;
    }

    public boolean getTestCreated(){
        return isTestCreated;
    }

    public TreeItem getCreateTest(){
        return createTest;
    }

    public TreeItem getAddStudents(){
        return addStudents;
    }

    public TreeItem getAddStudentsFromExcel(){
        return addStudentsFromExcel;
    }

    public TreeItem getAddStudentsManually(){
        return addStudentsManually;
    }

    public TreeItem getAddQuestions(){
        return addQuestions;
    }

    public TreeItem getAddInformation(){
        return addInformation;
    }

    public TreeItem getTestHistory(){
        return testHistory;
    }

    public TreeItem getGradeTests(){
        return gradeTests;
    }

    public TreeItem getManageTests(){
        return manageTests;
    }

    public TreeItem getChats(){
        return chats;
    }

    public TreeItem getExit(){
        return exit;
    }

    public TreeView getTreeView(){
        return treeView;
    }

}
