package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class StudentLogin extends Login {
    public StudentLogin(){
            super();
        Button enterButton=new Button("ورود");
        enterButton.setFont(new Font("Calibri Light", 30));
        enterButton.setStyle("-fx-background-color: #FCF3CF");
        enterButton.setLayoutX(180);
        enterButton.setLayoutY(220);
        setEnterButton(enterButton);

        Button registerButton=new Button("ثبت نام");
        registerButton.setFont(new Font("Calibri Light", 30));
        registerButton.setStyle("-fx-background-color: #FCF3CF");
        registerButton.setLayoutY(220);
        registerButton.setLayoutX(300);
        setRegisterButton(registerButton);


        getAnchorPane1().getChildren().addAll(getEnterButton(),getRegisterButton());
            getTitle().setText("دانشجو");
            getTitle().setLayoutX(250);
    }


}
