package GUI;

import Logic.Student;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.ArrayList;

public class AddManually {

    ArrayList<CheckBox> checkBoxes = new ArrayList<>();
    ArrayList<Student>students=new ArrayList<>();
    TextField name;
    TextField surName;
    TextField studentId;
    Button add;
    Button done=new Button("ذخیره آزمون");
    AnchorPane anchorPane=new AnchorPane();
    AnchorPane anchorPane1=new AnchorPane();
    ScrollPane scrollBar=new ScrollPane();
    VBox vBox;
    Label studentsAdded;

    public AddManually(){

        anchorPane1.setStyle("-fx-background-color: #F9E79F");
        anchorPane1.setLayoutX(100);
        anchorPane1.setLayoutY(250);
        anchorPane1.setMinSize(300,300);

        name=new TextField();
        name.setFocusTraversable(false);
        name.setFont(new Font("Calibri Light", 20));
        name.setPromptText("نام");
        name.setLayoutX(30);
        name.setLayoutY(30);

        surName=new TextField();
        surName.setFocusTraversable(false);
        surName.setFont(new Font("Calibri Light", 20));
        surName.setPromptText("نام خوانوادگی");
        surName.setLayoutX(30);
        surName.setLayoutY(80);

        studentId=new TextField();
        studentId.setFocusTraversable(false);
        studentId.setFont(new Font("Calibri Light", 20));
        studentId.setPromptText("شماره دانشجویی");
        studentId.setLayoutX(30);
        studentId.setLayoutY(130);

        add=new Button("اضافه");
        add.setFont(new Font("Calibri Light", 25));
        add.setStyle("-fx-background-color: #FCF3CF");
        add.setLayoutX(110);
        add.setLayoutY(220);

        done.setFont(new Font("Calibri Light", 25));
        done.setStyle("-fx-background-color: #FCF3CF");
        done.setLayoutX(190);
        done.setLayoutY(600);

        studentsAdded=new Label("دانشجویان اضافه شده");
        studentsAdded.setFont(new Font("Calibri Light", 25));
        studentsAdded.setPadding(new Insets(20,0,10,100));

        vBox =new VBox();
        vBox.setMinHeight(600);
        vBox.setMaxWidth(400);
        vBox.setMinWidth(400);
        vBox.setStyle("-fx-background-color: #F9E79F");

        ScrollPane scrollBar=new ScrollPane();
        vBox.getChildren().addAll(studentsAdded);
        scrollBar.setContent(vBox);
        scrollBar.setMinHeight(600);
        scrollBar.setMinWidth(400);
        scrollBar.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollBar.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollBar.setLayoutX(500);
        scrollBar.setLayoutY(80);


        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        anchorPane.setBackground(new Background(bgImg));

        anchorPane1.getChildren().addAll(name,surName,studentId,add);
        anchorPane.getChildren().addAll(anchorPane1, scrollBar, done);
    }

    public void addStudent(Student student){
        CheckBox checkBox = new CheckBox(String.valueOf(student.getStudentId())+"  "+String.valueOf(student.getName())+
                "  "+student.getSurName());
        checkBox.setPadding(new Insets(10,0,10,15));
        checkBox.setSelected(true);
        checkBoxes.add(checkBox);
        students.add(student);
        vBox.getChildren().add(checkBox);
        getName().setText(null);
        getSurName().setText(null);
        getStudentId().setText(null);
    }

    public AnchorPane getAnchorPane(){
        return anchorPane;
    }

    public Button getAdd() {
        return add;
    }

    public TextField getName() {
        return name;
    }

    public TextField getStudentId() {
        return studentId;
    }

    public TextField getSurName() {
        return surName;
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }
    public void setAddManuallyNull(){
        students=new ArrayList<>();
        checkBoxes=new ArrayList<>();
        vBox.getChildren().clear();
        vBox.getChildren().add(studentsAdded);
        getName().setText(null);
        getSurName().setText(null);
        getStudentId().setText(null);
    }

    public Button getDone() {
        return done;
    }
}
