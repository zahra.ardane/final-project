package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ExitAlert {

    Stage stage = new Stage();
    Scene scene ;
    Label label;
    Button back;
    Button exit;
    AnchorPane anchorPane = new AnchorPane();

    public ExitAlert(){

        stage.setTitle("خروج");
        stage.setMinHeight(300);
        stage.setMinWidth(500);
        stage.setX(700);
        stage.setY(400);

        label = new Label("آيا مطمئن هستيد كه مي خواهيد از برنامه خارج شويد ؟");
        label.setFont(new Font("Calibri Light", 23));
        label.setLayoutX(40);
        label.setLayoutY(70);

        back = new Button("بر گشت");
        back.setFont(new Font("Calibri Light", 23));
        back.setStyle("-fx-background-color: #FCF3CF");
        back.setLayoutX(80);
        back.setLayoutY(170);
        back.setOnAction(e ->stage.close());

        exit = new Button("خروج");
        exit.setFont(new Font("Calibri Light", 23));
        exit.setStyle("-fx-background-color: #FCF3CF");
        exit.setLayoutX(300);
        exit.setLayoutY(170);

        anchorPane.getChildren().addAll(back, exit, label);
        Image image = new Image("GUI\\pics\\alert.png");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(500,250,false,false,false,false));
        anchorPane.setBackground(new Background(background));

        scene = new Scene(anchorPane);
        stage.setScene(scene);
        stage.show();
    }

    public Stage getStage(){
        return stage;
    }

    public Button getExit(){
        return exit;
    }

}
