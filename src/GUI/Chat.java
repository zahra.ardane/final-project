package GUI;

import Logic.Information;
import Logic.Message;
import Logic.Quiz;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class Chat {

    private BorderPane borderPane;
    private TextField txtInput;
    private Button send;
    private VBox messageContainer;
    private Message tempMessage;
    private Information information = new Information();
    private ArrayList<Message> messages;

    public Chat(String quizName, String sender){
        tempMessage = new Message();
        tempMessage .setQuizName(quizName);

        txtInput = new TextField();
        txtInput.setMinSize(950,40);
        txtInput.setPromptText("نوشتن پیام");
        txtInput.setLayoutX(15);
        txtInput.setLayoutY(10);

        ImageView buttonIcon = new ImageView(new Image(getClass().getResourceAsStream("pics\\send.png")));
        int r =20;
        send = new Button("",buttonIcon);
        send.setStyle("-fx-background-color: #FCF3CF");
        send.setMinSize(2*r, 2*r);
        send.setMaxSize(2*r, 2*r);
        send.setShape(new Circle(r));
        send.setLayoutX(980);
        send.setLayoutY(8);

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(txtInput,send);
        anchorPane.setMinSize(500,60);
        anchorPane.setStyle("-fx-background-color: navajowhite");

        VBox borderBottom = new VBox();
        borderBottom.getChildren().addAll(anchorPane);

        messageContainer = new VBox(10);
        messageContainer.setLayoutY(15);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(messageContainer);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        Image image=new Image("GUI\\pics\\blurry.png");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(1040,745,false,false,false,false));
        messageContainer.setBackground(new Background(bgImg));

        borderPane = new BorderPane();
        borderPane.setBottom(borderBottom);
        borderPane.setCenter(scrollPane);

        messages = information.readMessage();
        if(messages.size() != 0){
            for (int i=0;i<messages.size();i++){
                if(messages.get(i).getSender().trim().equals(sender.trim())){
                    setNewText(messages.get(i).getSender(),messages.get(i).getMessage(),
                            true,messages.get(i).getHour(),messages.get(i).getMinute());
                } else {
                    setNewText(messages.get(i).getSender(),messages.get(i).getMessage(),
                            false,messages.get(i).getHour(),messages.get(i).getMinute());
                }

            }
        }

    }

    public void setNewText(String name ,String message, boolean ownTxt,int hour, int minute){
        Label label = new Label(" "+name +" : " + message+" ");
        label.setFont(new Font("Calibri Light", 23));
        Label time = new Label(hour+":"+minute);
        time.setFont(new Font("Calibri Light", 13));
        time.setStyle("-fx-background-color: #FCF3CF");
        time.setAlignment(Pos.BOTTOM_RIGHT);
        label.setMinHeight(message.length());
        if(name.length() + message.length()>=50){
            label.setMaxWidth(450);
        }
        label.setStyle("-fx-background-color: #FCF3CF");
        label.setWrapText(true);

        HBox hBox=new HBox();
        hBox.getChildren().addAll(label,time);

        if (ownTxt){
            hBox.setAlignment(Pos.BASELINE_RIGHT);
        } else {
            hBox.setAlignment(Pos.BASELINE_LEFT);
        }

        messageContainer.setPadding(new Insets(15,15,0,15));
        messageContainer.getChildren().add(hBox);
        tempMessage.setSender(name);
        tempMessage.setMessage(message);
        tempMessage.setHour(hour);
        tempMessage.setMinute(minute);
    }

    public Message getMessage(){
        return tempMessage;
    }

    public TextField getTxtInput(){
        return txtInput;
    }

    public void clearTxtInput(){
        txtInput.clear();
    }

    public Button getSend(){
        return send;
    }

    public VBox getMessageContainer(){
        return messageContainer;
    }

    public BorderPane getBorderPane(){
        return borderPane;
    }

}
