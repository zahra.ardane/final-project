package GUI;

import Logic.Information;
import Logic.Quiz;
import Logic.Student;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.util.ArrayList;

public class AddFromExcel {

    Stage stage = new Stage();
    AnchorPane anchorPane = new AnchorPane();
    ArrayList<Student> students=new ArrayList<>();
    ArrayList<CheckBox>checkBoxes=new ArrayList<>();

    public AddFromExcel(){

        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        anchorPane.setBackground(new Background(bgImg));
        
    }

    public ArrayList<Student> showFileChooser(Quiz quiz) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose an excel file");
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("EXCEL files (*.xlsx)", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            Information information = new Information();
            ArrayList<Student> students = information.readFromExcel(file.getPath(), quiz);
            for (int i = 0; i < students.size(); i++) System.out.println(students.get(i).getName());
            return students;
        }
        return null;
    }

    public AnchorPane getAnchorPane(){
        return anchorPane;
    }

    public void setBorderPane(ArrayList<Student>students){
        if(students!=null) {

            for (int i = 0; i < students.size(); i++) {
                CheckBox checkBox = new CheckBox();
                checkBoxes.add(checkBox);
                checkBox.setSelected(true);
                checkBoxes.get(i).setText(students.get(i).getName() + " " + students.get(i).getSurName() + ":" + students.get(i).getStudentId());
            }

            this.students = students;
            this.checkBoxes = checkBoxes;
            int half = checkBoxes.size() / 2;
            System.out.println(half);
            System.out.println(students.size());

            BorderPane borderPane = new BorderPane();

            VBox vBox = new VBox();
            vBox.setSpacing(20);
            vBox.setLayoutX(15);
            vBox.setLayoutY(15);
            VBox vBox1 = new VBox();
            vBox1.setSpacing(20);
            vBox1.setLayoutX(430);
            vBox1.setLayoutY(15);

            for (int i = 0; i <= half; i++) {
                vBox.getChildren().add(checkBoxes.get(i));
            }
            for (int i = half + 1; i < checkBoxes.size(); i++) {
                vBox1.getChildren().add(checkBoxes.get(i));
            }
            AnchorPane anchorPane = new AnchorPane();
            anchorPane.getChildren().addAll(vBox, vBox1);

            borderPane.setMinSize(830, 600);
            borderPane.setStyle("-fx-background-color: #F9E79F");
            borderPane.setLayoutX(100);
            borderPane.setLayoutY(80);
            borderPane.setCenter(anchorPane);

            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setContent(borderPane);
            scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
            scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            scrollPane.setLayoutY(80);
            scrollPane.setLayoutX(100);
            scrollPane.setMinSize(830, 600);

            this.anchorPane.getChildren().add(scrollPane);
        }
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }
}
