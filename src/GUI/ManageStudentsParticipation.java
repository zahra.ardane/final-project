package GUI;

import Logic.Student;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import java.util.ArrayList;

public class ManageStudentsParticipation {

    private AnchorPane container = new AnchorPane();
    private TextField name;
    private TextField surName;
    private TextField studentId;
    private Button add;
    private Button remove;
    private Button deprive;
    private Button done=new Button("ذخیره تغییرات");
    private Label label = new Label();
    private AnchorPane rightAnchor = new AnchorPane();
    private VBox vBox = new VBox();
    private ScrollPane scrollPane = new ScrollPane();

    public ManageStudentsParticipation(){

        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        container.setBackground(new Background(bgImg));

        rightAnchor.setStyle("-fx-background-color: #F9E79F");
        rightAnchor.setLayoutX(580);
        rightAnchor.setLayoutY(210);
        rightAnchor.setMinSize(400,300);

        name=new TextField();
        name.setFocusTraversable(false);
        name.setFont(new Font("Calibri Light", 20));
        name.setPromptText("نام");
        setLayoutCoordinates(80,30,name);

        surName=new TextField();
        surName.setFocusTraversable(false);
        surName.setFont(new Font("Calibri Light", 20));
        surName.setPromptText("نام خوانوادگی");
        setLayoutCoordinates(80,80,surName);

        studentId=new TextField();
        studentId.setFocusTraversable(false);
        studentId.setFont(new Font("Calibri Light", 20));
        studentId.setPromptText("شماره دانشجویی");
        setLayoutCoordinates(80,130,studentId);

        add = new Button("اضافه");
        add.setStyle("-fx-background-color: #FCF3CF");
        add.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(65,180,add);

        remove = new Button("حذف");
        remove.setStyle("-fx-background-color: #FCF3CF");
        remove.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(165,180,remove);

        deprive = new Button("محروم");
        deprive.setStyle("-fx-background-color: #FCF3CF");
        deprive.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(265,180,deprive);


        done.setStyle("-fx-background-color: #FCF3CF");
        done.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(148,240,done);

        rightAnchor.getChildren().addAll(name,surName,studentId,add,remove,deprive,done);


        container.getChildren().add(rightAnchor);

        Label label1 = new Label("تغییرات شرکت کنندگان");
        label1.setFont(new Font("Calibri Light", 25));
        label1.setAlignment(Pos.BASELINE_CENTER);

        vBox.setStyle("-fx-background-color: #F9E79F");
        vBox.setMinSize(450,650);
        vBox.setAlignment(Pos.BASELINE_CENTER);

        vBox.getChildren().add(label1);
        vBox.setSpacing(15);

        scrollPane.setContent(vBox);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setMinSize(450,650);
        scrollPane.setLayoutX(60);
        scrollPane.setLayoutY(50);

        container.getChildren().add(scrollPane);

    }

    public void setVBox(String string){
        Label label=new Label();

        label.setText(string);

        vBox.setSpacing(15);

        vBox.getChildren().add(label);

    }

    public void setLeftAnchorNull(){
        vBox.getChildren().clear();

        Label label1 = new Label("تغییرات شرکت کنندگان");
        label1.setAlignment(Pos.BASELINE_CENTER);
        label1.setFont(new Font("Calibri Light", 25));

        vBox.getChildren().add(label1);
        getStudentId().setText(null);
        getName().setText(null);
        getSurName().setText(null);
    }

    public AnchorPane getAnchorPane(){
        return container;
    }

    void setLayoutCoordinates(int x, int y, Button button){
        button.setLayoutX(x);
        button.setLayoutY(y);
    }

    void setLayoutCoordinates(int x,int y,TextField textField){
        textField.setLayoutX(x);
        textField.setLayoutY(y);
    }

    public TextField getName() {
        return name;
    }

    public TextField getStudentId() {
        return studentId;
    }

    public TextField getSurName() {
        return surName;
    }

    public Button getAddButton(){
        return add;
    }

    public Button getRemoveButton(){
        return remove;
    }

    public Button getDepriveButton(){
        return deprive;
    }

    public Label getLabel(){
        return label;
    }

    public Button getDone() {
        return done;
    }
}
