package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class StudentSignUp extends SignUp {

    Label id;
    TextField idTextField;

    public StudentSignUp(){

        super();
        signUpButton = new Button("ثبت نام");
        signUpButton.setFont(new Font("Calibri Light", 27));
        signUpButton.setStyle("-fx-background-color: #FCF3CF");
        signUpButton.setLayoutY(330);
        signUpButton.setLayoutX(270);

        back.setLayoutY(330);
        back.setLayoutX(90);

        anchor2.setMinSize(480,430);

        id = new Label("شماره دانشجویی : ");
        id.setFont(new Font("Calibri Light", 23));
        id.setTextFill(Color.BLACK);
        setLayoutCoordinates(300,150,id);

        idTextField = new TextField();
        idTextField.setFont(new Font("Calibri Light", 20));
        setLayoutCoordinates(60,145,idTextField);

        setLayoutCoordinates(300,200,userName);
        setLayoutCoordinates(60,195,userNameTextField);
        setLayoutCoordinates(300,250,password);
        setLayoutCoordinates(60,245,passwordTextField);
        setSignUpButton(signUpButton);

        anchor2.getChildren().addAll(id,idTextField,getSignUpButton());

        //anchor1.getChildren().add(anchor2);


    }

    public TextField getIdTextField() {
        return idTextField;
    }
}
