package GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.control.Label;
import javafx.scene.control.Button;


public class Start {

    Scene scene;
    Button managerPanel;
    Button studentPanel;

    public Start(){

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(30);
        grid.setVgap(30);

        Label appName = new Label("سیستم آزمون یار");
        appName.setFont(new Font("Calibri Light", 40));
        appName.setTextFill(Color.WHITE);
        grid.add(appName,15,3,5,5);

        managerPanel = new Button("پنل مدیریت");
        managerPanel.setFont(new Font("Calibri Light", 20));
        managerPanel.setMaxSize(200,70);
        managerPanel.setStyle("-fx-background-color: navajowhite");
        managerPanel.setTextFill(Color.BLACK);
        grid.add(managerPanel,3,8,10,10);

        studentPanel = new Button("پنل دانشجو");
        studentPanel.setFont(new Font("Calibri Light", 20));
        studentPanel.setMaxSize(200,70);
        studentPanel.setStyle("-fx-background-color: navajowhite");
        studentPanel.setTextFill(Color.BLACK);
        grid.add(studentPanel,24,8,10,10);

        Image image = new Image("GUI\\pics\\background.jpg");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1300,800,false,false,false,false));
        grid.setBackground(new Background(background));

        scene = new Scene(grid,100,100);

    }

//    @Override
//    public void canvasClicked(MouseEvent mouseEvent) {
//        DrawCoords mousePosition = new DrawCoords(mouseEvent.getX(), mouseEvent.getY());
//        CoordProjector projector = getProjector(boardCanvas);
//        Coords boardPos = projector.nearestCoords(mousePosition);
//
//        if ( !projector.isWithinBounds(mousePosition) )
//            return;
//
//        scorePane.enableButtons();
//        if ( mouseEvent.getButton() == MouseButton.PRIMARY ) {
//            if ( !mouseEvent.isShiftDown() )
//                boardScorer.markGroupDead(boardPos);
//            else
//                boardScorer.unmarkGroupDead(boardPos);
//        }
//    }

    Scene getScene(){
        return scene;
    }

    public Button getManagerPanel(){
        return managerPanel;
    }

    public Button getStudentPanel(){
        return studentPanel;
    }

}
