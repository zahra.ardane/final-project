package GUI;
import Logic.Quiz;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;


import java.awt.*;

public class Results {

    Quiz quiz;
    GridPane gridPane = new GridPane();
    AnchorPane container = new AnchorPane();
    Label average = new Label();
    Label rank = new Label();
    Label finalGrade = new Label();

    public Results(){
        javafx.scene.image.Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        gridPane.setBackground(new Background(bgImg));

        average.setFont(new Font("Calibri Light", 20));
        average.setLayoutX(100);
        average.setLayoutY(40);

        rank.setFont(new Font("Calibri Light", 20));
        rank.setLayoutX(80);
        rank.setLayoutY(90);

        finalGrade.setFont(new Font("Calibri Light", 20));
        finalGrade.setLayoutX(100);
        finalGrade.setLayoutY(140);

        container.setMinSize(400,400);
        container.setStyle("-fx-background-color: #F9E79F");
        container.getChildren().addAll(average,rank,finalGrade);

        gridPane.getChildren().add(container);
        gridPane.setAlignment(Pos.CENTER);
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        rank.setText(String.valueOf(quiz.getRank()));
        finalGrade.setText(String.valueOf(quiz.getGrade()));
        average.setText(String.valueOf(quiz.getAverage()));
    }

    public GridPane getGridPane() {
        return gridPane;
    }
}
