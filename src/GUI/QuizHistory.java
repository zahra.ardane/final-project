package GUI;

import Logic.Quiz;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class QuizHistory {

    Quiz quiz;
    AnchorPane anchorPane = new AnchorPane();
    AnchorPane rightContainer = new AnchorPane();
    Label testName = new Label();
    Label testDate = new Label();
    Label testAverage = new Label();
    Label startTime = new Label();
    Label finishTime = new Label();
    Label pollResult = new Label();
    Label studentsAdded = new Label("دانشجویان");
    VBox vBox;

    public QuizHistory(){
        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        anchorPane.setBackground(new Background(bgImg));

        testName.setFont(new Font("Calibri Light", 20));
        testName.setLayoutX(100);
        testName.setLayoutY(40);

        testDate.setFont(new Font("Calibri Light", 20));
        testDate.setLayoutX(80);
        testDate.setLayoutY(90);

        startTime.setFont(new Font("Calibri Light", 20));
        startTime.setLayoutX(100);
        startTime.setLayoutY(140);

        finishTime.setFont(new Font("Calibri Light", 20));
        finishTime.setLayoutX(100);
        finishTime.setLayoutY(190);

        testAverage.setFont(new Font("Calibri Light", 20));
        testAverage.setLayoutX(130);
        testAverage.setLayoutY(240);

        pollResult.setFont(new Font("Calibri Light", 20));
        pollResult.setLayoutX(100);
        pollResult.setLayoutY(290);

        rightContainer.getChildren().addAll(testName,testDate,startTime,finishTime,testAverage,pollResult);
        rightContainer.setStyle("-fx-background-color: #F9E79F");
        rightContainer.setMinSize(370,370);
        rightContainer.setLayoutX(570);
        rightContainer.setLayoutY(180);

        studentsAdded.setFont(new Font("Calibri Light", 25));
        studentsAdded.setAlignment(Pos.BASELINE_CENTER);

        vBox =new VBox(10);
        vBox.setMinHeight(600);
        vBox.setMaxWidth(400);
        vBox.setMinWidth(400);
        vBox.setStyle("-fx-background-color: #F9E79F");
        vBox.setAlignment(Pos.BASELINE_CENTER);

        ScrollPane scrollBar=new ScrollPane();
        vBox.getChildren().addAll(studentsAdded);
        scrollBar.setContent(vBox);
        scrollBar.setMinHeight(600);
        scrollBar.setMinWidth(400);
        scrollBar.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollBar.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollBar.setLayoutX(100);
        scrollBar.setLayoutY(80);

        anchorPane.getChildren().addAll(rightContainer,scrollBar);
    }

    public void setQuiz (Quiz quiz){
        this.quiz = quiz;
        String[]quizSplit = quiz.getName().split("#");
        testName.setText("نام آزمون    :     "+ quizSplit[0]);
        testDate.setText("تاریخ بر گذاری    :     "+quiz.getDay()+" / "+ quiz.getMonth()+" / "+quiz.getYear());
        int average = 0;
        for(int i=0;i<quiz.getStudents().size();i++){
            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                if(quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName()))
                    average += quiz.getStudents().get(i).getQuizzes().get(j).getGrade();
            }
        }
        average /= quiz.getStudents().size();
        testAverage.setText("میانگین    :     "+average);
        startTime.setText("زمان شروع    :     "+quiz.getStartMinute()+" : "+quiz.getStartHour());
        finishTime.setText("مدت آزمون    :     "+quiz.getStopMinute()+" : "+quiz.getStopHour());
        pollResult.setText( "نتیجه نظرسنجی    :     " + quiz.getHardshipLevel());
        for(int i=0;i<quiz.getStudents().size();i++){
            Label label = new Label();
            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                if(quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())){
                    if(quiz.getStudents().get(i).getQuizzes().get(j).getIsBanned()){
                        label.setText(quiz.getStudents().get(i).getName()+" "+quiz.getStudents().get(i).getSurName()+" _ "
                                +quiz.getStudents().get(i).getStudentId()+" _ "+"محروم شده");
                    } else {
                        label.setText(quiz.getStudents().get(i).getName()+" "+quiz.getStudents().get(i).getSurName()+" _ "
                                +quiz.getStudents().get(i).getStudentId()+" _ "+"شرکت کننده");
                    }
                }
            }
            vBox.getChildren().add(label);
        }

    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }
}
