package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class TwoOptionAlert {

    Stage stage = new Stage();
    Scene scene ;
    TextField textField;
    Button back;
    Button ok;
    AnchorPane anchorPane = new AnchorPane();

        public TwoOptionAlert(){

            stage.setTitle("خطا");
            stage.setMinHeight(300);
            stage.setMinWidth(500);
            stage.setX(700);
            stage.setY(400);


//        testName.setStyle("-fx-focus-color: transparent; ; -fx-faint-focus-color: transparent;");


            back = new Button("بر گشت");
            back.setFont(new Font("Calibri Light", 23));
            back.setStyle("-fx-background-color: #FCF3CF");
            back.setLayoutX(80);
            back.setLayoutY(170);
            back.setOnAction(e ->stage.close());

            ok = new Button("حذف");
            ok.setFont(new Font("Calibri Light", 23));
            ok.setStyle("-fx-background-color: #FCF3CF");
            ok.setLayoutX(300);
            ok.setLayoutY(170);


            anchorPane.getChildren().addAll(back,ok);
            Image image = new Image("GUI\\pics\\alert.png");
            BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                    new BackgroundSize(500,250,false,false,false,false));
            anchorPane.setBackground(new Background(background));

        }

        public TextField getTextField(){
            return textField;
        }

        public Button getOk(){
            return ok;
        }

        public Stage getStage(){
            return stage;
        }

        public void display(){
            scene = new Scene(anchorPane);
            stage.setScene(scene);
            stage.show();
        }

}

