package GUI;

import Logic.Manager;
import Logic.Quiz;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class Charts {

    private BorderPane borderPane=new BorderPane();

    public Charts(){

        borderPane.setMinSize(1000,1400);

        AnchorPane right=new AnchorPane();
        right.setMinSize(100,800);
        AnchorPane left=new AnchorPane();
        left.setMinSize(100,800);
        AnchorPane top=new AnchorPane();
        top.setMinSize(1300,100);
        AnchorPane bottom=new AnchorPane();
        bottom.setMinSize(100,800);
        borderPane.setRight(right);
        borderPane.setBottom(bottom);
        borderPane.setLeft(left);
        borderPane.setTop(top);

        Image image=new Image("GUI\\pics\\background.jpg");
        BackgroundImage bgImg = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(1035,800,false,false,false,false));
        borderPane.setBackground(new Background(bgImg));

    }
    public void quizByQuiz(Manager manager){
        VBox vBox=new VBox();
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("آزمون ها");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("میانگین نمرات");
        BarChart barChart=new BarChart(xAxis,yAxis);
        XYChart.Series dataSeries1 = new XYChart.Series();
        for(int i=0;i<manager.getQuizes().size();i++){
            int sum=0;
            for(int j=0;j<manager.getQuizes().get(i).getStudents().size();j++){
                for(int k=0;k<manager.getQuizes().get(i).getStudents().get(j).getQuizzes().size();k++){
                    if(manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getName().equals(manager.getQuizes().get(i).getName())) {
                        sum+=manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getGrade();
                        /*for(int z=0;z<manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().size();z++) {
                            if(manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().get(z)!=null) {
                                sum += manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().get(z).getGrade();
                                break;
                            }
                        }*/
                    }
                }
            }
            if(manager.getQuizes().get(i).getStudents().size()!=0)
            sum/=manager.getQuizes().size();
            String[] quizNameTrim=manager.getQuizes().get(i).getName().split("#");
            StringBuffer stringBuffer=new StringBuffer();
            for(int j=1;j<quizNameTrim.length;j++)stringBuffer.append(quizNameTrim[j]);
            dataSeries1.getData().add(new XYChart.Data(quizNameTrim[0], sum));

        }
        barChart.getData().add(dataSeries1);
        vBox.getChildren().add(barChart);
        vBox.setStyle("-fx-background-color: #F9E79F");
        borderPane.setCenter(vBox);
    }

    public void personByPerson(Quiz quiz){
        VBox vBox=new VBox();
        CategoryAxis xAxis    = new CategoryAxis();
        xAxis.setLabel("دانشجویان");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("نمرات");
        BarChart barChart=new BarChart(xAxis,yAxis);
        XYChart.Series dataSeries1 = new XYChart.Series();
        for(int i=0;i<quiz.getStudents().size();i++){
            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++) {
                if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {
                    /*int sum=0;
                    for(int k=0;k<quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().size();k++){
                        if(quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().get(k)!=null){
                            sum+=quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().get(k).getGrade();
                        }
                    }*/
                    dataSeries1.getData().add(new XYChart.Data(quiz.getStudents().get(i).getName(), quiz.getStudents().get(i).getQuizzes().get(j).getGrade()));
                    break;
                }
            }
        }
        System.out.println("is null?"+quiz==null);
        System.out.println(quiz.getName());
        String[] quizNameTrim=quiz.getName().split("#");
        StringBuffer stringBuffer=new StringBuffer();
        for(int j=1;j<quizNameTrim.length;j++)stringBuffer.append(quizNameTrim[j]);
        dataSeries1.setName(quizNameTrim[0]);
        barChart.setStyle("-fx-background-color: #F9E79F");
        barChart.getData().add(dataSeries1);
        vBox.getChildren().add(barChart);
        vBox.setStyle("-fx-background-color: #F9E79F");
        borderPane.setCenter(vBox);

    }

    public BorderPane getBorderPane() {
        return borderPane;
    }
}
