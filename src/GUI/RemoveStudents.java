package GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class RemoveStudents {

    Button remove;
    Stage window;

    public void display(int x, int y) {

        Stage window = new Stage();
        window.setMinHeight(250);
        window.setMinWidth(500);
        window.setX(x);
        window.setY(y);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("حذف دانشجو");
        Label label=new Label();
        label.setText("آیا از حذف این دانشجو مطمئن هستید ؟");
        label.setFont(new Font("Calibri Light", 23));
        label.setLayoutY(50);
        label.setLayoutX(90);

        remove = new Button("حذف کردن");
        remove.setLayoutX(280);
        remove.setLayoutY(115);
        remove.setFont(new Font("Calibri Light", 20));
        remove.setStyle("-fx-background-color: #FCF3CF");

        Button back = new Button("بر گشت");
        back.setFont(new Font("Calibri Light", 20));
        back.setStyle("-fx-background-color: #FCF3CF");
        back.setLayoutY(115);
        back.setLayoutX(90);
        back.setOnAction(e ->window.close());

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(label,remove,back);

        Image image = new Image("GUI\\pics\\alert.png");
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                new BackgroundSize(500,250,false,false,false,false));
        anchorPane.setBackground(new Background(background));

        Scene scene=new Scene(anchorPane);
        this.window=window;
        window.setScene(scene);
        window.show();
    }

    public Button getRemoveButton(){
        return remove;
    }

    public Stage getWindow() {
        return window;
    }
}
