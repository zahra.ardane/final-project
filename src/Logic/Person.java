package Logic;

public abstract class Person {
    private String name;
    private String surName;
    private String accountNumber;
    private String password;
    public static final int NAME_LENGTH=50;
    public static final int RECORD_LENGTH=16+NAME_LENGTH*4;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String  password) {
        this.password = password;
    }
}
