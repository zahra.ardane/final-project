package Logic;

import java.util.ArrayList;

public class Answer {
    private String answer;
    private ArrayList<Integer>yesNoAnswers=new ArrayList<>();
    private String imageUrl;
    private double grade;
    public static final int YESNOSIZE=50;

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAnswer() {
        return answer;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public ArrayList<Integer> getYesNoAnswers() {
        return yesNoAnswers;
    }

    public void setYesNoAnswer(int index,int string) {
        yesNoAnswers.set(index,string);
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public double getGrade() {
        return grade;
    }

    public void setYesNoAnswers(ArrayList<Integer> yesNoAnswers) {
        this.yesNoAnswers = yesNoAnswers;
    }
}
