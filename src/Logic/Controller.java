package Logic;

import GUI.AlertBox;
import GUI.Assembler;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;

import javax.imageio.ImageIO;

public class Controller {

    Assembler assembler ;
    Information information = new Information();
    Quiz quiz=new Quiz() ;
    Person person;
    Timer timer;
    Client client;

    public Controller(Stage stage){
        assembler = new Assembler(stage);
        assembler.start.getManagerPanel().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.showManagerLogin();
            }
        });
        assembler.start.getStudentPanel().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.showStudentLogin();
            }
        });
        assembler.managerSignUp.getBackButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.showStartPage();
            }
        });
        assembler.managerLogin.getEnterButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ULocale locale=new ULocale("fa_IR@calendar=persian");
                Calendar calendar=Calendar.getInstance(locale);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH)+1;
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int minute = calendar.get(Calendar.MINUTE);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                String text=assembler.managerLogin.getAccountNumberField().getText();
                String password=assembler.managerLogin.getPasswordField().getText();
                if(information.isManagerRegistered(text)){
                    if(information.isManagerPasswordCorrect(text,password)){
                        person=information.readManager(text);
                        client = new Client(assembler,person);
                        assembler.showManagerPanel();
                        setEventHandlerOnTreeView();
                        Manager manager=(Manager)person;
                        for(int i=0;i<manager.getQuizes().size();i++){
                            Quiz tempQuiz=manager.getQuizes().get(i);
                            String[]splitName=manager.getQuizes().get(i).getName().split("#");
                            assembler.managerPanel.addItemToManageAndChat(splitName[0]);
                            assembler.newChat(manager.getQuizes().get(i).getName(),manager.getName());
                            if(tempQuiz.getYear()<year)assembler.managerPanel.addItemsToHistoryAndCorrection(splitName[0]);
                            if(tempQuiz.getYear()==year&&tempQuiz.getMonth()<month)assembler.managerPanel.addItemsToHistoryAndCorrection(splitName[0]);
                            if(tempQuiz.getYear()==year&&tempQuiz.getMonth()==month&&tempQuiz.getDay()<day)assembler.managerPanel.addItemsToHistoryAndCorrection(splitName[0]);
                            else if(tempQuiz.getQuestionsShowedAtOnce()&&tempQuiz.getDay()==day&&tempQuiz.getYear()==year&&tempQuiz.getMonth()==month){
                                if(tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + tempQuiz.getStartMinute() * 60 + tempQuiz.getStopHour() * 3600 +
                                        tempQuiz.getStopMinute() * 60 < day * 24 * 3600 + hour * 3600 + minute * 60)
                                    assembler.managerPanel.addItemsToHistoryAndCorrection(splitName[0]);
                            }
                            else if(tempQuiz.getDay()==day){
                                int mins=0;
                                int hours=0;
                                for(int j=0;j<tempQuiz.getQuestions().size();j++){
                                    mins+=tempQuiz.getQuestions().get(j).getMinuet();
                                    hours+=tempQuiz.getQuestions().get(j).getHour();

                                }
                                hours=hours+mins/60;
                                mins=mins%60;
                                if (tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + tempQuiz.getStartMinute() * 60 + tempQuiz.getStopHour() * 3600 +
                                        tempQuiz.getStopMinute() * 60 < day * 24 * 3600 + hour * 3600 + minute * 60)
                                    assembler.managerPanel.addItemsToHistoryAndCorrection(splitName[0]);

                            }
                        }
                    } else {
                        assembler.loginAlertBox();
                        System.out.println("no");
                    }
                } else {
                    assembler.notRegistered();
                }
            }
        });
        assembler.studentLogin.getEnterButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text=assembler.studentLogin.getAccountNumberField().getText();
                String password=assembler.studentLogin.getPasswordField().getText();
                String regex = "\\d+";
                if(information.isStudentRegistered(text)){
                    person=information.readStudent(text);

                    System.out.println(person.getPassword());
                    Student student=(Student)person;
                    System.out.println(student.getQuizzes().size());
                    if(information.isStudentPasswordCorrect(text,password)){
                        person=information.readStudent(text);
                        Student student1 = (Student) person;
                        client = new Client(assembler,person);
                        ULocale locale=new ULocale("fa_IR@calendar=persian");
                        Calendar calendar=Calendar.getInstance(locale);
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH)+1;
                        int day = calendar.get(Calendar.DAY_OF_MONTH);
                        int minute = calendar.get(Calendar.MINUTE);
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        for(int i=0;i<student.getQuizzes().size();i++){
                            Quiz temp = student.getQuizzes().get(i);
                            String[] quizSplit=temp.getName().split("#");
                            assembler.studentPanel.addItemsToChat(quizSplit[0]+"."+quizSplit[1]);
//                            int sw=0;
                            if(temp.getQuestionsShowedAtOnce()) {
                                int quizMonthSec, monthSec;
                                if (temp.getMonth() >= 6) {
                                    quizMonthSec = temp.getMonth() * 30 * 24 * 3600;
                                    monthSec = month * 30 * 24 * 3600;
                                } else {
                                    quizMonthSec = temp.getMonth() * 31 * 24 * 3600;
                                    monthSec = month * 31 * 24 * 3600;
                                }
                                if (temp.getYear() * 365 * 24 * 3600 + quizMonthSec + temp.getDay() * 24 * 3600 + (temp.getStopHour()+temp.getStartHour()) * 3600
                                        + (temp.getStopMinute()+temp.getStartMinute()) * 60 < year * 365 * 24 * 3600 + monthSec + day * 24 * 3600 + hour * 3600 + minute * 60) {
                                    assembler.studentPanel.addItemsToHeldTests(quizSplit[0]);
                                } else {
                                    assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                }
                            }
                            else{
                                int quizMonthSec, monthSec;
                                if (temp.getMonth() >= 6) {
                                    quizMonthSec = temp.getMonth() * 30 * 24 * 3600;
                                    monthSec = month * 30 * 24 * 3600;
                                } else {
                                    quizMonthSec = temp.getMonth() * 31 * 24 * 3600;
                                    monthSec = month * 31 * 24 * 3600;
                                }
                                int hours=0;
                                int mins=0;
                                for(int j=0;j<temp.getQuestions().size();j++){
                                    hours+=temp.getQuestions().get(j).getHour();
                                    mins+=temp.getQuestions().get(j).getMinuet();
                                }
                                hours=hours+mins/60;
                                mins=mins%60;
                                if (temp.getYear() * 365 * 24 * 3600 + quizMonthSec + temp.getDay() * 24 * 3600 + (hours+temp.getStartHour()) * 3600
                                        + (mins+temp.getStartMinute()) * 60 < year * 365 * 24 * 3600 + monthSec + day * 24 * 3600 + hour * 3600 + minute * 60) {
                                    assembler.studentPanel.addItemsToHeldTests(quizSplit[0]);
                                } else {
                                    assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                }


                            }
//                            if(temp.getYear()>=year&&temp.getMonth()>=month){
//                                if(temp.getDay()*24*3600+temp.getStartHour()*3600+temp.getStartMinute()*60+temp.getStopHour()*3600+
//                                        temp.getStopMinute()*60>day*24*3600+
//                                        hour*3600+minute*60){
//                                    sw=1;
//                                    assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
//                                }
//                            }

                            /*if(temp.getYear()> year){
                                System.out.println("m");
                                assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                sw=1;
                            }
                            else if(temp.getYear()==year){
                                System.out.println("e");
                                if(temp.getMonth()>month) {
                                    System.out.println(month);
                                    DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, locale);

                                    System.out.println(df.format(calendar));
                                    System.out.println("r");
                                    assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                    sw=1;
                                }
                                else if(temp.getMonth()==month){
                                    System.out.println("u");
                                    if(temp.getDay()>day){
                                        System.out.println("l");
                                        assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                        sw=1;
                                    }
                                    else if(temp.getDay()==day){
                                        System.out.println("a");
                                        System.out.println(temp.getStartHour());
                                        System.out.println(temp.getStartMinute());
                                        System.out.println(temp.getStopHour());
                                        System.out.println(temp.getStopMinute());
                                        if((temp.getStartHour()+temp.getStopHour()+(temp.getStartMinute()+temp.getStopMinute())/60)%24>hour){
                                            System.out.println("s");
                                            assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                            sw=1;
                                        }
                                        else if((temp.getStartHour()+temp.getStopHour()+(temp.getStartMinute()+temp.getStopMinute())/60)==hour){
                                            System.out.println("n");
                                            System.out.println(temp.getStartMinute());
                                            System.out.println(temp.getStopMinute());
                                            if((temp.getStartMinute()+temp.getStopMinute())%60>=minute){
                                                System.out.println("y");
                                                assembler.studentPanel.addItemsToNotYetHeldTests(quizSplit[0]);
                                                sw=1;
                                            }
                                        }
                                    }
                                }

                            }*/
//                            if(sw==0){
//                                assembler.studentPanel.addItemsToHeldTests(quizSplit[0]);
//                            }
                            /*else {
                                assembler.studentPanel.addItemsToHeldTests(temp.getName());
                            }*/
                            assembler.newChat(temp.getName(),student.getName());
                        }
                        assembler.showStudentPanel();
                        setEventHandlerOnTreeView();
                    }
                    else {
                        assembler.loginAlertBox();
                        System.out.println("no");
                    }
                } else {
                    assembler.notRegistered();
                }
            }
        });
        assembler.managerLogin.getRegisterButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.showManagerSignUp();
            }
        });
        assembler.studentLogin.getRegisterButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.showStudentSignUp();
            }
        });
        assembler.managerSignUp.getSignUpButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String accountNumber=assembler.managerSignUp.getUserNameTextField().getText();
                if(!(information.isAccountNumberUsed(accountNumber))) {
                    System.out.println("accountNumber is used");
                    String password = assembler.managerSignUp.getPasswordTextField().getText();
                    String name = assembler.managerSignUp.getNameTextField().getText();
                    String lastName = assembler.managerSignUp.getLastNameTextField().getText();
                    Manager manager = new Manager();
                    manager.setName(name);
                    manager.setPassword(password);
                    manager.setAccountNumber(accountNumber);
                    manager.setSurName(lastName);
                    information.writeNewManager(manager);
                    person=manager;
                    assembler.showManagerPanel();
                    setEventHandlerOnTreeView();

                } else {
                    assembler.takenUserNameAlertBox();
                }
            }
        });
        assembler.studentSignUp.getSignUpButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String studentId = assembler.studentSignUp.getIdTextField().getText();
                String accountNumber = assembler.studentSignUp.getUserNameTextField().getText();
                String regex = "\\d+";
                if (studentId.matches(regex)) {
                    if (!(information.isAccountNumberUsed(accountNumber))) {
                        if (!(information.isStudentIdUsed(Long.parseLong(studentId)))) {
                            String password = assembler.studentSignUp.getPasswordTextField().getText();
                            //System.out.println(password);
                            String name = assembler.studentSignUp.getNameTextField().getText();
                            String lastName = assembler.studentSignUp.getLastNameTextField().getText();
                            Student student = new Student();
                            student.setName(name);
                            student.setPassword(password);
                            student.setAccountNumber(accountNumber);
                            student.setSurName(lastName);
                            student.setStudentId(Long.parseLong(studentId));
                            information.writeNewStudent(student);
                            person = student;
                            assembler.showStudentPanel();
                        } else {
                            assembler.takenIdAlertBox();
                        }
                    } else {
                        assembler.takenUserNameAlertBox();
                    }
                }
                else{
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("خطا","شماره دانشجویی اشتباه است",720,400);
                }
            }
        });
        assembler.twoOptionAlert.getOk().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                quiz=null;
            }
        });
        assembler.manageInformation.getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saveMethod();
            }
        });
        assembler.quizInfo.getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saveMethod();
            }
        });
        assembler.addManually.getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saveMethod();
            }
        });
        assembler.manageStudentsParticipation.getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saveMethod();
            }
        });
        assembler.getQuestionMaking().getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               /* if (quiz!=null&&quiz.getName() != null) {
                    System.out.println(quiz.getName());
                    System.out.println("phase one");
                    if (!quiz.getQuestionsShowedAtOnce()) {
                        int sw = 0;
                        for (int i = 0; i < quiz.getQuestions().size(); i++) {
                            if (quiz.getQuestions().get(i).getHour() == 0 && quiz.getQuestions().get(i).getMinuet() == 0)
                                sw = 1;
                        }
                        if (sw == 0) {
                            Manager manager = (Manager) person;
                            manager.getQuizes().add(quiz);
                            information.writeQuizForManager(manager, quiz);
                            manager.getQuizes().add(quiz);
                            for (int i = 0; i < quiz.getStudents().size(); i++) {
                                for (int j = 0; j < quiz.getStudents().get(i).getQuizzes().size(); j++) {
                                    if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {

                                        information.writeQuizForStudents(quiz.getStudents().get(i), quiz.getStudents().get(i).getQuizzes().get(j));

                                    }
                                }
                            }
                            assembler.getQuestionMaking().setCurrentQuestion(0);
                            assembler.getQuestionMaking().newPage();
                            assembler.getAddManually().setAddManuallyNull();
                            for(int i=0;i<quiz.getStudents().size();i++){
                                for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                                    if(quiz.getName().equals(quiz.getStudents().get(i).getQuizzes().get(j).getName())){
                                        Quiz quiz1=quiz.getStudents().get(i).getQuizzes().get(j);
                                        quiz1.setName(quiz.getName());
                                        quiz1.setStopMinute(quiz.getStopMinute());
                                        quiz1.setStopHour(quiz.getStopHour());
                                        quiz1.setStartMinute(quiz.getStartMinute());
                                        quiz1.setStartHour(quiz.getStartHour());
                                        quiz1.setDay(quiz.getDay());
                                        quiz1.setYear(quiz.getYear());
                                        quiz1.setMonth(quiz.getMonth());
                                        quiz1.setQuestions(quiz.getQuestions());
                                        information.writeNewStudent(quiz.getStudents().get(i));
                                    }
                                }
                            }
                            String[]quizSplit=quiz.getName().split("#");
                            assembler.managerPanel.addItemToManageAndChat(quizSplit[0]);
                            //the next line is only here for debugging process , remember to delete
                            //assembler.managerPanel.addItemsToHistoryAndCorrection(quizSplit[0]);
                            assembler.managerPanel.setTestCreated(false);
                            quiz=null;
                            information.writeNewManager(manager);
                        } else {
                            AlertBox alertBox = new AlertBox();
                            alertBox.display("خطا","زمان سوالات تعیین نشده", 720, 400);
                        }
                    } else {
                        System.out.println("phase two");
                        Manager manager = (Manager) person;
                        manager.getQuizes().add(quiz);
                        information.writeQuizForManager(manager, quiz);
                        for (int i = 0; i < quiz.getStudents().size(); i++) {
                            for (int j = 0; j < quiz.getStudents().get(i).getQuizzes().size(); j++) {
                                if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {
                                    System.out.println("phase3");
                                    information.writeQuizForStudents(quiz.getStudents().get(i), quiz.getStudents().get(i).getQuizzes().get(j));


                                }
                            }


                        }

                        assembler.getQuestionMaking().setCurrentQuestion(0);
                        assembler.getQuestionMaking().newPage();
                        assembler.getAddManually().setAddManuallyNull();
                        for(int i=0;i<quiz.getStudents().size();i++){
                            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                                if(quiz.getName().equals(quiz.getStudents().get(i).getQuizzes().get(j).getName())){
                                    Quiz quiz1=quiz.getStudents().get(i).getQuizzes().get(j);
                                    quiz1.setName(quiz.getName());
                                    quiz1.setStopMinute(quiz.getStopMinute());
                                    quiz1.setStopHour(quiz.getStopHour());
                                    quiz1.setStartMinute(quiz.getStartMinute());
                                    quiz1.setStartHour(quiz.getStartHour());
                                    quiz1.setDay(quiz.getDay());
                                    quiz1.setYear(quiz.getYear());
                                    quiz1.setMonth(quiz.getMonth());
                                    quiz1.setQuestions(quiz.getQuestions());
                                    information.writeNewStudent(quiz.getStudents().get(i));
                                }
                            }
                        }
                        String[] quizSplit=quiz.getName().split("#");
                        assembler.managerPanel.addItemToManageAndChat(quizSplit[0]);
                        assembler.managerPanel.setTestCreated(false);
                        quiz=null;
                        information.writeNewManager(manager);
                    }
                }
                else{
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("خطا","نام آزمون انتخاب نشده است",720,400);
                }*/
         saveMethod();
            }
        });
        assembler.manageStudentsParticipation.getAddButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String stringId = assembler.manageStudentsParticipation.getStudentId().getText();
                int sw = 0;
                for (int i = 0; i < quiz.getStudents().size(); i++) {
                    if (quiz.getStudents().get(i).getStudentId() == Long.parseLong(stringId)) sw = 1;
                }
                if (sw == 0) {
                    if (!(information.isStudentIdUsed(Long.parseLong(stringId)))) {
                        Student student = new Student();
                        student.setStudentId(Long.parseLong(stringId));
                        student.setAccountNumber(stringId);
                        student.setPassword(stringId);
                        student.setName(assembler.manageStudentsParticipation.getName().getText());
                        student.setSurName(assembler.manageStudentsParticipation.getSurName().getText());
                        assembler.manageStudentsParticipation.setVBox(student.getName()+" "+student.getSurName()+" "+
                                student.getStudentId()+"اضافه شده _");
                        assembler.manageStudentsParticipation.getSurName().setText(null);
                        assembler.manageStudentsParticipation.getName().setText(null);
                        assembler.manageStudentsParticipation.getStudentId().setText(null);
                        Quiz quiz1=new Quiz();
                        quiz1.setName(quiz.getName());
                        quiz1.setStopMinute(quiz.getStopMinute());
                        quiz1.setStopHour(quiz.getStopHour());
                        quiz1.setStartMinute(quiz.getStartMinute());
                        quiz1.setStartHour(quiz.getStartHour());
                        quiz1.setDay(quiz.getDay());
                        quiz1.setYear(quiz.getYear());
                        quiz1.setMonth(quiz.getMonth());
                        quiz1.setQuestions(quiz.getQuestions());
                        quiz.addStudent(student);
                        student.getQuizzes().add(quiz1);
                    } else {
                        Student student = information.readStudentWithId(Long.parseLong(stringId));
                        if (student.getName().equals(assembler.manageStudentsParticipation.getName().getText())) {
                            if (student.getSurName().equals(assembler.manageStudentsParticipation.getSurName().getText())) {
                                assembler.manageStudentsParticipation.setVBox(student.getName()+" "+student.getSurName()+" "+
                                        student.getStudentId()+"اضافه شده _");
                                assembler.manageStudentsParticipation.getSurName().setText(null);
                                assembler.manageStudentsParticipation.getName().setText(null);
                                assembler.manageStudentsParticipation.getStudentId().setText(null);
                                quiz.addStudent(student);
                                Quiz quiz1=new Quiz();
                                quiz1.setName(quiz.getName());
                                quiz1.setStopMinute(quiz.getStopMinute());
                                quiz1.setStopHour(quiz.getStopHour());
                                quiz1.setStartMinute(quiz.getStartMinute());
                                quiz1.setStartHour(quiz.getStartHour());
                                quiz1.setDay(quiz.getDay());
                                quiz1.setYear(quiz.getYear());
                                quiz1.setMonth(quiz.getMonth());
                                quiz1.setQuestions(quiz.getQuestions());
                                student.addQuiz(quiz1);
                            } else System.out.println("wrong surName");

                        } else System.out.println("wrong name");
                    }
                }
            }
        });
        assembler.manageStudentsParticipation.getDepriveButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String regex = "\\d+";
                if (assembler.manageStudentsParticipation.getStudentId().getText().matches(regex)) {
                    long checkId = Long.parseLong(assembler.manageStudentsParticipation.getStudentId().getText());
                    String name = assembler.manageStudentsParticipation.getName().getText();
                    String surName = assembler.manageStudentsParticipation.getSurName().getText();
                    int sw=1;
                    for (int i = 0; i <quiz.getStudents().size();i++){
                        if(quiz.getStudents().get(i).getStudentId()==checkId&&quiz.getStudents().get(i).getName().equals(name)&&
                                quiz.getStudents().get(i).getSurName().equals(surName)){
                            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                                if(quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())){
                                    quiz.getStudents().get(i).getQuizzes().get(j).setBanned(true);
                                    sw=0;
                                    assembler.manageStudentsParticipation.setVBox(quiz.getStudents().get(i).getName()+" "+
                                            quiz.getStudents().get(i).getSurName()+" "+quiz.getStudents().get(i).getStudentId()+
                                            "محروم _");
                                    assembler.manageStudentsParticipation.getSurName().setText(null);
                                    assembler.manageStudentsParticipation.getName().setText(null);
                                    assembler.manageStudentsParticipation.getStudentId().setText(null);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    if(sw==1){
                        AlertBox alertBox=new AlertBox();
                        alertBox.display("خطا","دانشجوی موردنظز یافت نشد",720,400);
                    }
                }
                else{
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("خطا","شماره دانشجویی به درستی وارد نشده",700,420);
                }
            }
        });
        assembler.manageStudentsParticipation.getRemoveButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String regex = "\\d+";
                if (assembler.manageStudentsParticipation.getStudentId().getText().matches(regex)) {
                    long checkId = Long.parseLong(assembler.manageStudentsParticipation.getStudentId().getText());
                    String name = assembler.manageStudentsParticipation.getName().getText();
                    String surName = assembler.manageStudentsParticipation.getSurName().getText();
                    int sw=1;
                    System.out.println(quiz.getStudents().size());
                    System.out.println(quiz.getName());
                    for(int i=0;i<quiz.getStudents().size();i++){
                        System.out.println(quiz.getStudents().get(i).getStudentId());
                        System.out.println(quiz.getStudents().get(i).getName());
                        System.out.println(quiz.getStudents().get(i).getSurName());
                        if(quiz.getStudents().get(i).getStudentId()==checkId&&quiz.getStudents().get(i).getName().equals(name)&&
                                quiz.getStudents().get(i).getSurName().equals(surName)){
                            sw=0;
                            assembler.manageStudentsParticipation.setVBox(quiz.getStudents().get(i).getName()+" "+
                                    quiz.getStudents().get(i).getSurName()+" "+quiz.getStudents().get(i).getStudentId()+
                                    "حذف _");
                            information.deleteExamForStudent(quiz.getName(),quiz.getStudents().get(i));
                            quiz.getStudents().remove(i);
                            assembler.manageStudentsParticipation.getSurName().setText(null);
                            assembler.manageStudentsParticipation.getName().setText(null);
                            assembler.manageStudentsParticipation.getStudentId().setText(null);

                        }
                    }
                    if(sw==1){
                        AlertBox alertBox=new AlertBox();
                        alertBox.display("خطا","دانشجوی مورد نظر یافت نشد",720,400);
                    }
                }
                else{
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("خطا","شماره دانشجویی به درستی وارد نشده",700,420);
                }
            }
        });

        assembler.getQuestionMaking().getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    assembler.getQuestionMaking().setQuiz(quiz);
                    int isAnswerPresent = 0;
                    if (quiz != null && quiz.getName() != null) {
                        ArrayList<String>multiples=new ArrayList<>();
                        ArrayList<String>yesNo=new ArrayList<>();
                        if (assembler.getQuestionMaking().getTextArea().getText() != null) {
                            Question question = new Question();
                            if(assembler.getQuestionMaking().getGrade().getText()!=null&&!assembler.getQuestionMaking().getGrade().getText().equals("")){
                                System.out.println(assembler.getQuestionMaking().getGrade().getText());
                                System.out.println("done");
                                question.setGrade(Double.parseDouble(assembler.getQuestionMaking().getGrade().getText()));}
                            else if(assembler.getQuestionMaking().getGrade().getText()==null||assembler.getQuestionMaking().getGrade().getText().equals("")){
                                question.setGrade(0);
                            }
                            if(assembler.getQuestionMaking().getHour().getText()!=null&&!assembler.getQuestionMaking().getHour().getText().equals(""))
                            question.setHour(Integer.parseInt(assembler.getQuestionMaking().getHour().getText()));
                            else question.setHour(0);
                            if(assembler.getQuestionMaking().getMinute().getText()!=null&&!assembler.getQuestionMaking().getMinute().getText().equals(""))
                            question.setMinuet(Integer.parseInt(assembler.getQuestionMaking().getMinute().getText()));
                            else question.setMinuet(0);
                            question.setContext(assembler.getQuestionMaking().getTextArea().getText());
                            if (assembler.getQuestionMaking().getTwoAnswer().isSelected()) {
                                question.setType(3);
                                System.out.println(assembler.getQuestionMaking().getYesNoQuestionsNumbers());
                                for (int i = 0; i < assembler.getQuestionMaking().getYesNoQuestionsNumbers(); i++) {
                                    if (assembler.getQuestionMaking().getYesNoQuestions().get(i).getText() != null&&
                                    !assembler.getQuestionMaking().getYesNoQuestions().get(i).getText().equals("")) {
                                        System.out.println(assembler.getQuestionMaking().getYesNoQuestions().get(i).getText());
                                        yesNo.add(assembler.getQuestionMaking().getYesNoQuestions().get(i).getText());
                                       isAnswerPresent = 1;
                                    }
                                }
                                question.setTrueFalsePhrases(yesNo);
                            } else if (assembler.getQuestionMaking().getFourAnswer().isSelected()) {
                                question.setType(2);
                                System.out.println(assembler.getQuestionMaking().getMultipleQuestionsNumber());
                                for (int i = 0; i < assembler.getQuestionMaking().getMultipleQuestionsNumber(); i++) {
                                    if (assembler.getQuestionMaking().getMultiples().get(i).getText() != null&&
                                            !assembler.getQuestionMaking().getMultiples().get(i).getText().equals("")) {
                                        multiples.add(assembler.getQuestionMaking().getMultiples().get(i).getText());
                                        isAnswerPresent = 1;
                                    }
                                }
                                question.setOptions(multiples);
                            } else if (assembler.getQuestionMaking().getDescriptive().isSelected()) {
                                isAnswerPresent = 1;
                                question.setType(1);
                            }
                            if (isAnswerPresent == 1) {
                                if (quiz.getQuestions().size() == assembler.getQuestionMaking().getCurrentQuestion()) {
                                    quiz.addQuestions(question);
                                    //quiz.getOrder().add(quiz.getQuestions().size()-1,quiz.getQuestions().size()-1);
                                    assembler.questionSaved();
                                    assembler.getQuestionMaking().setCurrentQuestion(quiz.getQuestions().size());
                                    assembler.getQuestionMaking().fixQuestionMaking();
                                } else {
                                    quiz.getQuestions().set(assembler.getQuestionMaking().getCurrentQuestion(), question);
                                    AlertBox alertBox = new AlertBox();
                                    alertBox.display(null, "ذخیره دانشجو با موفقیت انجام شد", 700, 400);
                                    assembler.getQuestionMaking().setCurrentQuestion(quiz.getQuestions().size());
                                    assembler.getQuestionMaking().fixQuestionMaking();

                                }
                            /*quiz.getQuestions().add(assembler.getQuestionMaking().getCurrentQuestion(),question);
                            assembler.getQuestionMaking().getTextArea().setText(null);
                            assembler.getQuestionMaking().setMultipleQuestionsNumber(2);
                            assembler.getQuestionMaking().setMultipleQuestionsNumber(2);
                            assembler.getQuestionMaking().getFourAnswer().setSelected(false);
                            assembler.getQuestionMaking().getTwoAnswer().setSelected(false);
                            assembler.getQuestionMaking().getDescriptive().setSelected(true);
                            assembler.getQuestionMaking().setTextsNull();*/
                            } else {
                                AlertBox alertBox = new AlertBox();
                                alertBox.display("خطا", "گزینه ها خالی است", 700, 400);
                            }
                        }
                    }
                }
                catch (NumberFormatException e){
                    System.out.println("whattttt");
                }
            }
        });
        assembler.getQuestionMaking().getRemove().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (quiz != null) {
                    if (assembler.getQuestionMaking().getCurrentQuestion() == quiz.getQuestions().size()) {
                        assembler.getQuestionMaking().fixQuestionMaking();
                    } else {
                        quiz.getQuestions().remove(assembler.getQuestionMaking().getCurrentQuestion());
                        assembler.getQuestionMaking().setCurrentQuestion(quiz.getQuestions().size());
                        assembler.getQuestionMaking().fixQuestionMaking();
                    }
                }
            }

        });
        assembler.takeTest.getFile().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student=(Student)person;
                String fileName = student.getStudentId() + "#" + student.getAccountNumber();
                if(quiz.getAnswers().get(quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())).getImageUrl()==null) {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".jpg","*.jpg"));
                    File file = fileChooser.showOpenDialog(null);


                    if(file!=null){
                        quiz.getAnswers().get(quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())).setImageUrl("w");
                        Image image=new Image(file.getAbsolutePath());
                        ImageView imageView=new ImageView(image);

                        File output=new File("src\\files\\students\\" + fileName
                                + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())
                        );
                        if(!output.exists())output.mkdirs();
                        output=new File("src\\files\\students\\" + fileName
                                + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())+"\\"+"w.jpg"
                        );
                        BufferedImage bufferedImage= SwingFXUtils.fromFXImage(image,null);
                        try {
                            ImageIO.write(bufferedImage,"jpg",output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else{

                    File file1=new File("src\\files\\students\\" + fileName
                            + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())+"\\"+"w.jpg");
                    Desktop dt=Desktop.getDesktop();
                    try {
                        dt.open(file1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        assembler.getQuestionMaking().getToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if(assembler.getQuestionMaking().getToggleGroup().getSelectedToggle()==assembler.getQuestionMaking().getFourAnswer()) {
                    assembler.getQuestionMaking().getLeftAnchor().setVisible(true);
                    assembler.getQuestionMaking().getYesNoPane().setVisible(false);
                    /*for(int i=0;i<assembler.getQuestionMaking().getMultipleQuestionsNumber();i++){
                        assembler.getQuestionMaking().getMultiples().get(i).setText(null);
                    }*/
                }
                else if(assembler.getQuestionMaking().getToggleGroup().getSelectedToggle()==assembler.getQuestionMaking().getTwoAnswer()){
                    assembler.getQuestionMaking().getLeftAnchor().setVisible(false);
                    assembler.getQuestionMaking().getYesNoPane().setVisible(true);
                   /* for(int i=0;i<assembler.getQuestionMaking().getYesNoQuestionsNumbers();i++){
                        assembler.getQuestionMaking().getYesNoQuestions().get(i).setText(null);
                    }*/
                }
                else {
                    assembler.getQuestionMaking().getLeftAnchor().setVisible(false);
                    assembler.getQuestionMaking().getYesNoPane().setVisible(false);

                }
            }
        });
        assembler.takeTest.getFile().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student=(Student)person;
                String fileName = student.getStudentId() + "#" + student.getAccountNumber();
                if(quiz.getAnswers().get(quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())).getImageUrl()==null) {
                    quiz.getAnswers().get(quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())).setImageUrl("w");
                    FileChooser fileChooser = new FileChooser();
                    File file = fileChooser.showOpenDialog(null);

                    if(file!=null){
                        System.out.println(file.getPath());
                        Image image=new Image(file.toURI().toString());
                        ImageView imageView=new ImageView(image);

                        File output=new File("src\\files\\students\\" + fileName
                                + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())
                        );
                   if(!output.exists())output.mkdirs();
                   output=new File("src\\files\\students\\" + fileName
                           + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())+"\\"+"w.jpg"
                   );
                        BufferedImage bufferedImage= SwingFXUtils.fromFXImage(image,null);
                        try {
                            ImageIO.write(bufferedImage,"jpg",output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else{
                    File file1=new File("src\\files\\students\\" + fileName
                            + "\\tests\\" + quiz.getName() + "\\answers\\" + quiz.getOrder().get(assembler.takeTest.getCurrentQuestion())+"\\"+"w.jpg");
                    System.out.println(file1.exists());
                    Desktop dt=Desktop.getDesktop();
                    try {
                        dt.open(file1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        assembler.getQuestionMaking().getAdd().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(assembler.getQuestionMaking().getTwoAnswer().isSelected())assembler.getQuestionMaking().addYesNoAnswer();
                else if(assembler.getQuestionMaking().getFourAnswer().isSelected())assembler.getQuestionMaking().addMultipleAnswer();
            }
        });
        assembler.getQuestionMaking().getNext().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.getQuestionMaking().setQuiz(quiz);
                assembler.getQuestionMaking().nextQuestion();
            }
        });
        assembler.getQuestionMaking().getPrevious().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                assembler.getQuestionMaking().setQuiz(quiz);
                assembler.getQuestionMaking().previousQuestion();
            }
        });
        //not checked and if studentId is already registered does nothing

        assembler.getAddManually().getAdd().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String stringId = assembler.getAddManually().getStudentId().getText();
                int sw = 0;
                for (int i = 0; i < quiz.getStudents().size(); i++) {
                    if (quiz.getStudents().get(i).getStudentId() == Long.parseLong(stringId)) sw = 1;
                }
                if (sw == 0) {
                    if (!(information.isStudentIdUsed(Long.parseLong(stringId)))) {
                        System.out.println("doesnt exist");
                        Student student = new Student();
                        student.setStudentId(Long.parseLong(stringId));
                        System.out.println(student.getStudentId());
                        student.setAccountNumber(stringId);
                        student.setPassword(stringId);
                        student.setName(assembler.getAddManually().getName().getText());
                        student.setSurName(assembler.getAddManually().getSurName().getText());
                        assembler.getAddManually().addStudent(student);
                        quiz.addStudent(student);
                        Quiz quiz1=new Quiz();
                        quiz1.setName(quiz.getName());
                        quiz1.setStopMinute(quiz.getStopMinute());
                        quiz1.setStopHour(quiz.getStopHour());
                        quiz1.setStartMinute(quiz.getStartMinute());
                        quiz1.setStartHour(quiz.getStartHour());
                        quiz1.setDay(quiz.getDay());
                        quiz1.setYear(quiz.getYear());
                        quiz1.setMonth(quiz.getMonth());
                        quiz1.setQuestions(quiz.getQuestions());
                        student.addQuiz(quiz1);
                    } else {
                        System.out.println("exists");
                        Student student = information.readStudentWithId(Long.parseLong(stringId));
                        System.out.println(student.getName());
                        System.out.println(student.getSurName());
                        if (student.getName().equals(assembler.getAddManually().getName().getText())) {
                            if (student.getSurName().equals(assembler.getAddManually().getSurName().getText())) {
                                assembler.addManually.addStudent(student);
                                quiz.addStudent(student);
                                Quiz quiz1=new Quiz();
                                quiz1.setName(quiz.getName());
                                quiz1.setStopMinute(quiz.getStopMinute());
                                quiz1.setStopHour(quiz.getStopHour());
                                quiz1.setStartMinute(quiz.getStartMinute());
                                quiz1.setStartHour(quiz.getStartHour());
                                quiz1.setDay(quiz.getDay());
                                quiz1.setYear(quiz.getYear());
                                quiz1.setMonth(quiz.getMonth());
                                quiz1.setQuestions(quiz.getQuestions());
                                student.addQuiz(quiz1);
                            } else System.out.println("wrong surName");

                        } else System.out.println("wrong name");
                    }
                }
                actionForAddManuallyCheckBoxes();
            }

        });
        assembler.correction.getNext().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(assembler.correction.getCurrentQuestion()<assembler.correction.getQuiz().getQuestions().size()-1){
                    if(assembler.correction.getGrade().getText()!=null&&!assembler.correction.getGrade().getText().equals(""))
                        assembler.correction.getQuiz().setGrade(Double.valueOf(assembler.correction.getGrade().getText()));
                    assembler.correction.setCurrentQuestion(assembler.correction.getCurrentQuestion()+1);
                    assembler.correction.fixCorrection();
                }
            }
        });
        assembler.correction.getPrevious().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(assembler.correction.getCurrentQuestion()>0){
                    if(assembler.correction.getGrade().getText()!=null&&!assembler.correction.getGrade().getText().equals(""))
                        assembler.correction.getQuiz().getAnswers().get(assembler.correction.getCurrentQuestion()).setGrade(Double.valueOf(assembler.correction.getGrade().getText()));
                    information.writeAnswerOfQuiz(assembler.correction.getQuiz().getAnswers().get(assembler.correction.getCurrentQuestion()), assembler.correction.getCurrentQuestion(), assembler.correction.getStudent(), assembler.correction.getQuiz());
                    assembler.correction.setCurrentQuestion(assembler.correction.getCurrentQuestion()-1);
                    assembler.correction.fixCorrection();
                }
            }
        });
        assembler.review.getPrevious().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(assembler.review.getCurrentQuestion()>0){
                    assembler.review.setCurrentQuestion(assembler.review.getCurrentQuestion()-1);
                    assembler.review.fixCorrection();
                }
            }
        });
        assembler.review.getNext().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(assembler.review.getCurrentQuestion()<assembler.review.getQuiz().getQuestions().size()-1){
                    assembler.review.setCurrentQuestion(assembler.review.getCurrentQuestion()+1);
                    assembler.review.fixCorrection();
                }
            }
        });
        assembler.review.getOpenFile().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student=(Student)person;
                String fileName = student.getStudentId() + "#" + student.getAccountNumber();
                File file1=new File("src\\files\\students\\" + fileName
                        + "\\tests\\" + assembler.review.getQuiz().getName() + "\\answers\\" + assembler.review.getCurrentQuestion()+"\\"+"w.jpg");
                Desktop dt=Desktop.getDesktop();
                try {
                    dt.open(file1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        assembler.correction.getOpenFile().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Student student=assembler.correction.getStudent();
                String fileName = student.getStudentId() + "#" + student.getAccountNumber();
                File file1=new File("src\\files\\students\\" + fileName
                        + "\\tests\\" + assembler.correction.getQuiz().getName() + "\\answers\\" + assembler.correction.getCurrentQuestion()+"\\"+"w.jpg");
                Desktop dt=Desktop.getDesktop();
                try {
                    dt.open(file1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        assembler.manageInformation.getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(quiz.getName());
                String startHour=assembler.manageInformation.getStartHour().getText();
                String startMinute=assembler.manageInformation.getStartMinute().getText();
                String stopMinute=assembler.manageInformation.getFinishMinute().getText();
                String stopHour=assembler.manageInformation.getFinishHour().getText();
                String day=assembler.manageInformation.getDayTextField().getText();
                String month=assembler.manageInformation.getMonthTextField().getText();
                String year=assembler.manageInformation.getYearTextField().getText();

                String regex = "\\d+";
                //if(stopMinute.matches(regex)&&stopMinute.matches(regex)
                //&&startHour.matches(regex)&&startMinute.matches(regex)){
                //if(Integer.parseInt(stopMinute)>=0&&Integer.parseInt(stopMinute)<60&&
                //Integer.parseInt(startMinute)>=0&&Integer.parseInt(startMinute)<60&&
                /*Integer.parseInt(assembler.quizInfo.getFinishHour().getText())>=0&&*///Integer.parseInt(startHour)<24&&
                //Integer.parseInt(startHour)<24){
                if(assembler.manageInformation.getStartHour().getText()!=null&&!assembler.manageInformation.getStartHour().getText().equals("")&&
                        startHour.matches(regex)&&Integer.parseInt(startHour)<24&&
                        Integer.parseInt(startHour)>=0)
                    quiz.setStartHour(Integer.parseInt(assembler.manageInformation.getStartHour().getText()));
                else if(startHour==null||startHour.equals(""))quiz.setStartHour(0);
                if(assembler.manageInformation.getStartMinute().getText()!=null&&!assembler.manageInformation.getStartMinute().getText().equals("")&&
                        startMinute.matches(regex)&&Integer.parseInt(startMinute)<60&&
                        Integer.parseInt(startMinute)>=0)
                    quiz.setStartMinute(Integer.parseInt(assembler.manageInformation.getStartMinute().getText()));
                else if(startMinute==null||startMinute.equals(""))quiz.setStartMinute(0);
                if(stopHour!=null&&!stopHour.equals("")&&
                        stopHour.matches(regex)&&Integer.parseInt(stopHour)>=0)
                    quiz.setStopHour(Integer.parseInt(assembler.manageInformation.getFinishHour().getText()));
                else if(stopHour==null||stopHour.equals(""))quiz.setStopHour(0);
                if(stopMinute!=null&&!stopMinute.equals("")&&Integer.parseInt(stopMinute)<60&&
                        stopMinute.matches(regex)&&Integer.parseInt(stopMinute)>=0)
                    quiz.setStopMinute(Integer.parseInt(assembler.manageInformation.getFinishMinute().getText()));
                else if(stopMinute==null||stopMinute.equals(""))quiz.setStopMinute(0);
                if(year!=null&&!year.equals("")&&Integer.parseInt(year)>0)quiz.setYear(Integer.parseInt(year));
                else if(year==null||year.equals(""))quiz.setYear(0);
                if(month!=null&&!month.equals("")&&Integer.parseInt(month)>0)quiz.setMonth(Integer.parseInt(month));
                else if(month==null||month.equals(""))quiz.setMonth(0);
                if(day!=null&&!day.equals("")&&Integer.parseInt(day)>0)quiz.setDay(Integer.parseInt(day));
                else if(day==null||day.equals(""))quiz.setYear(0);

                quiz.setReview(assembler.manageInformation.getReview().isSelected());
                quiz.setQuestionsShowedAtOnce(!assembler.manageInformation.getOneByOneQuestions().isSelected());
                quiz.setShuffle(assembler.manageInformation.getShuffle().isSelected());



                AlertBox alertBox=new AlertBox();
                alertBox.display("","ذخیره تغییرات با موفقیت انجام شد",720,400);
            }
        });
        assembler.quizInfo.getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    String startHour=assembler.quizInfo.getStartHour().getText();
                    String startMinute=assembler.quizInfo.getStartMinute().getText();
                    String stopMinute=assembler.quizInfo.getFinishMinute().getText();
                    String stopHour=assembler.quizInfo.getFinishHour().getText();
                String day=assembler.quizInfo.getDayTextField().getText();
                String month=assembler.quizInfo.getMonthTextField().getText();
                String year=assembler.quizInfo.getYearTextField().getText();
                System.out.println("start hour"+startHour);
                System.out.println(startMinute);
                System.out.println(stopMinute);
                System.out.println(stopHour);
                System.out.println(day);
                System.out.println(month);
                System.out.println(year);

                String regex = "\\d+";
                    //if(stopMinute.matches(regex)&&stopMinute.matches(regex)
                            //&&startHour.matches(regex)&&startMinute.matches(regex)){
                        //if(Integer.parseInt(stopMinute)>=0&&Integer.parseInt(stopMinute)<60&&
                                //Integer.parseInt(startMinute)>=0&&Integer.parseInt(startMinute)<60&&
                                /*Integer.parseInt(assembler.quizInfo.getFinishHour().getText())>=0&&*///Integer.parseInt(startHour)<24&&
                                //Integer.parseInt(startHour)<24){
                            if(assembler.quizInfo.getStartHour().getText()!=null&&!assembler.quizInfo.getStartHour().getText().equals("")&&
                                    startHour.matches(regex)&&Integer.parseInt(startHour)<24&&
                                    Integer.parseInt(startHour)>=0)
                            quiz.setStartHour(Integer.parseInt(assembler.quizInfo.getStartHour().getText()));
                            else if(startHour==null||startHour.equals(""))quiz.setStartHour(0);
                            if(assembler.quizInfo.getStartMinute().getText()!=null&&!assembler.quizInfo.getStartMinute().getText().equals("")&&
                                    startMinute.matches(regex)&&Integer.parseInt(startMinute)<60&&
                                    Integer.parseInt(startMinute)>=0)
                            quiz.setStartMinute(Integer.parseInt(assembler.quizInfo.getStartMinute().getText()));
                            else if(startMinute==null||startMinute.equals(""))quiz.setStartMinute(0);
                            if(stopHour!=null&&!stopHour.equals("")&&
                                    stopHour.matches(regex)&&Integer.parseInt(stopHour)>=0)
                            quiz.setStopHour(Integer.parseInt(assembler.quizInfo.getFinishHour().getText()));
                            else if(stopHour==null||stopHour.equals(""))quiz.setStopHour(0);
                            if(stopMinute!=null&&!stopMinute.equals("")&&
                                    stopMinute.matches(regex)&&Integer.parseInt(stopMinute)>=0&&Integer.parseInt(stopMinute)<60)
                            quiz.setStopMinute(Integer.parseInt(assembler.quizInfo.getFinishMinute().getText()));
                            else if(stopMinute==null||stopMinute.equals(""))quiz.setStopMinute(0);
                System.out.println(quiz.getStopMinute());
                quiz.setReview(assembler.quizInfo.getReview().isSelected());
                System.out.println(quiz.isReview());
                quiz.setQuestionsShowedAtOnce(!assembler.quizInfo.getOneByOneQuestions().isSelected());
                System.out.println(assembler.quizInfo.getOneByOneQuestions().isSelected());
                quiz.setShuffle(assembler.quizInfo.getShuffle().isSelected());
                if(year!=null&&!year.equals("")&&Integer.parseInt(year)>0)quiz.setYear(Integer.parseInt(year));
                else if(year==null||year.equals(""))quiz.setYear(0);
                if(month!=null&&!month.equals("")&&Integer.parseInt(month)>0)quiz.setMonth(Integer.parseInt(month));
                else if(month==null||month.equals(""))quiz.setMonth(0);
                if(day!=null&&!day.equals("")&&Integer.parseInt(day)>0)quiz.setDay(Integer.parseInt(day));
                else if(day==null||day.equals(""))quiz.setDay(0);

                            for(int i=0;i<quiz.getStudents().size();i++){

                                for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                                    if(quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())){
                                        quiz.getStudents().get(i).getQuizzes().get(j).setBanned(assembler.quizInfo.getCheckBoxes().get(i).isSelected());
                                    }
                                }

                            }

                        /*else{
                            AlertBox alertBox=new AlertBox();
                            alertBox.display("خطا","فیلدها به درستی پر نشده اند",700,400);
                        }*/

                    /*else{
                        AlertBox alertBox=new AlertBox();
                        alertBox.display("خطا","فیلدها به درستی پر نشده اند",700,400);
                    }*/


                /*else{
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("خطا","فیلدها به درستی پر نشده اند",700,400);
                }*/

            }
        });
        assembler.correction.getIsGraded().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int sum=0;
                for(int i=0;i<assembler.correction.getQuiz().getQuestions().size();i++){
                    if(assembler.correction.getQuiz().getAnswers().size()>i){
                        sum+=assembler.correction.getQuiz().getAnswers().get(i).getGrade();
                    }
                }
                assembler.correction.getQuiz().setGrade(sum);
                AlertBox alertBox=new AlertBox();
                alertBox.display("","نمره نهایی درنظر گرفته شد",720,400);
                assembler.correction.getQuiz().setGraded(true);
                setAverageAndRank(assembler.correction.getQuiz());
                information.writeQuizForStudents(assembler.correction.getStudent(),assembler.correction.getQuiz());
            }

        });
        assembler.takeTest.getNext().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                timer.saveMethod(assembler.takeTest.getCurrentQuestion());
                if (assembler.takeTest.getCurrentQuestion()+1 >= quiz.getQuestions().size()) {
                    assembler.takeTest.setCurrentQuestion(0);
                    assembler.takeTest.displayQuestion();
                } else {
                    assembler.takeTest.setCurrentQuestion(assembler.takeTest.getCurrentQuestion()+1);
                    assembler.takeTest.displayQuestion();
                }
            }
        });
        assembler.takeTest.getPrevious().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                timer.saveMethod(assembler.takeTest.getCurrentQuestion());
                if (assembler.takeTest.getCurrentQuestion()-1< 0) {
                    assembler.takeTest.setCurrentQuestion(quiz.getQuestions().size() - 1);
                    assembler.takeTest.displayQuestion();
                } else {
                    assembler.takeTest.setCurrentQuestion(assembler.takeTest.getCurrentQuestion()-1);
                    assembler.takeTest.displayQuestion();
                }
            }
        });
        /*assembler.poll.getToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if(assembler.poll.getToggleGroup().getSelectedToggle() == assembler.poll.getVeryHardButton()
                        && quiz.getHardshipLevel() == 0) {
                    quiz.setHardshipLevel(5);
                } else if(assembler.poll.getToggleGroup().getSelectedToggle() == assembler.poll.getHardButton()
                        && quiz.getHardshipLevel() == 0){
                    quiz.setHardshipLevel(4);
                } else if(assembler.poll.getToggleGroup().getSelectedToggle() == assembler.poll.getAverageButton()
                        && quiz.getHardshipLevel() == 0){
                    quiz.setHardshipLevel(3);
                } else if(assembler.poll.getToggleGroup().getSelectedToggle() == assembler.poll.getEasyButton()
                        && quiz.getHardshipLevel() == 0){
                    quiz.setHardshipLevel(2);
                } else if(assembler.poll.getToggleGroup().getSelectedToggle() == assembler.poll.getVeryEasyButton()
                        && quiz.getHardshipLevel() == 0){
                    quiz.setHardshipLevel(1);
                } else {
                    assembler.alreadyVoted();
                }
            }
        });*/
        /*assembler.poll.getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(quiz.getHardshipLevel() != 0){
                    assembler.poll.addParticipant();
                }
            }
        });*/
        assembler.prepareForTest.getParticipateButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                    Quiz tempQuiz = assembler.prepareForTest.getQuiz();
                for (int i = 0; i < quiz.getQuestions().size(); i++) {
                    if(quiz.getOrder().size()<=i)
                    quiz.getOrder().add(i, i);
                }

                    Student student = (Student) person;
                    int currentQuestion = 0;
                    assembler.takeTest.setQuiz(quiz);
                    timer = new Timer(quiz,assembler.takeTest, student,assembler);
                    timer.setQuiz(quiz);
                    int year = timer.getCalendar().get(Calendar.YEAR);
                    int month = timer.getCalendar().get(Calendar.MONTH)+1;
                    System.out.println(month);
                    int day = timer.getCalendar().get(Calendar.DAY_OF_MONTH);
                    int minute = timer.getCalendar().get(Calendar.MINUTE);
                    int hour = timer.getCalendar().get(Calendar.HOUR_OF_DAY);
                    System.out.println(hour);
                    if(quiz.getQuestionsShowedAtOnce()){
                        assembler.takeTest.getNext().setDisable(false);
                        assembler.takeTest.getPrevious().setDisable(false);
                    if (tempQuiz.getYear() == year && tempQuiz.getMonth() == month /*&& tempQuiz.getDay() <= day&&day+
                            (tempQuiz.getStartHour()+tempQuiz.getStopHour()+(tempQuiz.getStartMinute()+tempQuiz.getStopMinute())/60)%24>=day*/) {
                        System.out.println("1");
                        System.out.println(tempQuiz.getStartHour());
                        System.out.println(tempQuiz.getStartMinute());
                        System.out.println(tempQuiz.getStopHour());
                        System.out.println(tempQuiz.getStopMinute());
                        System.out.println((tempQuiz.getStartMinute()
                                + tempQuiz.getStopMinute()) / 60);
                        if (tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + tempQuiz.getStartMinute() * 60 + tempQuiz.getStopHour() * 3600 +
                                tempQuiz.getStopMinute() * 60 > day * 24 * 3600 + hour * 3600 + minute * 60) {
                            if (tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + (tempQuiz.getStartMinute()-1) * 60 < day * 24 * 3600 +
                                    hour * 3600 + minute * 60) {
                                System.out.println("stablished");
                                if (!quiz.getIsBanned()) {
                                    System.out.println("shuffle"+quiz.isShuffle());
                                    if (quiz.isShuffle()) quiz.shuffle();
                                    assembler.mergeTakeTest();

                                    assembler.takeTest.displayQuestion();
                                    timer.start();
                                    //timer.join();
                                    System.out.println("stablished");
                                } else {
                                    AlertBox alertBox = new AlertBox();
                                    alertBox.display("خطا", "شما از این آزمون محروم هستید", 720, 400);
                                }
                            }
                        }
                    }
                       /* if (tempQuiz.getStartHour() <= hour) {
                            if (tempQuiz.getStartHour() + tempQuiz.getStopHour() + (int) (tempQuiz.getStartMinute()
                                    + tempQuiz.getStopMinute()) / 60 > hour
                            ) {
                                System.out.println("stablished");
                                assembler.mergeTakeTest();
                                timer.start();
                                timer.join();

                            } else if (tempQuiz.getStartHour() + tempQuiz.getStopHour() + (tempQuiz.getStartMinute()
                                    + tempQuiz.getStopMinute()) / 60 == hour) {
                                int startMinute=0;
                                if((tempQuiz.getStartMinute() + tempQuiz.getStopMinute()) / 60==0)startMinute=tempQuiz.getStartMinute();
                                if (startMinute<=minute&&(tempQuiz.getStartMinute() + tempQuiz.getStopMinute()) % 60 >= minute) {
                                    timer.start();
                                    assembler.mergeTakeTest();
                                    System.out.println("stablished");
                                    timer.join();
                                    System.out.println("stablished");
                                }

                            }*/



                        /*if (tempQuiz.getQuestions().get(tempQuiz.getOrder().get(currentQuestion)).getType() == 1) {
                            tempQuiz.getAnswers().get(tempQuiz.getOrder().get(currentQuestion))
                                    .setAnswer(assembler.takeTest.getDescriptiveAnswer().getText());
                        } else if (tempQuiz.getQuestions().get(tempQuiz.getOrder().get(currentQuestion)).getType() == 2) {
                            assembler.takeTest.getMultipleOptionToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                                @Override
                                public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                                    for (int i = 0; i < assembler.takeTest.getMultipleOptionRadioButtons().size(); i++) {
                                        if (assembler.takeTest.getMultipleOptionToggleGroup().getSelectedToggle() ==
                                                assembler.takeTest.getMultipleOptionRadioButtons().get(i)) {
                                            for (int j = 0; j < student.getQuizzes().size(); j++) {
                                                if (student.getQuizzes().get(j).equals(tempQuiz.getName())) {
                                                    student.getQuizzes().get(j).getAnswers().get(tempQuiz.getOrder().get(currentQuestion))
                                                            .setAnswer(assembler.takeTest.getMultipleOptionRadioButtons().get(i).getText());
                                                }
                                            }
                                        }
                                    }

                                }
                            });
                        } else {
                            for (int i = 0; i < assembler.takeTest.getTrueFalseToggleGroup().size(); i++) {
                                final int temp = i;
                                assembler.takeTest.getTrueFalseToggleGroup().get(i).selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                                    @Override
                                    public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                                        for (int j = 0; j < assembler.takeTest.getTrueFalseRadioButtons().size(); j++) {
                                            if (assembler.takeTest.getTrueFalseToggleGroup().get(temp).getSelectedToggle() ==
                                                    assembler.takeTest.getTrueFalseRadioButtons().get(j)) {
                                                if (j % 2 == 0) {
                                                    student.getQuizzes().get(j).getAnswers().get(quiz.getOrder()
                                                            .get(currentQuestion)).setYesNoAnswer(j / 2, "true");
                                                } else {
                                                    student.getQuizzes().get(j).getAnswers().get(quiz.getOrder()
                                                            .get(currentQuestion)).setYesNoAnswer(j / 2, "false");
                                                }
                                            }
                                        }

                                    }
                                });
                            }
                        }*/


                        }
                    else{
                        assembler.takeTest.getNext().setDisable(true);
                        assembler.takeTest.getPrevious().setDisable(true);
                        int mins=0;
                        int hours=0;
                        for(int i=0;i<quiz.getQuestions().size();i++){
                            mins+=quiz.getQuestions().get(i).getMinuet();
                            hours+=quiz.getQuestions().get(i).getHour();

                        }
                        hours=hours+mins/60;
                        mins=mins%60;
                        if (tempQuiz.getYear() == year && tempQuiz.getMonth() == month /*&& tempQuiz.getDay() <= day&&day+
                            (tempQuiz.getStartHour()+tempQuiz.getStopHour()+(tempQuiz.getStartMinute()+tempQuiz.getStopMinute())/60)%24>=day*/) {
                            System.out.println("1");
                            System.out.println(tempQuiz.getStartHour());
                            System.out.println(tempQuiz.getStartMinute());
                            System.out.println(tempQuiz.getStopHour());
                            System.out.println(tempQuiz.getStopMinute());
                            System.out.println((tempQuiz.getStartMinute()
                                    + tempQuiz.getStopMinute()) / 60);
                            if (tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + tempQuiz.getStartMinute() * 60 + hours * 3600 +
                                    mins * 60 > day * 24 * 3600 + hour * 3600 + minute * 60) {
                                if (tempQuiz.getDay() * 24 * 3600 + tempQuiz.getStartHour() * 3600 + (tempQuiz.getStartMinute()-1) * 60 < day * 24 * 3600 +
                                        hour * 3600 + minute * 60) {
                                    System.out.println("stablished");
                                    if (!quiz.getIsBanned()) {

                                        if (quiz.isShuffle()) quiz.shuffle();
                                        assembler.mergeTakeTest();

                                        assembler.takeTest.displayQuestion();
                                        timer.start();
                                        //timer.join();
                                        System.out.println("stablished");
                                    } else {
                                        AlertBox alertBox = new AlertBox();
                                        alertBox.display("خطا", "شما از این آزمون محروم هستید", 720, 400);
                                    }
                                }
                            }
                        }

                    }



            }
        });
        assembler.takeTest.getDone().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                timer.saveMethod(assembler.takeTest.getCurrentQuestion());
                AlertBox alertBox=new AlertBox();
                alertBox.display("","آزمون ذخیره شد",720,400);
                assembler.showStudentPanel();
                timer.setCurrentQuestion(-1);
                timer.setMillsLeft(0);
                for(int i=0;i<assembler.takeTest.getQuiz().getQuestions().size();i++){
                    Student student=(Student)person;
                    information.writeAnswerOfQuiz(assembler.takeTest.getQuiz().getAnswers().get(i),i,student,assembler.takeTest.getQuiz());
                }
            }
        });
        assembler.poll.getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (quiz != null) {
                    if (quiz.getHardshipLevel() == 0) {
                        int isselected = 0;
                        if (assembler.poll.getVeryEasyButton().isSelected()) {
                            quiz.setHardshipLevel(1);
                            assembler.poll.addParticipant();
                            isselected = 1;
                        }
                        if (assembler.poll.getEasyButton().isSelected()) {
                            quiz.setHardshipLevel(2);
                            assembler.poll.addParticipant();
                            isselected = 1;
                        }
                        if (assembler.poll.getAverageButton().isSelected()) {
                            quiz.setHardshipLevel(3);
                            assembler.poll.addParticipant();
                            isselected = 1;
                        }
                        if (assembler.poll.getHardButton().isSelected()) {
                            quiz.setHardshipLevel(4);
                            assembler.poll.addParticipant();
                            isselected = 1;
                        }
                        if (assembler.poll.getVeryHardButton().isSelected()) {
                            quiz.setHardshipLevel(5);
                            assembler.poll.addParticipant();
                            isselected = 1;
                        }
                        if (isselected == 1) {
                            AlertBox alertBox = new AlertBox();
                            alertBox.display("", "رای شما با موفقیت ثبت شد", 720, 400);
                        } else {
                            AlertBox alertBox = new AlertBox();
                            alertBox.display("خطا", "گزینه ای انتخاب نشده است", 720, 400);
                        }
                    }
                    else{
                        AlertBox alertBox = new AlertBox();
                        alertBox.display("خطا", "رای شما قبلا ثبت شده است", 720, 400);
                    }
                }
            }
        });
        assembler.correction.getSave().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if(assembler.correction.getGrade().getText()!=null&&!assembler.correction.getGrade().getText().equals("")) {
                        System.out.println("hi");
                        System.out.println(assembler.correction.getGrade().getText());

                        assembler.correction.getQuiz().getAnswers().get(assembler.correction.getCurrentQuestion()).setGrade(Double.parseDouble(assembler.correction.getGrade().getText()));

                    }
                    else{
                    assembler.correction.getQuiz().getAnswers().get(assembler.correction.getCurrentQuestion()).setGrade((double) 0);}
                    information.writeAnswerOfQuiz(assembler.correction.getQuiz().getAnswers().get(assembler.correction.getCurrentQuestion()), assembler.correction.getCurrentQuestion(), assembler.correction.getStudent(), assembler.correction.getQuiz());
                    AlertBox alertBox = new AlertBox();
                    alertBox.display("", "نمره ثبت شد", 720, 400);
                }
                catch (NumberFormatException e){
                    System.out.println("realy");

                }
            }
        });



    }
    //only for manager
    public void mouseClicked(MouseEvent event) {
        TreeItem item=null;
        Manager manager = (Manager) person;
        Node node = event.getPickResult().getIntersectedNode();
        // Accept clicks only on node cells, and not on empty spaces of the TreeView
        if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
            item = (TreeItem) assembler.managerPanel.getTreeView().getSelectionModel().getSelectedItem();
            if (assembler.managerPanel.getTestCreated() == false && (item == assembler.managerPanel.getCreateTest()
                    || item == assembler.managerPanel.getAddStudents() || item == assembler.managerPanel.getAddStudentsManually()
                    || item == assembler.managerPanel.getAddStudentsFromExcel() || item == assembler.managerPanel.getAddQuestions()
                    || item == assembler.managerPanel.getAddInformation())&&(quiz==null||quiz.getName()==null)) {
                assembler.showCreateTest();
                assembler.createTest.getOk().setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        assembler.managerPanel.setTestCreated(true);
                        quiz = new Quiz();
                        quiz.setName(assembler.createTest.getTextField().getText()+"#"+manager.getAccountNumber());
                        assembler.createTest.getStage().close();
                        assembler.getQuestionMaking().setQuiz(quiz);
                        assembler.quizInfo.setQuiz(quiz);
                    }
                });
                assembler.createTest.getBack().setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        assembler.showManagerPanel();
                        assembler.createTest.getStage().close();
                    }
                });
            }
            if (item == assembler.managerPanel.getAddStudentsManually()) {
                boolean temp = true;
                for(Quiz q : manager.getQuizes()){
                    if(quiz == q){
                        temp = false;
                        assembler.saveChanges();
                    }
                }
                if(temp) assembler.mergeAddManually();
            } else if (item == assembler.managerPanel.getAddQuestions()) {
                boolean temp = true;
                for(Quiz q : manager.getQuizes()){
                    if(quiz == q){
                        temp = false;
                        assembler.saveChanges();
                    }
                }
                if(temp){
                    assembler.getQuestionMaking().setQuiz(quiz);
                    assembler.mergeAddQuestions();
                }
            } else if (item == assembler.managerPanel.getAddInformation()) {
                boolean temp = true;
                for (Quiz q : manager.getQuizes()) {
                    if (quiz == q) {
                        temp = false;
                        assembler.saveChanges();
                    }
                }
                if (temp) {
                    assembler.mergeAddInfo();
                    assembler.quizInfo.setQuiz(quiz);
                }
            } else if (item == assembler.managerPanel.getAddStudentsFromExcel()) {
                boolean temp = true;
                for(Quiz q : manager.getQuizes()){
                    if(quiz == q){
                        temp = false;
                        assembler.saveChanges();
                    }
                }
                if(temp) {
                    assembler.mergeAddFromExcel();
                    if (quiz != null) {
                        ArrayList<Student> students = new ArrayList<>();
                        students = assembler.addFromExcel.showFileChooser(quiz);
                        if (students != null) {
                            for (int i = 0; i < students.size(); i++) quiz.getStudents().add(students.get(i));
                        }
                        assembler.addFromExcel.setBorderPane(students);
                        actionForAddFromExcelCheckBoxes();
                    }
                }
            } else if (item == assembler.managerPanel.getExit()) {
                assembler.showExitAlert();
                assembler.exitAlert.getExit().setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        assembler.exitAlert.getStage().close();
                        assembler.getStage().close();
                    }
                });
            } else {
                boolean check = false;
                for(int j=0;j<manager.getQuizes().size();j++) {
                    if(manager.getQuizes().get(j) == quiz) check = true;
                }
                if(check || quiz == null || quiz.getName()==null){
                    for (int i = 0; i < assembler.managerPanel.getTreeItems().size(); i++) {
                        if (item == assembler.managerPanel.getTreeItems().get(i)){
                            if(item.getParent() == assembler.managerPanel.getGradeTests()){
                                for(int j=0;j<manager.getQuizes().size();j++){
                                    String[]quizSplit=manager.getQuizes().get(j).getName().split("#");
                                    if(quizSplit[0].equals(item.getValue().toString())){
                                        quiz = manager.getQuizes().get(j);
                                    }
                                }
                                assembler.mergeCorrection();
                                for(int j=0;j<quiz.getStudents().get(0).getQuizzes().size();j++){
                                    if(quiz.getStudents().get(0).getQuizzes().get(j).getName().equals(quiz.getName())){
                                        assembler.correction.setQuiz(quiz.getStudents().get(0).getQuizzes().get(j));

                                        assembler.correction.fixCorrection();
                                        assembler.correction.setStudents(quiz.getStudents());
                                        assembler.correction.setStudent(quiz.getStudents().get(0));
                                        actionForCorrectButtons();
                                        break;
                                    }
                                }
                            } else if(item.getParent() == assembler.managerPanel.getChats()){
                                System.out.println("chat 1");
                                for(int j=0;j<manager.getQuizes().size();j++){
                                    System.out.println("chat 2");
                                    String[]quizSplit=manager.getQuizes().get(j).getName().split("#");
                                    if(quizSplit[0].equals(item.getValue().toString())){
                                        System.out.println("chat 3");
                                        quiz = manager.getQuizes().get(j);
                                        final int temp = j;
                                        assembler.chats.get(temp).getSend().setOnAction(new EventHandler<ActionEvent>() {
                                            @Override
                                            public void handle(ActionEvent event) {
                                                try {
                                                    client.setMessage(quiz.getName()+"#"+manager.getAccountNumber()
                                                            +"#"+assembler.chats.get(temp).getTxtInput().getText().trim());
                                                    if (client.getMessage().length() == 0) {
                                                        return;
                                                    }
                                                    client.getOutput().writeUTF("" + client.getMessage());
                                                    client.getOutput().flush();

                                                    assembler.chats.get(temp).clearTxtInput();
                                                } catch (IOException ex) {
                                                    System.err.println(ex);
                                                }
                                            }
                                        });
                                    }
                                }
                                assembler.mergeChatForManager(quiz.getName());
                            } else if(item.getParent().getParent() == assembler.managerPanel.getManageTests()){
                                for(int j=0;j<manager.getQuizes().size();j++){
                                    String[]quizSplit=manager.getQuizes().get(j).getName().split("#");
                                    System.out.println(quizSplit[0]);
                                    System.out.println(item.getParent().getValue().toString());
                                    if(quizSplit[0].equals(item.getParent().getValue().toString())){
                                        quiz = manager.getQuizes().get(j);
                                    }
                                }
                                if(item.getValue().toString().equals("تغییر شرکت کنندگان")){
                                    assembler.manageStudentsParticipation.setLeftAnchorNull();
                                    assembler.mergeManageStudentsParticipation();
                                } else if (item.getValue().toString().equals("تغییر اطلاعات")){
                                    assembler.manageInformation.setNull();
                                    assembler.manageInformation.setQuiz(quiz);
                                    assembler.mergeManageInformation();
                                } else if (item.getValue().toString().equals("تغییر سوالات")){
                                    assembler.mergeAddQuestions();
                                    assembler.getQuestionMaking().setQuiz(quiz);
                                    assembler.getQuestionMaking().setCurrentQuestion(0);
                                    assembler.getQuestionMaking().fixQuestionMaking();
                                }
                            } else if(item.getParent().getParent() == assembler.managerPanel.getTestHistory()){
                                for(int j=0;j<manager.getQuizes().size();j++){
                                    String[]quizSplit=manager.getQuizes().get(j).getName().split("#");
                                    if(quizSplit[0].equals(item.getParent().getValue().toString())){
                                        quiz = manager.getQuizes().get(j);
                                    }
                                }
                                assembler.quizHistory.setQuiz(quiz);
                                assembler.mergeQuizHistory();
                            } else if(item.getParent().getParent().getParent() == assembler.managerPanel.getTestHistory()){
                                for(int j=0;j<manager.getQuizes().size();j++){
                                    String[]quizSplit=manager.getQuizes().get(j).getName().split("#");
                                    if(quizSplit[0].equals(item.getParent().getParent().getValue().toString())){
                                        quiz = manager.getQuizes().get(j);
                                    }
                                }
                                if(item.getParent().getValue().toString().equals("نمودار آزمون ها")){
                                    if(item.getValue().toString().equals("فرد به فرد")){
                                        assembler.charts.personByPerson(quiz);
                                    } else if(item.getValue().toString().equals("آزمون به آزمون")){
                                        assembler.showChart();
                                        assembler.charts.quizByQuiz((Manager) person);
                                    }
                                } else if(item.getParent().getValue().toString().equals("نمایش نتیجه در اکسل")){
                                    if(item.getValue().toString().equals("فرد به فرد")){
                                        DirectoryChooser directoryChooser=new DirectoryChooser();
                                        File file=directoryChooser.showDialog(assembler.getStage());
                                        if(file!=null&&quiz!=null){
                                            information.personByPersonToExcel(file.getAbsolutePath(),quiz);
                                        }
                                    } else if(item.getValue().toString().equals("آزمون به آزمون")){
                                        DirectoryChooser directoryChooser=new DirectoryChooser();
                                        File file=directoryChooser.showDialog(assembler.getStage());
                                        if(file!=null&&quiz!=null){
                                            information.quizByQuizToExcel(file.getAbsolutePath(),manager);
                                        }

                                    }
                                }

                            }

                        }
                    }
                } else assembler.saveChanges();
            }
        }
    }
    //only for student
    public void treeItemClicked(MouseEvent event) {
        TreeItem item;
        Student student = (Student) person;
        Node node = event.getPickResult().getIntersectedNode();
        // Accept clicks only on node cells, and not on empty spaces of the TreeView
        if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
            item = (TreeItem) assembler.studentPanel.getTreeView().getSelectionModel().getSelectedItem();
            System.out.println(item.getValue().toString());
            for (int i = 0; i < assembler.studentPanel.getItems().size(); i++) {
                if (item == assembler.studentPanel.getItems().get(i)){
                    if(item.getParent() == assembler.studentPanel.getToBeHeldTests()){
                        for(int j=0;j<student.getQuizzes().size();j++){
                            String[]quizSplit=student.getQuizzes().get(j).getName().split("#");
                            if(quizSplit[0].equals(item.getValue().toString())){
                                quiz = student.getQuizzes().get(j);
                                System.out.println(quiz==null);
                                assembler.prepareForTest.setQuiz(quiz);
                                assembler.mergePrepareForTest();
                            }

                        }
                    } else if(item.getParent() == assembler.studentPanel.getChats()){
                        for(int j=0;j<student.getQuizzes().size();j++){
                            String[]quizSplit=student.getQuizzes().get(j).getName().split("#");
                            if(item.getValue().toString().equals(quizSplit[0]+"."+quizSplit[1])){
                                quiz = student.getQuizzes().get(j);
                                    final int temp = j;
                                    assembler.chats.get(j).getSend().setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                client.setMessage(quiz.getName()+"#"+student.getAccountNumber()
                                                        +"#"+assembler.chats.get(temp).getTxtInput().getText().trim());
                                                if (client.getMessage().length() == 0) {
                                                    return;
                                                }
                                                client.getOutput().writeUTF("" + client.getMessage());
                                                client.getOutput().flush();

                                                assembler.chats.get(temp).clearTxtInput();
                                            } catch (IOException ex) {
                                                System.err.println(ex);
                                            }
                                        }
                                    });
                                }
                            }
                             assembler.mergeChatForStudent(quiz.getName());
                        }
                    } else if(item.getParent().getParent() == assembler.studentPanel.getHeldTests()){
                        for(int j=0;j<student.getQuizzes().size();j++){
                            String[]quizSplit=student.getQuizzes().get(j).getName().split("#");
                            if(quizSplit[0].equals(item.getParent().getValue().toString())){
                                quiz = student.getQuizzes().get(j);
                            }
                        }
                        if(item.getValue().toString().equals("مرور")){
                            assembler.review.setQuiz(quiz);
                            assembler.review.setStudent(student);
                            assembler.review.fixCorrection();
                            assembler.mergeReview();
                        } else if(item.getValue().toString().equals("اطلاعات کلی")){
                            assembler.results.setQuiz(quiz);
                            assembler.mergeResults();
                        } else if(item.getValue().toString().equals("نظرسنجی")){
                            assembler.poll.setQuiz(quiz);
                            assembler.mergePoll();
                            for(int j=0;j<student.getQuizzes().size();j++){
                                if(student.getQuizzes().get(j).getName().equals(item.getParent().getValue().toString())){
                                    quiz = student.getQuizzes().get(j);
                                }
                            }
                        }
                    }

                }
            }
        System.out.println(quiz==null);
    }


    public void setEventHandlerOnTreeView(){
        if(person instanceof Manager){
            EventHandler<MouseEvent> mouseEventHandle = (MouseEvent event) -> {
                mouseClicked(event);
            };
            assembler.managerPanel.getTreeView().addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventHandle);
        } else {
            EventHandler<MouseEvent> mouseEventHandle = (MouseEvent event) -> {
                treeItemClicked(event);
            };
            assembler.studentPanel.getTreeView().addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventHandle);
        }
    }

    public void addFromExcel(){
        FileChooser fileChooser=new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".xlsx","*.xlsx"));
        File file=fileChooser.showOpenDialog(null);
        information.readFromExcel(file.getPath(),quiz);
    }

    public Assembler getAssembler() {
        return assembler;
    }
    public void actionForCorrectButtons(){
        ArrayList<Button>buttons=assembler.correction.getStudentsButtons();
        for(int i=0;i<buttons.size();i++){
            final int index=i;
            buttons.get(i).setOnDragEntered(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    Button current=(Button) event.getSource();
                    current.setStyle("-fx-background-color: #EDBB99");
                }
            });
            buttons.get(i).setOnDragExited(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    Button current=(Button) event.getSource();
                    current.setStyle("-fx-background-color: #F9E79F");
                }
            });
            buttons.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    assembler.correction.setStudent(assembler.correction.getStudentsArray().get(index));
                    for(int i=0;i<assembler.correction.getStudent().getQuizzes().size();i++){
                        if(assembler.correction.getStudent().getQuizzes().get(i).getName().equals(quiz.getName()))
                            assembler.correction.setQuiz(assembler.correction.getStudent().getQuizzes().get(i));
                        assembler.correction.setCurrentQuestion(0);
                        assembler.correction.fixCorrection();
                    }
                }
            });
        }
    }
    public void actionForAddManuallyCheckBoxes(){
        ArrayList<CheckBox>checkBoxes=assembler.getAddManually().getCheckBoxes();
        for(int i=0;i<checkBoxes.size();i++){
            final int index=i;
            checkBoxes.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(!checkBoxes.get(index).isSelected()){
                        checkBoxes.get(index).setSelected(true);
                        assembler.removeStudents.display(100,100);
                        assembler.removeStudents.getRemoveButton().setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                checkBoxes.get(index).setSelected(false);
                                checkBoxes.get(index).setDisable(true);
                                assembler.removeStudents.getWindow().close();

                                for(int i=0;i<quiz.getStudents().size();i++){
                                    if(quiz.getStudents().get(i).getStudentId()==assembler.getAddManually().getStudents().get(index).getStudentId()) {
                                        quiz.getStudents().remove(i);
                                        break;
                                    }
                                }

                            }
                        });
                    }
                }
            });
        }
    }
    public void actionForAddFromExcelCheckBoxes(){
        ArrayList<CheckBox>checkBoxes=assembler.addFromExcel.getCheckBoxes();
        for(int i=0;i<checkBoxes.size();i++){
            final int index=i;
            checkBoxes.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(!checkBoxes.get(index).isSelected()){
                        checkBoxes.get(index).setSelected(true);
                        assembler.removeStudents.display(700,450);
                        assembler.removeStudents.getRemoveButton().setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                checkBoxes.get(index).setSelected(false);
                                checkBoxes.get(index).setDisable(true);
                                assembler.removeStudents.getWindow().close();

                                for(int i=0;i<quiz.getStudents().size();i++){
                                    if(quiz.getStudents().get(i).getStudentId()==assembler.addFromExcel.getStudents().get(index).getStudentId()) {
                                        quiz.getStudents().remove(i);
                                        break;
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }

    }
    public void actionForQuizInfoCheckBoxes(){
        ArrayList<CheckBox>checkBoxes=assembler.quizInfo.getCheckBoxes();
        for(int i=0;i<checkBoxes.size();i++){
            final int index=i;
            checkBoxes.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(checkBoxes.get(index).isSelected()){
                        for(int i=0;i<assembler.quizInfo.getStudents().get(index).getQuizzes().size();i++){
                            if(assembler.quizInfo.getStudents().get(index).getQuizzes().get(i).getName().equals(quiz.getName()))
                                assembler.quizInfo.getStudents().get(index).getQuizzes().get(i).setBanned(true);

                        }
                    }
                    if(!checkBoxes.get(index).isSelected()){
                        for(int i=0;i<assembler.quizInfo.getStudents().get(index).getQuizzes().size();i++){
                            if(assembler.quizInfo.getStudents().get(index).getQuizzes().get(i).getName().equals(quiz.getName()))
                                assembler.quizInfo.getStudents().get(index).getQuizzes().get(i).setBanned(false);

                        }
                    }
                }
            });
        }
    }
    public void saveMethod(){
        if (quiz != null && quiz.getName() != null) {
            System.out.println("size"+quiz.getStudents().size());
            System.out.println("phase one");
            if (!quiz.getQuestionsShowedAtOnce()) {
                int sw = 0;
                for (int i = 0; i < quiz.getQuestions().size(); i++) {
                    if (quiz.getQuestions().get(i).getHour() == 0 && quiz.getQuestions().get(i).getMinuet() == 0)
                        sw = 1;
                }
                if (sw == 0) {

                    Manager manager = (Manager) person;
                    int sw1=0;
                    for(int i=0;i<manager.getQuizes().size();i++){
                        if(manager.getQuizes().get(i).getName().equals(quiz.getName())) {
                            manager.getQuizes().set(i, quiz);
                            sw1 = 1;
                        }
                    }
                    if(sw1==0){
                        String[]quizSplit=quiz.getName().split("#");
                        assembler.managerPanel.addItemToManageAndChat(quizSplit[0]);
                        manager.getQuizes().add(quiz);
                        System.out.println("added");}

                    information.writeQuizForManager(manager, quiz);
                    for (int i = 0; i < quiz.getStudents().size(); i++) {
                        for (int j = 0; j < quiz.getStudents().get(i).getQuizzes().size(); j++) {
                            if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {

                                //information.writeQuizForStudents(quiz.getStudents().get(i), quiz.getStudents().get(i).getQuizzes().get(j));

                            }
                        }
                    }
                    assembler.getQuestionMaking().setCurrentQuestion(0);
                    assembler.getQuestionMaking().newPage();
                    assembler.getAddManually().setAddManuallyNull();
                    assembler.manageInformation.setNull();
                    assembler.quizInfo.setNull();
                    for(int i=0;i<quiz.getStudents().size();i++){
                        for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                            if(quiz.getName().equals(quiz.getStudents().get(i).getQuizzes().get(j).getName())){
                                Quiz quiz1=quiz.getStudents().get(i).getQuizzes().get(j);
                                quiz1.setName(quiz.getName());
                                quiz1.setStopMinute(quiz.getStopMinute());
                                quiz1.setStopHour(quiz.getStopHour());
                                quiz1.setStartMinute(quiz.getStartMinute());
                                quiz1.setStartHour(quiz.getStartHour());
                                quiz1.setDay(quiz.getDay());
                                quiz1.setYear(quiz.getYear());
                                quiz1.setMonth(quiz.getMonth());
                                quiz1.setQuestions(quiz.getQuestions());
                                quiz1.setShuffle(quiz.isShuffle());
                                quiz1.setReview(quiz.isReview());
                                quiz1.setQuestionsShowedAtOnce(quiz.getQuestionsShowedAtOnce());
                                quiz.getStudents().get(i).getQuizzes().set(j,quiz1);
                                information.writeNewStudent(quiz.getStudents().get(i));
                            }
                        }
                    }

                    //the next line is only here for debugging process , remember to delete
                    //assembler.managerPanel.addItemsToHistoryAndCorrection(quizSplit[0]);
                    assembler.managerPanel.setTestCreated(false);
                    quiz=null;
                    information.writeNewManager(manager);
                    AlertBox alertBox=new AlertBox();
                    alertBox.display("","ذخیره سوال با موفقیت انجام شد",700,420);
                } else {
                    AlertBox alertBox = new AlertBox();
                    alertBox.display("خطا","زمان سوالات تعیین نشده", 720, 400);
                }
            } else {
                System.out.println("phase two");
                Manager manager = (Manager) person;
                int sw1=0;
                for(int i=0;i<manager.getQuizes().size();i++){
                    if(manager.getQuizes().get(i).getName().equals(quiz.getName())){sw1=1;
                    manager.getQuizes().set(i,quiz);}
                }
                if(sw1==0){
                    String[]quizSplit=quiz.getName().split("#");
                    assembler.managerPanel.addItemToManageAndChat(quizSplit[0]);
                    assembler.mergeChatForManager(quizSplit[0]);
                    manager.getQuizes().add(quiz);}

                information.writeQuizForManager(manager, quiz);
                for (int i = 0; i < quiz.getStudents().size(); i++) {
                    for (int j = 0; j < quiz.getStudents().get(i).getQuizzes().size(); j++) {
                        if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {
                            System.out.println("phase3");
                            //information.writeQuizForStudents(quiz.getStudents().get(i), quiz.getStudents().get(i).getQuizzes().get(j));


                        }
                    }


                }

                assembler.getQuestionMaking().setCurrentQuestion(0);
                assembler.getQuestionMaking().newPage();
                assembler.getAddManually().setAddManuallyNull();
                assembler.getAddManually().setAddManuallyNull();
                assembler.manageInformation.setNull();
                assembler.quizInfo.setNull();

                for(int i=0;i<quiz.getStudents().size();i++){
                    for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                        if(quiz.getName().equals(quiz.getStudents().get(i).getQuizzes().get(j).getName())){
                            Quiz quiz1=quiz.getStudents().get(i).getQuizzes().get(j);
                            quiz1.setName(quiz.getName());
                            quiz1.setStopMinute(quiz.getStopMinute());
                            quiz1.setStopHour(quiz.getStopHour());
                            quiz1.setStartMinute(quiz.getStartMinute());
                            quiz1.setStartHour(quiz.getStartHour());
                            quiz1.setDay(quiz.getDay());
                            quiz1.setYear(quiz.getYear());
                            quiz1.setMonth(quiz.getMonth());
                            quiz1.setQuestions(quiz.getQuestions());
                            quiz1.setShuffle(quiz.isShuffle());
                            quiz1.setReview(quiz.isReview());
                            quiz1.setQuestionsShowedAtOnce(quiz.getQuestionsShowedAtOnce());
                            information.writeNewStudent(quiz.getStudents().get(i));

                        }
                    }
                }

                assembler.managerPanel.setTestCreated(false);
                quiz=null;
                assembler.showManagerPanel();
                information.writeNewManager(manager);
                AlertBox alertBox=new AlertBox();
                alertBox.display("","ذخیره آزمون با موفقیت انجام شد",720,400);
            }
        } else{
            AlertBox alertBox=new AlertBox();
            alertBox.display("خطا","نام آزمون انتخاب نشده است",720,400);
            }
        }
        public boolean isExamInTheArray(){
            Manager manager=(Manager)person;
            if(quiz==null||quiz.getName()==null)return false;
            for(int i=0;i<manager.getQuizes().size();i++){
                if(manager.getQuizes().get(i).getName().equals(quiz.getName()))return true;
            }
            return false;
        }
        public void setAverageAndRank(Quiz quiz){
        double sum=0;
        ArrayList<Double>arrayList=new ArrayList<>();
        ArrayList<Quiz>quizzes=new ArrayList<>();
        ArrayList<Student>students=new ArrayList<>();
        for(int i=0;i<quiz.getStudents().size();i++){
            for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++){
                if(quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())){
                        sum += quiz.getStudents().get(i).getQuizzes().get(j).getGrade();
                        arrayList.add(quiz.getStudents().get(i).getQuizzes().get(j).getGrade());
                        quizzes.add(quiz.getStudents().get(i).getQuizzes().get(j));
                        students.add(quiz.getStudents().get(i));
                        break;
                }
            }
        }
        if(arrayList.size()!=0)
        sum=sum/arrayList.size();
        else sum=0;
            Collections.sort(arrayList,Collections.reverseOrder());
            for(int i=0;i<quizzes.size();i++){
                for(int j=0;j<arrayList.size();j++){
                    if(quizzes.get(i).getGrade()==arrayList.get(j))
                        quizzes.get(i).setRank(arrayList.size());
                    quizzes.get(i).setAverage(sum);
                    information.writeQuizForStudents(students.get(i),quizzes.get(i));
                    break;
                }
            }


        }
    }

