package Logic;

import java.util.ArrayList;

public class Manager extends Person {
    private ArrayList<Quiz> quizes=new ArrayList<>();

    public ArrayList<Quiz> getQuizes() {
        return quizes;
    }

    public void setQuizes(ArrayList<Quiz> quizes) {
        this.quizes = quizes;
    }
}
