package Logic;

//import com.ibm.icu.util.Calendar;
//import com.ibm.icu.util.ULocale;

import GUI.AlertBox;
import GUI.Assembler;
import GUI.TakeTest;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;

import java.util.ArrayList;

public class Timer extends Thread {
    Label time=new Label();
    long elapsedSeconds;
    long secondsDisplay;
    long elapsedMinuets;
    long minuetsDisplay;
    long elapsedHours;
    long hoursDisplay;
    long millsLeft;
    long millsDisplay;
    long elapsedSeconds2;
    long secondsDisplay2;
    long elapsedMinuets2;
    long minuetsDisplay2;
    long elapsedHours2;
    long hoursDisplay2;
    long millsLeft2;
    long millsDisplay2;
    private Quiz quiz;
    private int currentQuestion;
    private Information information=new Information();
    private Student student;
    private TakeTest takeTest;
    private Assembler assembler;
    ULocale locale=new ULocale("fa_IR@calendar=persian");
    Calendar calendar=Calendar.getInstance(locale);



    public Timer(Quiz quiz, TakeTest takeTest, Student student, Assembler assembler) {
        this.takeTest=takeTest;
        this.student=student;
        this.assembler=assembler;
        time=takeTest.getTime();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        System.out.println(month);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int minute = calendar.get(Calendar.MINUTE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int totalSeconds = 60 - calendar.get(Calendar.SECOND);
        if (!quiz.getQuestionsShowedAtOnce()) {
            int start=quiz.getDay()*24*3600+quiz.getStartHour()*3600+quiz.getStartMinute()*60;
            for(int i=0;i<quiz.getQuestions().size();i++){
                Question question=quiz.getQuestions().get(quiz.getOrder().get(i));
                System.out.println("hour"+question.getHour());
                System.out.println("minute"+question.getMinuet());
                start=start+question.getHour()*3600+question.getMinuet()*60;
                if(start>day*24*3600+hour*3600+minute*60){
                    takeTest.setQuiz(quiz);
                    System.out.println("i="+i);
                    takeTest.setCurrentQuestion(i);
                    takeTest.displayQuestion();
                    millsLeft=start- day * 24 * 3600 - hour * 3600 - minute * 60;
                    break;
                }
            }
            millsLeft*=1000;
            millsLeft+=System.currentTimeMillis();
            int hours=0;
            int mins=0;

            for(int i=0;i<quiz.getQuestions().size();i++){
                mins+=quiz.getQuestions().get(i).getMinuet();
                hours+=quiz.getQuestions().get(i).getHour();
            }
            hours=hours+mins/60;
            mins=mins%60;
            millsLeft2=(quiz.getDay() * 24 * 3600 + (quiz.getStartHour() + hours) * 3600 + (quiz.getStartMinute() + mins) * 60 -
                    day * 24 * 3600 - hour * 3600 - minute * 60) * 1000;
            millsLeft2+=System.currentTimeMillis();
        } else {

            millsLeft = (quiz.getDay() * 24 * 3600 + (quiz.getStartHour() + quiz.getStopHour()) * 3600 + (quiz.getStartMinute() + quiz.getStopMinute()) * 60 -
                    day * 24 * 3600 - hour * 3600 - minute * 60) * 1000;
            millsLeft = millsLeft + System.currentTimeMillis();
        }



    }
    public void run(){
        System.out.println("current"+takeTest.getCurrentQuestion());



        while(/*currentQuestion<quiz.getQuestions().size()&&currentQuestion>=0*/currentQuestion!=-1) {
            long lastMin=0;
            long lastMin2=0;


            while (millsLeft - System.currentTimeMillis() > 0) {

                millsDisplay = millsLeft - System.currentTimeMillis();
                elapsedSeconds = millsDisplay / 1000;
                secondsDisplay = elapsedSeconds % 60;
                elapsedMinuets = elapsedSeconds / 60;
                minuetsDisplay = elapsedMinuets % 60;
                elapsedHours = elapsedMinuets / 60;
                hoursDisplay = elapsedHours % 60;
                if(secondsDisplay!=lastMin) {
                    lastMin=secondsDisplay;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            takeTest.setTime(hoursDisplay + ":" + minuetsDisplay + ":" + secondsDisplay);
                        }
                    });
                }
                if(!quiz.getQuestionsShowedAtOnce()){
                    millsDisplay2 = millsLeft2 - System.currentTimeMillis();
                    elapsedSeconds2 = millsDisplay2 / 1000;
                    secondsDisplay2 = elapsedSeconds2 % 60;
                    elapsedMinuets2 = elapsedSeconds2 / 60;
                    minuetsDisplay2 = elapsedMinuets2 % 60;
                    elapsedHours2 = elapsedMinuets2 / 60;
                    hoursDisplay2 = elapsedHours2 % 60;
                    if(secondsDisplay2!=lastMin2) {
                        lastMin2=secondsDisplay2;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                takeTest.setTime2(hoursDisplay2 + ":" + minuetsDisplay2 + ":" + secondsDisplay2);
                            }
                        });
                    }
                }

            }
            System.out.println("timer stoped");


            if(!quiz.getQuestionsShowedAtOnce()){

                if(takeTest.getCurrentQuestion()+1<quiz.getQuestions().size()){

                    saveMethod(takeTest.getCurrentQuestion());
                    takeTest.setCurrentQuestion(takeTest.getCurrentQuestion()+1);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            takeTest.displayQuestion();
                        }
                    });


                    calculateTimeNeeded();

                }
                else currentQuestion=-1;
            }
            else{
                saveMethod(takeTest.getCurrentQuestion());
                //get out of quiz page but howwwww????????????????????????????
                currentQuestion=-1;
            }
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                assembler.showStudentPanel();
            }
        });
        for(int i=0;i<quiz.getQuestions().size();i++){
            information.writeAnswerOfQuiz(quiz.getAnswers().get(i),i,student,quiz);
        }

    }
    public void calculateTimeNeeded(){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int minute = calendar.get(Calendar.MINUTE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int start=0;
        //for(int i=0;i<=takeTest.getCurrentQuestion();i++){
            Question question=quiz.getQuestions().get(quiz.getOrder().get(takeTest.getCurrentQuestion()));
            start=question.getHour()*3600+question.getMinuet()*60;



        //}
        millsLeft=System.currentTimeMillis()+(start)*1000;
        //millsLeft=start- day * 24 * 3600 - hour * 3600 - minute * 60;
    }


    public Calendar getCalendar() {
        return calendar;
    }
    public void saveMethod(int currentQuestion){
        Answer answer=new Answer();
        if(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getType() == 1){
            System.out.println("type");
            /*quiz.getAnswers().get(quiz.getOrder().get(currentQuestion))
                    .setAnswer(takeTest.getDescriptiveAnswer().getText());*/
            answer.setAnswer(takeTest.getDescriptiveAnswer().getText());
        } else if(quiz.getQuestions().get(quiz.getOrder().get(currentQuestion)).getType() == 2){
            System.out.println("type2");
            ArrayList<RadioButton>radioButtons=takeTest.getMultipleOptionRadioButtons();
            for(int i=0;i<radioButtons.size();i++){
                if(radioButtons.get(i).isSelected())answer.setAnswer(radioButtons.get(i).getText());
            }
            /*takeTest.getMultipleOptionToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                @Override
                public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                    for(int i=0;i<takeTest.getMultipleOptionRadioButtons().size();i++){
                        if(takeTest.getMultipleOptionToggleGroup().getSelectedToggle() ==
                                takeTest.getMultipleOptionRadioButtons().get(i)) {
                            for(int j = 0; j< student.getQuizzes().size(); j++){
                                if(student.getQuizzes().get(j).equals(quiz.getName())){
                                    student.getQuizzes().get(j).getAnswers().get(quiz.getOrder().get(currentQuestion))
                                            .setAnswer(takeTest.getMultipleOptionRadioButtons().get(i).getText());
                                }
                            }
                        }
                    }

                }
            });*/


        } else {
            System.out.println("type3");
            System.out.println(takeTest.getTrueFalseToggleGroup().size());
            for(int i=0;i<takeTest.getTrueFalseToggleGroup().size();i++){
                RadioButton selectedRadioButton = (RadioButton) takeTest.getTrueFalseToggleGroup().get(i).getSelectedToggle();
                if(selectedRadioButton!=null) {
                    System.out.println("sel");
                    String toogleGroupValue = selectedRadioButton.getText();
                    if(toogleGroupValue!=null){
                        System.out.println("whay");
                        System.out.println(toogleGroupValue);
                        if(toogleGroupValue.equals("صحیح")){
                            answer.getYesNoAnswers().add(i,2);
                            answer.setYesNoAnswer(i,2);
                        }
                        else if(toogleGroupValue.equals("غلط")){
                            System.out.println("yes");
                            answer.getYesNoAnswers().add(i,1);
                            answer.setYesNoAnswer(i,1);
                        }
                    }
                    else{
                        answer.getYesNoAnswers().add(i,0);
                        answer.setYesNoAnswer(i,0);
                    }
                }
                else{
                    answer.getYesNoAnswers().add(i,0);
                    answer.setYesNoAnswer(i,0);
                }
            }
        }
        quiz.getAnswers().set(quiz.getOrder().get(currentQuestion),answer);

                /*takeTest.getTrueFalseToggleGroup().get(i).selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                    @Override
                    public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                        for(int j = 0; j < takeTest.getTrueFalseRadioButtons().size(); j++){
                            if(takeTest.getTrueFalseToggleGroup().get(temp).getSelectedToggle() ==
                                    takeTest.getTrueFalseRadioButtons().get(j)) {
                                if(j % 2 == 0){
                                    student.getQuizzes().get(j).getAnswers().get(quiz.getOrder()
                                            .get(currentQuestion)).setYesNoAnswer(j/2,2);
                                    //true is 2
                                } else {
                                    student.getQuizzes().get(j).getAnswers().get(quiz.getOrder()
                                            .get(currentQuestion)).setYesNoAnswer(j/2,1);
                                    //false is 1
                                }
                            }
                        }

                    }
                });
            }
        }*/
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public void setMillsLeft(long millsLeft) {
        this.millsLeft = millsLeft;
    }
}
