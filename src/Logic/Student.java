package Logic;

import java.util.ArrayList;

public class Student extends Person {
    private Long studentId;
    private ArrayList<Quiz> quizzes =new ArrayList<>();

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public ArrayList<Quiz> getQuizzes() {
        return quizzes;
    }

    public void setQuizzes(ArrayList<Quiz> quizzes) {
        this.quizzes = quizzes;
    }
    public void addQuiz(Quiz quiz){
        quizzes.add(quiz);

    }
}
