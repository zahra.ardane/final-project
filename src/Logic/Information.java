package Logic;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import java.io.*;
import java.util.ArrayList;

public class Information {
    public Boolean isAccountNumberUsed(String accountNumber) {
        if (isStudentRegistered(accountNumber)) return true;
        if (isManagerRegistered(accountNumber)) return true;
        return false;

    }

    public Boolean isStudentIdUsed(long studentId) {
        File file = new File("src\\files\\students");
        if (!file.exists()) file.mkdirs();
        File[] innerFiles = file.listFiles();
        if (innerFiles == null) return false;
        for (int i = 0; i < innerFiles.length; i++) {
            String name = innerFiles[i].getName();
            String[] nameSplited = name.split("#");
            long checkId = Long.parseLong(nameSplited[0]);
            if (checkId ==studentId ) return true;

        }
        return false;
    }

    //check if it gets to students directory properly
    public Boolean isStudentRegistered(String accountNumber) {
        File file = new File("src\\files\\students");
        File[] innerFiles = file.listFiles();
        String[] accountNumberString;
        if (innerFiles == null) {
            System.out.println("its null");
            return false;

        }
        for (int i = 0; i < innerFiles.length; i++) {
            String name = innerFiles[i].getName();
            accountNumberString = name.split("#");
            StringBuffer checkAccountNumber = new StringBuffer();
            for (int j = 1; j < accountNumberString.length; j++) {
                checkAccountNumber.append(accountNumberString[j]);
            }
            System.out.println(checkAccountNumber.toString());
            System.out.println(accountNumber);
            if (checkAccountNumber.toString().equals(accountNumber)) return true;
        }
        return false;
    }

    public Boolean isManagerRegistered(String accountNumber) {
        //new File("src\\files\\managers\\" + accountNumber).mkdir();
        File file = new File("src\\files\\managers\\" + accountNumber + "\\" + accountNumber + ".bin");
        if (file.exists()) return true;
        else return false;
    }

    public Boolean isManagerPasswordCorrect(String accountNumber, String password) {
        try {
            File file = new File("src\\files\\managers");
            File[] innerFiles = file.listFiles();
            String fileName = null;
            if (innerFiles == null) return false;
            for (int i = 0; i < innerFiles.length; i++) {
                if (innerFiles[i].getName().equals(accountNumber)) {
                    fileName = innerFiles[i].getName();
                    break;
                }
                ;
            }
            RandomAccessFile file1 = null;

            file1 = new RandomAccessFile("src\\files\\managers\\" + fileName + "\\" + fileName + ".bin", "r");
            file1.seek(2 * Person.NAME_LENGTH);
            StringBuffer buffer = new StringBuffer();

            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String checkPassword = buffer.toString();
            checkPassword = checkPassword.trim();

            file1.close();
            if (checkPassword.equals(password)) return true;
            else return false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }

    public Boolean isStudentPasswordCorrect(String accountNumber, String password) {
        try {
            File file = new File("src\\files\\students");
            File[] innerFiles = file.listFiles();
            String[] accountNumberString;
            String randomAccessFileName = null;
            for (int i = 0; i < innerFiles.length; i++) {
                String name = innerFiles[i].getName();
                accountNumberString = name.split("#");
                StringBuffer checkAccountNumber = new StringBuffer();
                for (int j = 1; j < accountNumberString.length; j++) {
                    checkAccountNumber.append(accountNumberString[j]);
                }
                if (checkAccountNumber.toString().equals(accountNumber)) {
                    randomAccessFileName = name;
                    break;
                }
            }
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\students\\" + randomAccessFileName + "\\" + randomAccessFileName + ".bin", "r");
            file1.seek(2 * Person.NAME_LENGTH);
            /*StringBuffer stringBuffer=new StringBuffer();
            for(int i=0;i<Person.NAME_LENGTH;i++)stringBuffer.append(file1.readChar());*/
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String checkPassword = buffer.toString();
            checkPassword = checkPassword.trim();
            file1.close();
            if (checkPassword.equals(password)) return true;
            else return false;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void writeNewStudent(Student student) {
        //order:accountNumber,password,id,name,lastName
        try {
            String fileName = student.getStudentId() + "#" + student.getAccountNumber();
            File file = new File("src\\files\\students\\" + fileName);
            if (!file.exists()) file.mkdirs();
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\students\\" + fileName + "\\" + fileName + ".bin", "rw");
            StringBuffer buffer = new StringBuffer();
            buffer.append(student.getAccountNumber());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            buffer.delete(0, 49);
            buffer.append(student.getPassword());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            file1.writeLong(student.getStudentId());
            buffer.delete(0, 49);
            buffer.append(student.getName());
            buffer.setLength(Student.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            buffer.delete(0, 49);
            buffer.append(student.getSurName());
            buffer.setLength(Student.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            file1.close();
            for (int i = 0; i < student.getQuizzes().size(); i++)
                writeQuizForStudents(student, student.getQuizzes().get(i));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeNewManager(Manager manager) {
        //writing order:username,password,name,surname
        try {

            StringBuffer buffer = new StringBuffer();
            String fileName = manager.getAccountNumber();
            File file = new File("src\\files\\managers\\" + fileName);
            if (!file.exists()) file.mkdirs();
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\managers\\" + fileName + "\\" + fileName + ".bin", "rw");
            buffer.append(manager.getAccountNumber());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            buffer.delete(0, Person.NAME_LENGTH - 1);
            buffer.append(manager.getPassword());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            buffer.delete(0, Person.NAME_LENGTH - 1);
            buffer.append(manager.getName());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            buffer.delete(0, Person.NAME_LENGTH - 1);
            buffer.append(manager.getSurName());
            buffer.setLength(Person.NAME_LENGTH);
            file1.writeChars(buffer.toString());
            file1.close();
            /*for (int i = 0; i < manager.getQuizes().size(); i++) {
                writeQuizForManager(manager, manager.getQuizes().get(i));
                for(int j=0;j<manager.getQuizes().get(i).getStudents().size();j++){
                    writeNewStudent(manager.getQuizes().get(i).getStudents().get(j));
                }
            }*/
            for(int i=0;i<manager.getQuizes().size();i++){
                System.out.println("i"+i);
                for(int j=0;j<manager.getQuizes().get(i).getQuestions().size();j++){
                    System.out.println("j"+j);
                    writeQuestionToFile(manager.getQuizes().get(i).getName(),j,manager,manager.getQuizes().get(i).getQuestions().get(j));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Student readStudentWithId(long studentId) {

        Student student = new Student();
        String fileName = null;
        File file = new File("src\\files\\students");
        File[] innerFiles = file.listFiles();
        for (int i = 0; i < innerFiles.length; i++) {
            String name = innerFiles[i].getName();
            String[] nameSplit = name.split("#");
            long checkId = Long.parseLong(nameSplit[0]);
            if (checkId == studentId) {
                fileName = innerFiles[i].getName();
                break;
            }
        }
        try {
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\students\\" + fileName + "\\" + fileName + ".bin", "r");
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String name = buffer.toString();
            name = name.trim();
            student.setAccountNumber(name);
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String password = buffer.toString();
            password = password.trim();
            student.setPassword(password);
            student.setStudentId(file1.readLong());
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name=buffer.toString();

            name = name.trim();
            student.setName(name);
            buffer.delete(0, 49);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name = buffer.toString();
            name = name.trim();
            student.setSurName(name);
            file1.close();
            ArrayList<Quiz> quizzes = readQuizzesForStudent(student);
            if (quizzes != null) student.setQuizzes(quizzes);
            for(int i=0;i<student.getQuizzes().size();i++) {
                ArrayList<Answer> answers = readStudentAnswersToQuiz(student, student.getQuizzes().get(i));
                if (answers == null) answers = new ArrayList<>();
                for (int j = 0; j < student.getQuizzes().get(i).getQuestions().size(); j++) {
                    if (answers.size() <= j) {
                        answers.add(j, new Answer());
                    }
                }

                student.getQuizzes().get(i).setAnswers(answers);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return student;
    }

    public Student readStudent(String accountNumber) {
        Student student = new Student();
        try {
            String fileName = null;
            File studentPath = new File("src\\files\\students");
            File[] files = studentPath.listFiles();
            for (int i = 0; i < files.length; i++) {
                String[] fileNameSplit = files[i].getName().split("#");
                StringBuffer stringBuffer = new StringBuffer();
                for (int j = 1; j < fileNameSplit.length; j++) {
                    stringBuffer.append(fileNameSplit[j]);
                }
                if (stringBuffer.toString().equals(accountNumber)) {
                    fileName = files[i].getName();
                    break;

                }
            }
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\students\\" + fileName + "\\" + fileName + ".bin", "r");
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String name = buffer.toString();
            name = name.trim();
            student.setAccountNumber(name);
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String password = buffer.toString();
            password = password.trim();
            student.setPassword(password);
            student.setStudentId(file1.readLong());
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name=buffer.toString();
            name = name.trim();
            student.setName(name);
            buffer.delete(0, 49);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name = buffer.toString();
            name = name.trim();
            student.setSurName(name);
            file1.close();
            ArrayList<Quiz>quizzes=readQuizzesForStudent(student);
            if(quizzes!=null){
                student.setQuizzes(quizzes);
            }
            for(int i=0;i<student.getQuizzes().size();i++){
                ArrayList<Question>questions=readQuestionsFromFile(student.getQuizzes().get(i).getName());
                if(questions!=null)student.getQuizzes().get(i).setQuestions(questions);
                ArrayList<Answer>answers=readStudentAnswersToQuiz(student,student.getQuizzes().get(i));
                if(answers==null)answers=new ArrayList<>();
                for(int j=0;j<student.getQuizzes().get(i).getQuestions().size();j++){
                    if(answers.size()<=j){
                        answers.add(j,new Answer());
                    }
                }
                student.getQuizzes().get(i).setAnswers(answers);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return student;
    }

    public Manager readManager(String accountNumber) {
        Manager manager = new Manager();
        try {
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\managers\\" + accountNumber + "\\" + accountNumber + ".bin", "r");
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String name = buffer.toString();
            name = name.trim();
            manager.setAccountNumber(name);
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            String password = buffer.toString();
            password = password.trim();
            manager.setPassword(password);
            buffer.delete(0, Person.NAME_LENGTH - 1);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name = buffer.toString();
            name = name.trim();
            manager.setName(name);
            buffer.delete(0, 49);
            for (int i = 0; i < Student.NAME_LENGTH; i++) {
                char ch = file1.readChar();
                buffer.append(ch);
            }
            name = buffer.toString();
            name = name.trim();
            manager.setSurName(name);
            file1.close();
            ArrayList<Quiz> quizzes = readManagerExams(manager);
            if (quizzes != null) {
                manager.setQuizes(quizzes);
                for(int i=0;i<manager.getQuizes().size();i++){
                    System.out.println(manager.getQuizes().get(i).getName());
                    ArrayList<Question>questions=readQuestionsFromFile(manager.getQuizes().get(i).getName());
                    if(questions!=null)
                    manager.getQuizes().get(i).setQuestions(questions);;
                    //System.out.println(manager.getQuizes().get(i).getStudents().get(0).getStudentId());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return manager;
    }

    //method readFromExcel is nt checked
    public ArrayList<Student> readFromExcel(String path,Quiz quiz){
        try {
            FileInputStream fis=new FileInputStream(path);
            Workbook workbook= WorkbookFactory.create(fis);
            Sheet sheet=workbook.getSheet("sheet1");
            int numberOfRows=sheet.getLastRowNum();
            ArrayList<Student>students=new ArrayList<>();
            for(int i=0;i<=numberOfRows;i++) {
                Student student = new Student();
                double idString = sheet.getRow(i).getCell(0).getNumericCellValue();

                student.setStudentId((long) idString);
                student.setAccountNumber(String.valueOf(idString));
                student.setName(sheet.getRow(i).getCell(1).getStringCellValue());
                student.setSurName(sheet.getRow(i).getCell(2).getStringCellValue());
                int sw = 0;
                for (int j = 0; j < quiz.getStudents().size(); j++) {
                    if (quiz.getStudents().get(j).getStudentId() == (long) idString) {
                        sw = 1;
                        break;
                    }
                }
                if (sw == 0) {
                    if (!isStudentIdUsed((long) idString)){
                        students.add(student);
                    quiz.getStudents().add(student);}
                    else {
                        Student checkStudent = readStudentWithId((long) idString);
                        if (checkStudent.getName().equals(student.getName())) {
                            if (student.getSurName().equals(checkStudent.getSurName())) {students.add(student);
                            quiz.getStudents().add(student);}
                        }
                    }


                }
            }
            fis.close();
            return students;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;


    }
    public void writeQuestionToFile(String quizName, int questionNumber, Manager manager, Question question) {
        //write order:type,number of options,options,context
        try {
            File file = new File("src\\files\\managers\\" + manager.getAccountNumber() + "\\tests\\" + quizName + "\\answers");
            if (!file.exists()) file.mkdirs();
            RandomAccessFile file1 = new RandomAccessFile("src\\files\\managers\\" + manager.getAccountNumber() +
                    "\\tests\\" + quizName + "\\answers\\" + questionNumber + ".bin", "rw");
            file1.writeInt(question.getType());
            file1.writeInt(question.getHour());
            file1.writeInt(question.getMinuet());
            file1.writeDouble(question.getGrade());
            if (question.getType() != 1) {
                if (question.getOptions() != null) {
                    file1.writeInt(question.getOptions().size());
                    StringBuffer stringBuffer = new StringBuffer();
                    for (int i = 0; i < question.getOptions().size(); i++) {
                        stringBuffer.append(question.getOptions().get(i));
                        stringBuffer.setLength(Question.ANSWERS_LENGTH);
                        file1.writeChars(stringBuffer.toString());
                        stringBuffer.delete(0, 499);
                    }
                    file1.writeInt(question.getTrueFalsePhrases().size());
                    for(int j=0;j<question.getTrueFalsePhrases().size();j++){
                        stringBuffer.append(question.getTrueFalsePhrases().get(j));
                        stringBuffer.setLength(Question.ANSWERS_LENGTH);
                        file1.writeChars(stringBuffer.toString());
                        stringBuffer.delete(0, 499);
                    }
                } else file1.writeInt(0);
            }
            file1.writeChars(question.getContext());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Question> readQuestionsFromFile(String quizName) {
        try {
            String[] quizNameSplit = quizName.split("#");
            StringBuffer stringBuffer1=new StringBuffer();
            for(int i=1;i<quizNameSplit.length;i++){
                stringBuffer1.append(quizNameSplit[i]);
            }
            File file = new File("src\\files\\managers\\" + stringBuffer1.toString() +
                    "\\tests\\" + quizName + "\\answers");
            if (!file.exists()) {
                System.out.println("dexist");
                return null;}
            ArrayList<Question> questions = new ArrayList<>();
            File[] innerFiles = file.listFiles();
            if (innerFiles == null){
                System.out.println("null");
                return null;}
            for (int i = 0; i < innerFiles.length; i++) {
                Question newQuestion = new Question();

                RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\managers\\" + stringBuffer1.toString() +
                        "\\tests\\" + quizName + "\\answers\\" + i + ".bin", "r");
                newQuestion.setType(randomAccessFile.readInt());
                newQuestion.setHour(randomAccessFile.readInt());
                newQuestion.setMinuet(randomAccessFile.readInt());
                newQuestion.setGrade(randomAccessFile.readDouble());
                if (newQuestion.getType() != 1) {
                    int numberOfAnswers = randomAccessFile.readInt();
                    for (int j = 0; j < numberOfAnswers; j++) {
                        StringBuffer stringBuffer = new StringBuffer();
                        for (int k = 0; k < Question.ANSWERS_LENGTH; k++) {
                            stringBuffer.append(randomAccessFile.readChar());
                        }
                        newQuestion.getOptions().add(stringBuffer.toString().trim());
                    }
                    numberOfAnswers = randomAccessFile.readInt();
                    for (int j = 0; j < numberOfAnswers; j++) {
                        StringBuffer stringBuffer = new StringBuffer();
                        for (int k = 0; k < Question.ANSWERS_LENGTH; k++) {
                            stringBuffer.append(randomAccessFile.readChar());
                        }
                        newQuestion.getTrueFalsePhrases().add(stringBuffer.toString().trim());
                    }

                }
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    try {
                        char newChar = randomAccessFile.readChar();
                        stringBuffer.append(newChar);


                    } catch (EOFException e) {
                        newQuestion.setContext(stringBuffer.toString());
                        break;
                    }
                }
                randomAccessFile.close();
                questions.add(newQuestion);
            }
            return questions;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void writeQuizForManager(Manager manager, Quiz quiz) {
        //write order:quizName,start hour,start minuets,duration hour,duration minuets,students' id
        try {
            File file = new File("src\\files\\managers\\" + manager.getAccountNumber() + "\\tests\\" + quiz.getName());
            if (!file.exists()) file.mkdirs();

            RandomAccessFile file1 = new RandomAccessFile("src\\files\\managers\\" + manager.getAccountNumber() +
                    "\\tests\\" + quiz.getName() + "\\" + quiz.getName() + ".bin", "rw");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(quiz.getName());
            stringBuffer.setLength(Quiz.NAME_LENGTH);
            file1.writeChars(stringBuffer.toString());
            file1.writeInt(quiz.getStartHour());
            file1.writeInt(quiz.getStartMinute());
            file1.writeInt(quiz.getStopHour());
            file1.writeInt(quiz.getStopMinute());
            file1.writeInt(quiz.getDay());
            file1.writeInt(quiz.getMonth());
            file1.writeInt(quiz.getYear());
            file1.writeBoolean(quiz.getQuestionsShowedAtOnce());
            file1.writeBoolean(quiz.isReview());
            file1.writeBoolean(quiz.isShuffle());
            file1.writeDouble(quiz.getGrade());

            file1.writeInt(quiz.getStudents().size());
            for (int i = 0; i < quiz.getStudents().size(); i++) {
                file1.writeLong(quiz.getStudents().get(i).getStudentId());
            }
            file1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Quiz> readManagerExams(Manager manager) {
        try {
            File file = new File("src\\files\\managers\\" + manager.getAccountNumber() + "\\tests");
            if (!file.exists()) return null;
            File[] files = file.listFiles();
            if (files == null) return null;
            ArrayList<Quiz> quizzes = new ArrayList<>();
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                Quiz quiz = new Quiz();

                RandomAccessFile file1 = new RandomAccessFile("src\\files\\managers\\" + manager.getAccountNumber() +
                        "\\tests\\" + fileName + "\\" + fileName + ".bin", "r");
                StringBuffer quizName = new StringBuffer();
                for (int j = 0; j < Quiz.NAME_LENGTH; j++) {
                    quizName.append(file1.readChar());
                }
                quiz.setName(quizName.toString().trim());
                quiz.setStartHour(file1.readInt());
                quiz.setStartMinute(file1.readInt());
                quiz.setStopHour(file1.readInt());
                quiz.setStopMinute(file1.readInt());
                quiz.setDay(file1.readInt());
                quiz.setMonth(file1.readInt());
                quiz.setYear(file1.readInt());
                quiz.setQuestionsShowedAtOnce(file1.readBoolean());
                quiz.setReview(file1.readBoolean());
                quiz.setShuffle(file1.readBoolean());
                quiz.setGrade(file1.readDouble());
                int studentsNumber = file1.readInt();
                for (int j = 0; j < studentsNumber; j++) {
                    long studentid = file1.readLong();
                    Student newStudent = readStudentWithId(studentid);
                    quiz.addStudent(newStudent);

                }
                file1.close();
                ArrayList<Question> questions = readQuestionsFromFile(quiz.getName());
                if (questions != null) quiz.setQuestions(questions);
                quizzes.add(quiz);


            }
            return quizzes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    //writes students' quizzes in a file
    public ArrayList<Quiz> readQuizzesForStudent(Student student) {
        try {
            String fileName = student.getStudentId() + "#" + student.getAccountNumber();
            File file = new File("src\\files\\students\\" + fileName + "\\tests");
            if (!file.exists()) return null;
            File[] innerFiles = file.listFiles();
            if (innerFiles == null) return null;
            ArrayList<Quiz> quizzes = new ArrayList<>();
            for (File file1 : innerFiles) {
                RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\students\\" + fileName + "\\tests\\"
                        + file1.getName() + "\\" + file1.getName() +
                        ".bin", "r");
                Quiz newQuiz = new Quiz();
                StringBuffer quizName = new StringBuffer();
                for (int j = 0; j < Quiz.NAME_LENGTH; j++) {
                    quizName.append(randomAccessFile.readChar());
                }
                newQuiz.setName(quizName.toString().trim());

                newQuiz.setStartHour(randomAccessFile.readInt());
                newQuiz.setStartMinute(randomAccessFile.readInt());
                newQuiz.setStopHour(randomAccessFile.readInt());
                newQuiz.setStopMinute(randomAccessFile.readInt());
                newQuiz.setGrade(randomAccessFile.readDouble());
                newQuiz.setBanned(randomAccessFile.readBoolean());
                newQuiz.setDay(randomAccessFile.readInt());
                newQuiz.setMonth(randomAccessFile.readInt());
                newQuiz.setYear(randomAccessFile.readInt());
                newQuiz.setQuestionsShowedAtOnce(randomAccessFile.readBoolean());
                newQuiz.setReview(randomAccessFile.readBoolean());
                newQuiz.setShuffle(randomAccessFile.readBoolean());
                newQuiz.setGraded(randomAccessFile.readBoolean());
                newQuiz.setAverage(randomAccessFile.readDouble());
                newQuiz.setRank(randomAccessFile.readInt());
                newQuiz.setHardshipLevel(randomAccessFile.readInt());
                int numberOfQuestions = randomAccessFile.readInt();
                ArrayList<Integer> order = new ArrayList<>();
                for (int j = 0; j < numberOfQuestions; j++) {
                    order.add(randomAccessFile.readInt());
                }
                newQuiz.setOrder(order);
                randomAccessFile.close();
                ArrayList<Question> questions = readQuestionsFromFile(newQuiz.getName());
                if (questions != null) newQuiz.setQuestions(questions);
                quizzes.add(newQuiz);

            }
            return quizzes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeQuizForStudents(Student student, Quiz quiz) {
        try {
            String fileName = student.getStudentId() + "#" + student.getAccountNumber();
            File file = new File("src\\files\\students\\" + fileName + "\\tests");
            if (!file.exists()) file.mkdirs();
            file = new File("src\\files\\students\\" + fileName + "\\tests\\" + quiz.getName());
            if (!file.exists()) file.mkdirs();

            RandomAccessFile file1 = new RandomAccessFile("src\\files\\students\\" + fileName + "\\tests\\"
                    + quiz.getName() + "\\" + quiz.getName() +
                    ".bin", "rw");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(quiz.getName());
            stringBuffer.setLength(Quiz.NAME_LENGTH);
            file1.writeChars(stringBuffer.toString());
            file1.writeInt(quiz.getStartHour());
            file1.writeInt(quiz.getStartMinute());
            file1.writeInt(quiz.getStopHour());
            file1.writeInt(quiz.getStopMinute());
            file1.writeDouble(quiz.getGrade());
            file1.writeBoolean(quiz.getIsBanned());
            file1.writeInt(quiz.getDay());
            file1.writeInt(quiz.getMonth());
            file1.writeInt(quiz.getYear());
            file1.writeBoolean(quiz.getQuestionsShowedAtOnce());
            file1.writeBoolean(quiz.isReview());
            file1.writeBoolean(quiz.isShuffle());
            file1.writeBoolean(quiz.isGraded());
            file1.writeDouble(quiz.getAverage());
            file1.writeInt(quiz.getRank());
            file1.writeInt(quiz.getOrder().size());
            file1.writeInt(quiz.getHardshipLevel());
            file1.writeInt(student.getQuizzes().size());
            for (int j = 0; j < quiz.getOrder().size(); j++) {
                file1.writeInt(quiz.getOrder().get(j));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void deleteQuestionFileFromExam(String quizName, int questionNumber) {
        String[] nameSplit = quizName.split("#");
        File file = new File("src\\files\\managers\\" + nameSplit[0] +
                "\\tests\\" + quizName + "\\answers\\" + questionNumber + ".bin");
        file.delete();
        File[] innerFiles = file.listFiles();
        for (int i = questionNumber - 1; i < innerFiles.length; i++) {
            innerFiles[i].renameTo(new File("src\\files\\managers\\" + nameSplit[0] +
                    "\\tests\\" + quizName + "\\answers\\" + (i + 1) + ".bin"));
        }
    }

    public void deleteExamForStudent(String quizName, Student student) {
        String fileName = student.getStudentId() + "#" + student.getAccountNumber();
        File file = new File("src\\files\\students\\" + fileName + "\\tests\\"
                + quizName);
        File[] innerFiles = file.listFiles();
        if (innerFiles != null){
            for (int i = 0; i < innerFiles.length; i++) {
                if (innerFiles[i].getName().equals(quizName)) {
                    deleteDirectory(innerFiles[i]);
                }
            }
    }
    }

    public void deleteDirectory(File file) {
        if (file.isFile()) {
            file.delete();
        } else {
            File[] innerFiles = file.listFiles();
            for (File file1 : innerFiles) {
                deleteDirectory(file1);
            }
        }

    }

    public void writeAnswerOfQuiz(Answer answer, int questionNumber, Student student, Quiz quiz) {
        try {
            String fileName = student.getStudentId() + "#" + student.getAccountNumber();
            File file = new File("src\\files\\students\\" + fileName + "\\tests\\" + quiz.getName() + "\\answers\\"
                    + questionNumber);
            if (!file.exists()) file.mkdirs();
            RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\students\\" + fileName + "\\tests\\" + quiz.getName() + "\\answers"+"\\"+
                    + questionNumber + "\\" + questionNumber + ".bin", "rw");
            randomAccessFile.writeDouble(answer.getGrade());
            randomAccessFile.writeInt(answer.getYesNoAnswers().size());
            for(int i=0;i<answer.getYesNoAnswers().size();i++){
                randomAccessFile.writeInt(answer.getYesNoAnswers().get(i));
                /*StringBuffer stringBuffer=new StringBuffer();
                stringBuffer.append(answer.getYesNoAnswers().get(i));
                stringBuffer.setLength(Answer.YESNOSIZE);
                randomAccessFile.writeChars(stringBuffer.toString());*/

            }
            if(answer.getAnswer()!=null){
                System.out.println("answer.getAnswer():"+answer.getAnswer());
            randomAccessFile.writeChars(answer.getAnswer());}
            randomAccessFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Answer> readStudentAnswersToQuiz(Student student, Quiz quiz) {
        try {
            String fileName = student.getStudentId() + "#" + student.getAccountNumber();
            File file = new File("src\\files\\students\\" + fileName + "\\tests\\" + quiz.getName() + "\\answers");

            if (!file.exists()) return null;
            ArrayList<Answer> returnAnswers = new ArrayList<>();
            File[] innerFiles = file.listFiles();
            for (int i = 0; i < innerFiles.length; i++) {
                Answer newAnser = new Answer();
                File[] answers = innerFiles[i].listFiles();
                for (File file1 : answers) {
                    if (file1.getName().equals(i + ".bin")) {
                        RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\students\\" + fileName
                                + "\\tests\\" + quiz.getName() + "\\answers\\" + i + "\\" + i + ".bin", "r");
                        newAnser.setGrade(randomAccessFile.readDouble());
                        int yesNoAnswers=randomAccessFile.readInt();
                        ArrayList<String>arrayList=new ArrayList<>();
                        for(int j=0;j<yesNoAnswers;j++){
                            int index=randomAccessFile.readInt();
                            newAnser.getYesNoAnswers().add(j,index);
                            newAnser.setYesNoAnswer(j,index);
                            //StringBuffer stringBuffer = new StringBuffer();
                            //for(int k=0;k<Answer.YESNOSIZE;k++){
                                //stringBuffer.append(randomAccessFile.readChar());
                            //}
                            //arrayList.add(stringBuffer.toString());
                        }
                        //newAnser.setYesNoAnswers(arrayList);
                        StringBuffer stringBuffer=new StringBuffer();
                        try {

                            while (true) {
                                stringBuffer.append(randomAccessFile.readChar());
                            }
                        } catch (EOFException e) {
                            newAnser.setAnswer(stringBuffer.toString());
                            randomAccessFile.close();
                        }
                    } else newAnser.setImageUrl(file1.getName());
                }
                returnAnswers.add(newAnser);

            }
            return returnAnswers;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void quizByQuizToExcel(String path, Manager manager)  {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("hi.xlsx");


            Workbook wb = WorkbookFactory.create(fis);
            Sheet sh = wb.getSheet("sheet1");
            for (int index = sh.getLastRowNum(); index >= sh.getFirstRowNum(); index--) {
                if (sh.getRow(index) != null)
                    sh.removeRow(sh.getRow(index));
            }
            for(int i=0;i<manager.getQuizes().size();i++){
                int sum=0;
                sh.createRow(i).createCell(0);
                sh.getRow(i).createCell(1);
                for(int j=0;j<manager.getQuizes().get(i).getStudents().size();j++){
                    for(int k=0;k<manager.getQuizes().get(i).getStudents().get(j).getQuizzes().size();k++){
                        sum+=manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getGrade();
                        /*for(int z=0;z<manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().size();z++) {
                            if(manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().get(z)!=null) {
                                sum += manager.getQuizes().get(i).getStudents().get(j).getQuizzes().get(k).getAnswers().get(z).getGrade();
                            }

                        }*/
                    }
                }
                if(manager.getQuizes().get(i).getStudents().size()!=0)
                sum/=manager.getQuizes().get(i).getStudents().size();
                String[] quizNameTrim=manager.getQuizes().get(i).getName().split("#");
                StringBuffer stringBuffer=new StringBuffer();
                for(int j=1;j<quizNameTrim.length;j++)stringBuffer.append(quizNameTrim[0]);
                sh.getRow(i).getCell(0).setCellValue(stringBuffer.toString());
                sh.getRow(i).getCell(1).setCellValue(sum);
            }
            File file=new File(path);
            System.out.println(file.toURI().toString());
            FileOutputStream fos=new FileOutputStream(path+"\\graph.xlsx");
            wb.write(fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void personByPersonToExcel(String path,Quiz quiz) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("hi.xlsx");


            Workbook wb = WorkbookFactory.create(fis);
            Sheet sh = wb.getSheet("sheet1");
            for (int index = sh.getLastRowNum(); index >= sh.getFirstRowNum(); index--) {
                if (sh.getRow(index) != null)
                    sh.removeRow(sh.getRow(index));
            }
            for(int i=0;i<quiz.getStudents().size();i++){
                sh.createRow(i).createCell(0);
                sh.getRow(i).createCell(1);
                for(int j=0;j<quiz.getStudents().get(i).getQuizzes().size();j++) {
                    if (quiz.getStudents().get(i).getQuizzes().get(j).getName().equals(quiz.getName())) {
                        sh.getRow(i).getCell(0).setCellValue(quiz.getStudents().get(i).getStudentId());
                        int sum=0;
                        /*for(int k=0;k<quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().size();k++){
                            if(quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().get(k)!=null){
                                sum+=quiz.getStudents().get(i).getQuizzes().get(j).getAnswers().get(k).getGrade();
                            }
                        }*/
                        sh.getRow(i).getCell(1).setCellValue(quiz.getStudents().get(i).getQuizzes().get(j).getGrade());
                        break;
                    }
                }
            }
            FileOutputStream fos=new FileOutputStream(path+"\\graph.xlsx");
            wb.write(fos);
            fos.flush();
            fos.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Message> readMessage(){
        System.out.println("in read message");
        ArrayList<Message> messages = new ArrayList<>();
        try {
            File file = new File("src\\files\\chats");
            if (!file.exists()) file.mkdirs();
            RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\chats\\chats.bin", "rw");
            for(int i = 0; ; i++){
                String line = randomAccessFile.readLine();
                if(line == null) break;
                System.out.println(line);
                String[] splited = line.split("#");
                Message message = new Message();
                message.setQuizName(splited[0]+"#"+splited[1]);
                message.setSender(splited[2]);
                message.setMessage(splited[3]);
                try {
                    message.setHour((int)Integer.parseInt(splited[4]));
                    message.setMinute((int)Integer.parseInt(splited[5]));
                } catch (Exception e){
                    message.setHour(17);
                    message.setMinute(15);
                }

                messages.add(message);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return messages;
    }
    public void writeMessage(Message message){
        try {
            File file = new File("src\\files\\chats");
            if (!file.exists()) file.mkdirs();
            RandomAccessFile randomAccessFile = new RandomAccessFile("src\\files\\chats\\chats.bin", "rw");
            randomAccessFile.seek(randomAccessFile.length());
            randomAccessFile.writeChars(message.getQuizName()+"#"+message.getSender()+"#"+message.getMessage()+
                    "#"+message.getHour()+"#"+message.getMinute()+"\n");

        } catch (IOException e){
            e.printStackTrace();
        }
    }

}