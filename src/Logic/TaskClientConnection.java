package Logic;

import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;
import javafx.application.Platform;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TaskClientConnection implements Runnable{

    Socket socket;
    Server server;
    DataInputStream input;
    DataOutputStream output;
    private Information information = new Information();
    private ULocale locale=new ULocale("fa_IR@calendar=persian");
    private Calendar calendar=Calendar.getInstance(locale);

    public TaskClientConnection(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {

        try {

            int minute = calendar.get(Calendar.MINUTE);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);

            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());

            while (true) {
                String message = input.readUTF();

                String[] splited = message.split("#");
                Message m = new Message();
                m.setQuizName(splited[0] + "#" + splited[1]);
                m.setSender(splited[2]);
                m.setMessage(splited[3]);
                m.setHour(hour);
                m.setMinute(minute);
                information.writeMessage(m);

                System.out.println("task client connection : "+message);
                server.broadcast(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public void sendMessage(String message) {
        try {
            output.writeUTF(message);
            output.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
