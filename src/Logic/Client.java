package Logic;

import GUI.Assembler;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {

    private DataOutputStream output = null;
    private String message;
    private Assembler assembler;
    private Person person;

    public Client(Assembler assembler,Person person) {

        try {
            this.assembler=assembler;
            this.person = person;
            Socket socket = new Socket("localhost", 8818);

            output = new DataOutputStream(socket.getOutputStream());

            TaskReadThread task = new TaskReadThread(socket, this);
            Thread thread = new Thread(task);
            thread.start();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public DataOutputStream getOutput(){
        return output;
    }

    public Assembler getAssembler(){
        return assembler;
    }

    public Person getPerson(){
        return person;
    }
}
