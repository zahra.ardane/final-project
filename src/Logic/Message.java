package Logic;


public class Message {

    private String sender;
    private String quizName;
    private String message;
    private int hour;
    private int minute;

    public void setSender(String sender){
        this.sender = sender;
    }

    public String getSender(){
        return sender;
    }

    public void setQuizName(String quizName){
        this.quizName = quizName;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setHour(int hour){
        this.hour = hour;
    }

    public int getHour() {
        return hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getMinute() {
        return minute;
    }
}
