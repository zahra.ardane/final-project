package Logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class Question {
    //1 for descriptive 2 for multiple answer 3 for yes no questions
    private int type;
    private String context;
    private ArrayList<String> options=new ArrayList<>();
    private ArrayList<String> trueFalsePhrases = new ArrayList<>();
    private int hour;
    private int minuet;
    private double grade;
    public static final int ANSWERS_LENGTH=500;

    public void setType(int type) {
        this.type = type;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public String getContext() {
        return context;
    }

    public int getType() {
        return type;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinuet(int minuet) {
        this.minuet = minuet;
    }

    public ArrayList<String> getTrueFalsePhrases() {
        return trueFalsePhrases;
    }

    public void setTrueFalsePhrases(ArrayList<String> trueFalsePhrases) {
        this.trueFalsePhrases = trueFalsePhrases;
    }

    public double getGrade() {
        return grade;
    }

    public int getHour() {
        return hour;
    }

    public int getMinuet() {
        return minuet;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}
