package Logic;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

public class Quiz {
    public static final int NAME_LENGTH=50;
    private boolean questionsShowedAtOnce=true;
    private boolean isBanned;
    private boolean review;
    private String name;
    private double grade;
    private int startHour;
    private int startMinute;
    private int stopHour;
    private int stopMinute;
    private int day;
    private int month;
    private int year;
    private int hardshipLevel;
    private boolean isShuffle;
    private boolean isGraded;
    private int rank;
    private double average;
    private ArrayList<Student> students=new ArrayList<>();
    private ArrayList<Question>questions=new ArrayList<>();
    private ArrayList<Integer> order =new ArrayList<>();
    private ArrayList<Answer>answers=new ArrayList<>();
    public void addStudent(Student student){
        students.add(student);
    }
    public void addQuestions(Question question){
        questions.add(question);
    }

    public void shuffle(){
        Random r = new Random();
        System.out.println("length:"+getOrder().size());

        //for(int i=0;i<students.size();i++){
          //  for(int j=0;j<students.get(i).getQuizzes().size();j++){
            //    if(students.get(i).getQuizzes().get(j).getName().equals(name)){
                    for(int k = getOrder().size()-1;k>0;k--){
                        int random = r.nextInt(k+1);
                        int temp = getOrder().get(k);
                        getOrder().set(k,getOrder().get(random));
                        getOrder().set(random,temp);

                    }
                    for(int i=0;i<getOrder().size();i++){
                        System.out.println(getOrder().get(i));;
                    }



    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public double getGrade() {
        return grade;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    public boolean getIsBanned() {
        return isBanned;
    }

    public void setOrder(ArrayList<Integer> order) {
        this.order = order;
    }

    public ArrayList<Integer> getOrder() {
        return order;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }


    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public void setStartMinute(int startMinuet) {
        this.startMinute = startMinuet;
    }

    public void setStopHour(int stopHour) {
        this.stopHour = stopHour;
    }

    public void setStopMinute(int stopMinuet) {
        this.stopMinute = stopMinuet;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public int getStopHour() {
        return stopHour;
    }

    public int getStopMinute() {
        return stopMinute;
    }

    public void setHardshipLevel(int hardshipLevel){
        this.hardshipLevel = hardshipLevel;
    }

    public int getHardshipLevel(){
        return hardshipLevel;
    }

    public void setQuestionsShowedAtOnce(Boolean questionsShowedAtOnce) {
        this.questionsShowedAtOnce = questionsShowedAtOnce;
    }

    public Boolean getQuestionsShowedAtOnce() {
        return questionsShowedAtOnce;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }

    public void setReview(boolean review) {
        this.review = review;
    }

    public boolean isReview() {
        return review;
    }

    public void setShuffle(boolean shuffle) {
        isShuffle = shuffle;
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public void setGraded(boolean graded) {
        isGraded = graded;
    }

    public boolean isGraded() {
        return isGraded;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getAverage() {
        return average;
    }

    public int getRank() {
        return rank;
    }
}
