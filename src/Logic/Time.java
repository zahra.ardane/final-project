package Logic;

public class Time {
    private int hour;
    private int minuets;
    private int seconds;

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinuets(int minuets) {
        this.minuets = minuets;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getHour() {
        return hour;
    }

    public int getMinuets() {
        return minuets;
    }

    public int getSeconds() {
        return seconds;
    }
}
