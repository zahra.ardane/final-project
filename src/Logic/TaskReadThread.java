package Logic;

import com.ibm.icu.text.IDNA;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;
import javafx.application.Platform;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class TaskReadThread implements Runnable{

    private Socket socket;
    private Client client;
    private DataInputStream input;
    private Information information = new Information();
    private ULocale locale=new ULocale("fa_IR@calendar=persian");
    private Calendar calendar=Calendar.getInstance(locale);

    public TaskReadThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int minute = calendar.get(Calendar.MINUTE);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                input = new DataInputStream(socket.getInputStream());
                String message = input.readUTF();

                String[] splited = message.split("#");

                Platform.runLater(() -> {
                    if (client.getPerson() instanceof Manager) {
                        Manager manager = (Manager) client.getPerson();
                        for (int i = 0; i < manager.getQuizes().size(); i++) {
                            if (manager.getQuizes().get(i).getName().equals(splited[0] + "#" + splited[1])) {
                                client.setMessage(message);
                                if (splited[2].equals(manager.getAccountNumber())) {
                                    client.getAssembler().chats.get(i).setNewText(splited[2], splited[3], true, hour, minute);
                                } else {
                                    client.getAssembler().chats.get(i).setNewText(splited[2], splited[3], false, hour, minute);
                                }
                                break;
                            }

                        }
                    } else {
                        Student student = (Student) client.getPerson();
                        for (int i = 0; i < student.getQuizzes().size(); i++) {
                            if (student.getQuizzes().get(i).getName().equals(splited[0] + "#" + splited[1])) {
                                client.setMessage(message);
                                if (splited[2].equals(student.getAccountNumber())) {
                                    client.getAssembler().chats.get(i).setNewText(splited[2], splited[3], true, hour, minute);
                                } else {
                                    client.getAssembler().chats.get(i).setNewText(splited[2], splited[3], false, hour, minute);
                                }
                                break;
                                }

                        }
                    }
                });
            } catch (IOException ex) {
                System.out.println("Error reading from server: " + ex.getMessage());
                ex.printStackTrace();
                break;
            }
        }
    }


}
